/**
 * Extend the create view in order to pupolate the name on create
 */

 $(document).on('click', '.closeCustomIconII', function () {
    self.saveInventoryItemRecord();
});

function onChangeSubtype(subtype) {
    var checked = $(subtype).prop('checked');
    $('input[name="checkst"]').prop('checked', checked);
    if ($("#checkAllst:checked")) {
        var count = $("[name='checkst']:checked").length;
        if (count > 0) {
            var str = 'You have selected ' + count + ' records in the result set.<button type="button" class="btn btn-link btn-inline removeselectedchk" data-action="clear" onclick="removeselectedchk();">Clear selections.</button>';
            $('.showalert').show();
            $('.showalert').html(str);
            $('.showalertcls').show();
        } else {
            $('.showalert').hide();
            $('.showalert').html('');
            $('.showalertcls').hide();
        }

    } else {
        $('.showalert').hide();
        $('.showalert').html('');
        $('.showalertcls').hide();
    }
}

function removeselectedchk() {
    $('.showalert').html('');
    $('.showalertcls').hide();
    $('input[name="checkst"]').prop('checked', false);
    $('input[name="checkAllst"]').prop('checked', false);
}

/* Prakash Jangir 934 & 935 On 26-Feb-2021 */

//Settings for load more buttom
var offset = 0;
var orderType = "desc";
var lastSortedColumnName = "";
var hideShowMoreButton = 0;
var appendQuery = "";

var offsetPOI = 0;
var orderTypePOI = "desc";
var lastSortedColumnNamePOI = "";
var hideShowMoreButtonPOI = 0;
var appendQueryPOI = "";

(function (app) {

    $(document).on('click', 'button[data-action="show-more-ii-sidecar"]', function () {
        $('button[data-action="show-more-ii-sidecar"]').hide();
        var record_count1 = $('.get-ii-sidecar-count').attr('record_count');
        var record_count = parseInt(record_count1);
        $('div.loading-ii').show();

        var workProduct = $('.get-ii-sidecar-count').attr('wpId');
        var name_search = $('.ts_selection input.search-name').val();
        var APIURL = "";
        if (name_search != "") {
            APIURL = "rest/v10/get_wpa_test_system_search";
        } else {
            APIURL = "rest/v10/get_wpa_test_system";
        }

        app.api.call("create", APIURL, {
            workProduct: workProduct,
            record_count: record_count,
            searchText: name_search,
        }, {
            success: function (tsRecords) {
                $('div.loading-ii').hide();

                if (tsRecords.records.length == 0) {
                    hideShowMoreButton = 1;
                    $('button[data-action="show-more-ii-sidecar"]').hide();
                    $('.iiloadmore').remove();
                    $('.loading-ii').remove();
                    return;
                }
                var tsModels = [];

                if (tsRecords.records && tsRecords.records.length > 0) {
                    tsModels = tsRecords.records;
                    record_count = record_count + 20;
                    console.log('record_count 2 ', record_count);
                    $('.get-ii-sidecar-count').attr('record_count', record_count);
                } else {
                    $('.get-ii-sidecar-count').attr('record_count', 20);
                    $("div.main-pane.ts_selection div.main-content").html('');
                    $('.iiloadmore').remove();
                    $('.loading-ii').remove();
                }


                tsView.collection.add(tsModels);
                tsView.render();

                $('div.main-pane.ts_selection div.flex-list-view-content').css("overflow", "visible");
                $('button[data-action="show-more-ii-sidecar"]').show();

            }.bind(this),
            error: function (error) {
                app.alert.show("ts_wp_error", {
                    level: "error",
                    messages: "Error retrieving Test System of Work Products",
                    autoClose: true
                });
            }
        });
    });

    app.events.on("app:sync:complete", function () {
        if (!app.view.views.BaseII_Inventory_ItemCreateView && !app.view.views.BaseII_Inventory_ItemCustomCreateView) {
            app.view.declareComponent("view", "create", "II_Inventory_Item", undefined, false, "base");
        }

        var createView = "BaseII_Inventory_ItemCreateView";

        if (app.view.views.BaseII_Inventory_ItemCustomCreateView) {
            createView = "BaseII_Inventory_ItemCustomCreateView";
        }

        if (App.view.views[createView].createExtended === true) {
            return;
        }

        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function () {
                this._super("initialize", arguments);
                /**
                 * Registering events on the model
                 */
                this.model.on('change:m01_sales_ii_inventory_item_1_name', this.showWorkProductsOfSale, this);
                this.model.on('change:unique_id_type', this.setUniqueId, this);
                this.model.on('change:collection_task', this.setTimepoint, this);
                this.model.on('change:related_to_c', this.resetWorkProducts, this);
                /** //////////// */
                this.model.on('change:m03_work_product_ii_inventory_item_1_name', this.showTestSystemOfWorkProduct, this);
                this.model.on('change:type_2', this.getSubtypeSubpanel, this);
                app.error.errorName2Keys['custom_message'] = "Required"; //'ERROR_CUSTOM_MESSAGE';
                //add validation tasks         
                this.model.addValidationTask('subtype', _.bind(this._doValidatesubtype, this));
                this.model.on('change:ri_received_items_ii_inventory_item_1_name', this.getReceivedItemDetails, this);
                this.model.on('change:pk_samples_c', this.pk_samples_function, this);
            },

            render: function () {
                this._super("render", arguments);
                this.model.set("status", "Planned Inventory");
                this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();
                if (this.model.get("related_to_c") === "") {
                    this.layout.layout.$el.find("[data-component=sidebar] .ts-selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").hide();
                    /*Added by gsingh #387 */
                    this.layout.layout.$el.find("[data-component=sidebar] .poi_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ori_selection").hide();
                }
                this.setUniqueId(this.model);
                /*var getInternalBarcode = this.model.get("internal_barcode");
                 if (getInternalBarcode != "")
                 {
                 this.model.set("internal_barcode", getInternalBarcode);
                 } else {
                 this.model.set("internal_barcode", this.generateUniqueId());
                 }*/


                //var parentModule = this.context.parent.get('module');

                if (this.context.parent.get('module') == "RI_Received_Items") {
                    var self = this;
                    var parentType_2 = this.context.parent.get('model').get('type_2');
                    if (parentType_2 == "POI") {
                        this.model.set('related_to_c', 'Purchase Order Item');
                        setTimeout(function () {
                            self.getReceivedItemDetails();
                        });
                    } else if (parentType_2 == "ORI") {
                        this.model.set('related_to_c', 'Order Request Item');
                        setTimeout(function () {
                            self.getReceivedItemDetails();
                        });
                    }

                }
            },

            /** //////////// */
            _doValidatesubtype: function (fields, errors, callback) {

                var type2 = this.model.get('type_2');
                var subtype = this.model.get('subtype');
                var pk_samplesc_field = this.model.get("pk_samples_c");

                if ((type2 == "Equipment Facility Record" || type2 == "Block" || type2 == "Culture" || type2 == "Slide" || type2 == "Tissue") && subtype == "" && pk_samplesc_field != 'No' && pk_samplesc_field != 'NA') {
                    errors['subtype'] = "Subtype should not be blank!";
                    errors['subtype'].custom_message = true;
                    app.alert.show('subtypealert', {
                        level: 'error',
                        messages: 'Subtype field is required.'
                    });
                } else {
                    app.alert.dismiss("subtypealert");
                }
                callback(null, fields, errors);
            },

            /**
             * When the Task is being changed, then set the timepoint field
             * @param {SugarBean} model 
             */


            setTimepoint: function (model) {
                var taskId = model.get("tdes_task_design_id_c");
                if (taskId) {
                    app.api.call("read", "rest/v10/TDes_Task_Design/" + taskId + "?fields=timepoint,timepoint_integer,timepoint_unit", null, {
                        success: function (task) {
                            if (task && task.id) {
                                if (task.timepoint && (
                                    task.timepoint == 'Day 0' || task.timepoint == 'Day 1' || task.timepoint == 'None')) {
                                    this.model.set('ii_timepoint_c', task.timepoint);
                                } else {
                                    this.model.set('ii_timepoint_c', task.timepoint_unit + " " + task.timepoint_integer);
                                }
                            }
                        }.bind(this),
                        error: function (error) {
                            app.alert.show('error_retreiving', {
                                level: 'error',
                                messages: 'Error retrieving Task'
                            })
                        }
                    })
                }
            },
            /**
             * Update the Unique Id field when setting the Id type as Internal
             * @param {SugarBean} model 
             */
            setUniqueId: function (model) {
                if (model.get("unique_id_type") == "Internal") {
                    this.model.set("unique_id_c", this.generateUniqueId());
                    this.hideUniqueId();
                    //this.showQuantity();
                } else if (model.get("unique_id_type") == "External") {
                    this.model.set("unique_id_c", "");
                    this.showUniqueId();
                    //this.hideQuantity();
                } else {
                    this.hideUniqueId();
                    //this.hideQuantity();
                }
            },
            showQuantity: function () {
                this.$el.find(".record-cell[data-name='quantity_c']").show();
            },
            hideQuantity: function () {
                this.$el.find(".record-cell[data-name='quantity_c']").hide();
            },
            hideUniqueId: function () {
                this.$el.find(".record-cell[data-name='unique_id_c']").hide();
            },
            showUniqueId: function () {
                this.$el.find(".record-cell[data-name='unique_id_c']").show();
            },

            /**
             * Generates a random string
             */
            generateUniqueId: function () {
                return "AP-" + Math.random().toString().substring(2, 10)
            },
            /**
             * Reset the Products view when choosing the type as Work Product
             * @param {SugarBean} model  
             */
            resetWorkProducts: function (model) {

                $('.panel_body').removeAttr("style");
                $('div[data-name="m01_sales_ii_inventory_item_1_name"]').removeAttr("style");
                $('div[data-name="m03_work_product_ii_inventory_item_1_name"]').removeAttr("style");

                var relatedto = this.model.get("related_to_c");
                var category = this.model.get("category");

                $('div[data-name="subtype"]').css('pointer-events', 'none');
                this.model.set("m01_sales_ii_inventory_item_1m01_sales_ida", null);
                this.model.set("m01_sales_ii_inventory_item_1_name", null);

                this.model.set("m03_work_product_ii_inventory_item_1m03_work_product_ida", null);
                this.model.set("m03_work_product_ii_inventory_item_1_name", null);

                var wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                var wpView = wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("customWp-selection");


                this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();
                this.layout.layout.$el.find("[data-component=sidebar] .ts-selection").hide();

                var type2 = this.model.get("type_2");
                if (type2 != "" && (type2 == "Equipment Facility Record" || type2 == "Block" || type2 == "Culture" || type2 == "Slide" || type2 == "Tissue")) {
                    this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").show();
                    this.layout.layout.$el.find(".subtype_selection").css("margin-top", "unset");
                    this.layout.layout.$el.find(".subtype_selection").css("overflow", "unset");
                    this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                } else {
                    this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").hide();
                }

                if (relatedto != 'Order Request Item' && relatedto != 'Purchase Order Item') {
                    setTimeout
                        (
                            function () {
                                $('input[name="owner"]').prop('disabled', false);
                                $('input[name="product_name"]').prop('disabled', false);
                                $('input[name="type_2"]').prop('disabled', false);
                                $('input[name="manufacturer_c"]').prop('disabled', false);
                                $('input[name="concentration_c"]').prop('disabled', false);
                                $('input[name="concentration_unit_c"]').prop('disabled', false);
                                $('input[name="product_id_2_c"]').prop('disabled', false);
                                $('input[name="external_barcode"]').prop('disabled', false);
                                $('input[name="allowed_storage_conditions"]').prop('disabled', false);
                                $('input[name="subtype"]').prop('disabled', false);
                            },
                            500
                        );
                    $('.checkall').trigger('click');
                    $('.checkall input[name="check"]').prop('checked', false);
                    $('input[name="check"]').trigger('click');
                    $('input[name="check"]').prop('checked', false);
                }
                this.model.set("type_2", null);
                this.model.set("subtype", null);
                this.model.set("owner", null);
                //this.model.set("product_name", null);
                this.model.set("manufacturer_c", null);

                this.model.set("poi_purcha8945er_item_ida", null);
                this.model.set("poi_purchase_order_item_ii_inventory_item_1_name", null);

                this.model.set("ori_order_2123st_item_ida", null);
                this.model.set("ori_order_request_item_ii_inventory_item_1_name", null);

            },
            /**
             * Whem choosing the type to be Sales then show a list with all the Work Products related to that sale
             * @param {SugarBean} model  
             */
            showWorkProductsOfSale: function (model) {

                if (this.layout.layout.isSidePaneVisible() === false) {
                    this.layout.layout.toggleSidePane();
                }

                var related_to = this.model.get('related_to_c');
                if (related_to != 'Purchase Order Item' && related_to != 'Order Request Item') {
                    this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").show();
                    this.layout.layout.$el.find("[data-component=sidebar] .poi_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ori_selection").hide();
                }
                this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").hide();
                salesName = this.model.get("m01_sales_ii_inventory_item_1_name");
                if (salesName == "" || salesName == null) {
                    // this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                }

                var type2 = this.model.get("type_2");
                if (type2 != "" && (type2 == "Equipment Facility Record" || type2 == "Block" || type2 == "Culture" || type2 == "Slide" || type2 == "Tissue")) {
                    this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").show();
                    var work_product = this.model.get('m03_work_product_ii_inventory_item_1_name');
                    var sales = this.model.get('m01_sales_ii_inventory_item_1_name');
                    if (related_to == 'Multiple Test Systems' && work_product != "") {
                        this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").show();
                        this.layout.layout.$el.find(".ts_selection").css("height", "250px");
                        this.layout.layout.$el.find(".ts_selection").css("overflow", "scroll");
                        this.layout.layout.$el.find(".subtype_selection").css("margin-top", "250px");
                        this.layout.layout.$el.find(".subtype_selection").css("overflow", "scroll");
                        this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                    } else if (related_to == 'Sales' && sales != "") {
                        this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").show();
                        this.layout.layout.$el.find(".wp_selection").css("height", "250px");
                        this.layout.layout.$el.find(".wp_selection").css("overflow", "scroll");
                        this.layout.layout.$el.find(".subtype_selection").css("margin-top", "250px");
                        this.layout.layout.$el.find(".subtype_selection").css("overflow", "scroll");
                        this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                    } else {
                        this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .poi_selection").hide();
                        this.layout.layout.$el.find(".wp_selection").css("height", "unset");
                        this.layout.layout.$el.find(".wp_selection").css("overflow", "unset");
                        this.layout.layout.$el.find(".ts_selection").css("height", "unset");
                        this.layout.layout.$el.find(".ts_selection").css("overflow", "unset");
                        this.layout.layout.$el.find(".subtype_selection").css("margin-top", "unset");
                        this.layout.layout.$el.find(".subtype_selection").css("overflow", "unset");
                        this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                    }
                } else {
                    this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").hide();
                }
                var saleId = model.get("m01_sales_ii_inventory_item_1m01_sales_ida");
                if (saleId) {

                    app.api.call("read", "rest/v10/M01_Sales/" + saleId + "/link/m01_sales_m03_work_product_1?fields=name,work_product_status_c", null, {
                        success: function (wps) {
                            var wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                            var wpView = wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("customWp-selection");

                            var wpModels = [];

                            if (wps.records && wps.records.length > 0) {
                                wpModels = wps.records;
                            }

                            wpView.collection.reset();
                            wpView.collection.add(wpModels);

                            wpView.render();
                            /* this.layout.layout.$el.find(".wp_selection .dataTable td:first-child span").css("margin-top", "-13px");
                             this.layout.layout.$el.find(".wp_selection .dataTable td:first-child span").css("padding", "0px 12px"); */
                            this.layout.layout.$el.find(".wp_selection .dataTable td:nth-child(2) span").css("line-height", "21px");
                            this.layout.layout.$el.find(".wp_selection .btn.checkall").css("border", "none");
                        }.bind(this),
                        error: function (error) {
                            app.alert.show("sale_wp_error", {
                                level: "error",
                                messages: "Error retrieving Work Products for this Sale",
                                autoClose: true
                            });
                        }
                    });
                }

            },
            /**
             * When saving the Inventory Item, make sure we link the selected work products.
             * Also if the quantity is greatere than 0 then create duplicates
             */
            saveAndClose: function () {
                $("#alertsCustom").remove();

                self = this;
                var other_test_type_c = self.model.get("other_test_type_c");
                var specimentypeL = self.model.get("specimen_type_1_c");
                var testtypeL = self.model.get("test_type_c");
                if (specimentypeL == "Other" || testtypeL == "Other") {
                    self.model.set("other_test_type_c", other_test_type_c);
                }

                if (self.model.get("processed_per_protocol_c") == "No") {
                    $("#sidecar").prepend(`    
                        <div id="alertsCustom" class="alert-top">
                        <div class="alert-wrapper">
                            <div class="alert alert-warning alert-block closeable">
                                <div class="indicator">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </div>
                                <button class="close btn btn-link btn-invisible closeCustomIconII" data-action="close"><i class="fa fa-times"></i></button>
                                <span class="text">
                            <strong>Warning: </strong>
                            Inventory Item not processed per protocol. Remember to submit a Communication.
                        </span>
                            </div>
                        </div>
                        </div>
                    `);
                } else {
                    self.saveInventoryItemRecord();
                }
            },

            saveInventoryItemRecord: function () {
                $("#alertsCustom").remove();

                if (this.wpSelected() == false) {
                    app.alert.show('no_wps_seletecte', {
                        level: 'error',
                        messages: 'A work product must be linked!'
                    });
                    return;
                }

                if (this.model.get("related_to_c") === "Multiple Test Systems") {
                    //console.log('tsSelected line 457');
                    var tsData = this.tsSelected();

                    if (tsData == false) {
                        app.alert.show('no_ts_seletecte', {
                            level: 'error',
                            messages: 'A Test System must be linked!'
                        });
                        return;
                    } else {
                        if (tsData.status == true) {
                            console.log('tsSelected line 468');
                            var strid = '';
                            strid = tsData.TsId;
                            var tostrId = strid.toString();
                            var TsIdArr = tostrId.split(',');
                            var TsId_str = TsIdArr[0];

                            var strName = '';
                            strName = tsData.TsName;
                            var tostrName = strName.toString();
                            var TsNameArr = tostrName.split(',');
                            var TsName_str = TsNameArr[0];

                            this.model.set('anml_animals_ii_inventory_item_1anml_animals_ida', TsId_str);
                            this.model.set('anml_animals_ii_inventory_item_1_name', TsName_str);

                            var subtype = tsData.subtype;
                            var subtype_str = subtype.toString();
                            var subtypeArr = subtype_str.split(',');


                            this.model.set('subtype', subtypeArr[0]);

                            var pk_samplesc = this.model.get("pk_samples_c");
                            //console.log('pk_samplesc line 522');
                            if (pk_samplesc == 'No') {
                                var specimen_type_var = this.model.get("specimen_type_1_c");
                                this.model.set('type_2', specimen_type_var);

                                var test_type_var = this.model.get("test_type_1_c");
                                this.model.set('test_type_c', test_type_var);
                            }

                        } else {
                            app.alert.show('no_ts_seletecte', {
                                level: 'error',
                                messages: 'A Test System must be linked!'
                            });
                            return;
                        }
                        //console.log(app.controller.context.get('model'));
                    }
                } else if (this.model.get("related_to_c") === "Sales" || this.model.get("related_to_c") === "Work Product" || this.model.get("related_to_c") === "Single Test System") {
                    var subtype_chk = [];
                    $.each($("input[name='checkst']:checked"), function () {
                        subtype_chk.push($(this).val());
                    });
                    var subtype = subtype_chk.join(",");
                    var subtype_str = subtype.toString();
                    console.log('subtype_strLax', subtype_str);
                }

                var subtypefield = 0;
                var type2 = this.model.get("type_2");
                var category = this.model.get("category");
                var pk_samples_c1 = this.model.get("pk_samples_c");


                if (type2 == "Equipment Facility Record") {

                    var subtype_selected = [];
                    $.each($("input[name='checkst']:checked"), function () {
                        subtype_selected.push($(this).val());
                    });
                    var sub_type = subtype_selected.join(",");
                    var sub_type_str = sub_type.toString();
                    var sub_type_Arr = sub_type_str.split(',');

                    this.model.set('subtype', sub_type_Arr[0]);

                    if (sub_type_str == "") {
                        subtypefield = 0;
                    } else {
                        subtypefield = sub_type_str;
                    }


                } else if ((type2 == "Block" || type2 == "Culture" || type2 == "Slide" || type2 == "Tissue") && pk_samples_c1 != "No") {
                    var subtype_specimen_selected = [];
                    $.each($("input[name='checkst']:checked"), function () {
                        subtype_specimen_selected.push($(this).val());
                    });
                    var sub_specimen_type = subtype_specimen_selected.join(",");
                    var sub_specimen_type_str = sub_specimen_type.toString();
                    var sub_specimen_type_Arr = sub_specimen_type_str.split(',');

                    this.model.set('subtype', sub_specimen_type_Arr[0]);

                    if (sub_specimen_type_str == "") {
                        subtypefield = 0;
                    } else {
                        subtypefield = sub_specimen_type_str;
                    }
                }

                var pk_samplesc = this.model.get("pk_samples_c");
                if (pk_samplesc == 'No') {
                    var specimen_type_var = this.model.get("specimen_type_1_c");
                    this.model.set('type_2', specimen_type_var);

                    var test_type_var = this.model.get("test_type_1_c");
                    this.model.set('test_type_c', test_type_var);
                }

                console.log('pk_samplesc type_2 line 595');
                console.log('pk_samplesc test_type_c line 596');

                var self = this;
                var IIName = this.model.get("name");
                var IINameNew = "";
                var related_to = this.model.get("related_to_c");
                var relatedName = "";
                var animalName = this.model.get("anml_animals_ii_inventory_item_1_name");
                var productName = this.model.get("product_name");
                //var solutionName = this.model.get("solution_name_c");
                type2Name = this.model.get("type_2");
                subtypeName = this.model.get("subtype");
                subtypeProductName = this.model.get("subtype");
                subtypeSpecimenName = this.model.get("subtype");
                var other_type = this.model.get("other_type_c");
                /**08 March 2022 : #2218 Changes : Update Inventory Item Name */
                var test_type_c = this.model.get("test_type_c");
                var other_test_type_c = this.model.get("other_test_type_c");
                var timepoint_type = this.model.get("timepoint_type");
                var timepoint_name_c = this.model.get("timepoint_name_c");
                var timepoint_integer = this.model.get("timepoint_integer");
                var timepoint_unit = this.model.get("timepoint_unit");
                var timepoint_end_integer_c = this.model.get("timepoint_end_integer_c");
                /**EOC ** 08 March 2022 : #2218 Changes : Update Inventory Item Name */

                var test_typeArr = [];
                var type2Arr = [];
                subtypeArr = [];
                var timpointArr = [];
                subtypeProductArr = [];
                subtypeSpecimenArr = [];
                setTimeout(function () {
                    if (related_to == "Work Product" || related_to == "Single Test System" || related_to == "Multiple Test Systems") {
                        relatedName = self.model.get("m03_work_product_ii_inventory_item_1_name");
                    }

                    if (related_to == "Sales") {
                        relatedName = self.model.get("m01_sales_ii_inventory_item_1_name");
                    }

                    if (relatedName != "") {
                        IINameNew = relatedName;
                    }
                    /* Prakash Jangir 148 start */
                    if (animalName != "" && animalName != "undefined" && animalName != null) {
                        console.log('1');
                        var animalId = self.model.get("anml_animals_ii_inventory_item_1anml_animals_ida");
                        App.api.call("get", "rest/v10/ANML_Animals/" + animalId + "?fields=name,species_2_c,usda_id_c", null, {
                            success: function (data) {
                                var arrPullTSId = ["", "Bovine", "Canine", "Caprine", "Ovine", "Porcine"];
                                var arrPullUSDAId = ["Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig"];
                                if (data.usda_id_c != "") {
                                    if (arrPullTSId.includes(data.species_2_c)) {
                                        IINameNew += " " + animalName;
                                    } else if (arrPullUSDAId.includes(data.species_2_c)) {
                                        IINameNew += " " + data.usda_id_c;
                                    } else {
                                        IINameNew += " " + animalName;
                                    }
                                } else {
                                    IINameNew += " " + animalName;
                                }

                                if (typeof productName !== 'undefined' && productName != "" && productName != "undefined" && productName != null) {
                                    IINameNew += " " + productName;
                                }

                                if (type2Name != "" && type2Name != "undefined" && type2Name != null && type2Name == "Other" && (related_to == "Purchase Order Item" || related_to == "Order Request Item")) {
                                    var ProductTypeArr = App.lang.getAppListStrings("product_category_list");
                                    IINameNew += " " + ProductTypeArr[type2Name];
                                }

                                if (type2Name != "" && type2Name != "undefined" && type2Name != null && type2Name != "Other") {
                                    var ProductTypeArr = App.lang.getAppListStrings("product_category_list");
                                    var InventoryTypeArr = App.lang.getAppListStrings("inventory_item_type_list");
                                    type2Arr = $.extend(ProductTypeArr, InventoryTypeArr);
                                    IINameNew += " " + type2Arr[type2Name];
                                }

                                if (type2Name == "Other" && other_type != "" && other_type != "undefined" && other_type != null) {
                                    IINameNew += " " + other_type;
                                }

                                /**08 March 2022 : #2218 Changes : Update Inventory Item Name */
                                if (test_type_c != "" && test_type_c != "undefined" && test_type_c != null && test_type_c != "Other") {
                                    var ProductTypeArr = App.lang.getAppListStrings("product_category_list");
                                    var InventoryTypeArr = App.lang.getAppListStrings("ii_test_type_list");
                                    test_typeArr = $.extend(ProductTypeArr, InventoryTypeArr);
                                    IINameNew += " " + test_typeArr[test_type_c];
                                }

                                if (test_type_c == "Other" && other_test_type_c != "" && other_test_type_c != "undefined" && other_test_type_c != null) {
                                    IINameNew += " " + other_test_type_c;
                                }
                                if (timepoint_type != "" && timepoint_type != "undefined" && timepoint_type != null && timepoint_type != "Ad Hoc" && timepoint_type != "Defined") {
                                    timpointArr = App.lang.getAppListStrings("timepoint_type_list");
                                    IINameNew += " " + timpointArr[timepoint_type];
                                }
                                if (timepoint_type == "Ad Hoc" || timepoint_type == "Defined") {
                                    var nametoupdate = '';
                                    if (timepoint_name_c != "" && timepoint_name_c != "undefined" && timepoint_name_c != null) {                                
                                        timepoint_name_c = " " + timepoint_name_c;
                                    }
                                    else
                                    {
                                        timepoint_name_c ='';
                                    }
                                    if (timepoint_unit != "" && timepoint_unit != "undefined" && timepoint_unit != null) {                                
                                        timepoint_unit = " " + timepoint_unit;
                                    }
                                    else
                                    {
                                        timepoint_unit ='';
                                    }
                                    console.log('timepoint_integer line 723',timepoint_integer);
                                    if (timepoint_integer != "" && timepoint_integer != "0" && timepoint_integer != "undefined" && timepoint_integer != null) {                                
                                        timepoint_integer = " " + timepoint_integer;
                                        console.log('timepoint_integer line 726',timepoint_integer);
                                    }
                                    else
                                    {
                                        console.log('timepoint_integer line 730',timepoint_integer);
                                        if(timepoint_integer == "0")
                                        {
                                            timepoint_integer = timepoint_integer;
                                        }
                                        else
                                        {
                                            timepoint_integer ='';
                                        }
                                        
                                    }
                                    console.log('timepoint_end_integer_c line 732',timepoint_end_integer_c); 
                                    if (timepoint_end_integer_c != "" && timepoint_end_integer_c != "0" && timepoint_end_integer_c != "undefined" && timepoint_end_integer_c != null) {                      
                                       
                                        if(timepoint_integer!='' && timepoint_integer!='0' && timepoint_integer != "undefined" && timepoint_integer != null)
                                        {
                                            console.log('timepoint_end_integer_c line 737',timepoint_end_integer_c); 
                                            timepoint_end_integer_c = "-" + timepoint_end_integer_c;
                                        }
                                        else
                                        {
                                            console.log('timepoint_end_integer_c line 741',timepoint_end_integer_c); 
                                            if(timepoint_integer =='0')
                                            {
                                                timepoint_end_integer_c = "-" + timepoint_end_integer_c;
                                            }
                                            else
                                            {
                                                timepoint_end_integer_c = "" + timepoint_end_integer_c;
                                            }
                                            
                                        }
                                        
                                    }
                                    else
                                    {
                                        if(timepoint_end_integer_c =='0')
                                        {
                                            timepoint_end_integer_c = "-" + timepoint_end_integer_c;
                                        }
                                        else
                                        {
                                            timepoint_end_integer_c ='';
                                        }
                                        
                                    }    
                                    console.log('timepoint_integer line 748',timepoint_integer);    
                                    console.log('timepoint_end_integer_c line 752',timepoint_end_integer_c);                  
                                    nametoupdate = timepoint_name_c + timepoint_unit + timepoint_integer + timepoint_end_integer_c;
                                    console.log('line 795 ii nametoupdate',nametoupdate);
                                    IINameNew += nametoupdate;
                                }
                                /**EOC ** 08 March 2022 : #2218 Changes : Update Inventory Item Name */
                                if (subtypeName != "" && subtypeName != "undefined" && subtypeName != null && type2 == "Equipment Facility Record") {
                                    subtypeArr = App.lang.getAppListStrings("inventory_item_subtype_list");
                                    IINameNew += " " + subtypeArr[subtypeName];
                                }

                                if (subtypeProductName != "" && subtypeProductName != "undefined" && subtypeProductName != null && category == "Product") {
                                    console.log('st1', subtypeProductName);
                                    subtypeProductArr = App.lang.getAppListStrings("pro_subtype_list");
                                    if (subtypeProductName == 0) {
                                        IINameNew += " ";
                                    } else {
                                        IINameNew += " " + subtypeProductArr[subtypeProductName];
                                    }
                                }

                                if (subtypeSpecimenName != "" && subtypeSpecimenName != "undefined" && subtypeSpecimenName != null && (type2 == "Block" || type2 == "Culture" || type2 == "Slide" || type2 == "Tissue")) {
                                    subtypeSpecimenArr = App.lang.getAppListStrings("tissues_list");
                                    IINameNew += " " + subtypeSpecimenArr[subtypeSpecimenName];
                                }

                                self.model.set("name", IINameNew);
                                console.log('name', IINameNew);
                            }
                        });
                    }
                    else {
                        if (typeof productName !== 'undefined' && productName != "" && productName != "undefined" && productName != null) {
                            IINameNew += " " + productName;
                        }

                        if (type2Name != "" && type2Name != "undefined" && type2Name != null && type2Name == "Other" && (related_to == "Purchase Order Item" || related_to == "Order Request Item")) {
                            var ProductTypeArr = App.lang.getAppListStrings("product_category_list");
                            IINameNew += " " + ProductTypeArr[type2Name];
                        }

                        if (type2Name != "" && type2Name != "undefined" && type2Name != null && type2Name != "Other") {

                            var ProductTypeArr = App.lang.getAppListStrings("product_category_list");
                            var InventoryTypeArr = App.lang.getAppListStrings("inventory_item_type_list");

                            type2Arr = $.extend(ProductTypeArr, InventoryTypeArr);
                            IINameNew += " " + type2Arr[type2Name];
                        }
                        if (type2Name == "Other" && other_type != "" && other_type != "undefined" && other_type != null) {
                            IINameNew += " " + other_type;
                        }
                        /**08 March 2022 : #2218 Changes : Update Inventory Item Name */
                        if (test_type_c != "" && test_type_c != "undefined" && test_type_c != null && test_type_c != "Other") {
                            var ProductTypeArr = App.lang.getAppListStrings("product_category_list");
                            var InventoryTypeArr = App.lang.getAppListStrings("ii_test_type_list");
                            test_typeArr = $.extend(ProductTypeArr, InventoryTypeArr);
                            IINameNew += " " + test_typeArr[test_type_c];
                        }

                        if (test_type_c == "Other" && other_test_type_c != "" && other_test_type_c != "undefined" && other_test_type_c != null) {
                            IINameNew += " " + other_test_type_c;
                        }

                        if (timepoint_type != "" && timepoint_type != "undefined" && timepoint_type != null && timepoint_type != "Ad Hoc" && timepoint_type != "Defined") {
                            timpointArr = App.lang.getAppListStrings("timepoint_type_list");
                            IINameNew += " " + timpointArr[timepoint_type];
                        }
                        if (timepoint_type == "Ad Hoc" || timepoint_type == "Defined") {
                            var nametoupdate = '';
                            if (timepoint_name_c != "" && timepoint_name_c != "undefined" && timepoint_name_c != null) {                                
                                timepoint_name_c = " " + timepoint_name_c;
                            }
                            else
                            {
                                timepoint_name_c ='';
                            }
                            if (timepoint_unit != "" && timepoint_unit != "undefined" && timepoint_unit != null) {                                
                                timepoint_unit = " " + timepoint_unit;
                            }
                            else
                            {
                                timepoint_unit ='';
                            }
                            console.log('timepoint_integer line 837',timepoint_integer);
                            if (timepoint_integer != "" && timepoint_integer != "0" && timepoint_integer != "undefined" && timepoint_integer != null) {                                
                                timepoint_integer = " " + timepoint_integer;
                                console.log('timepoint_integer line 840',timepoint_integer);
                            }
                            else
                            {
                                console.log('timepoint_integer line 844',timepoint_integer);
                                if(timepoint_integer == "0")
                                {
                                    timepoint_integer = timepoint_integer;
                                }
                                else
                                {
                                    timepoint_integer ='';
                                }
                               
                            }
                            console.log('timepoint_end_integer_c line 847',timepoint_end_integer_c); 
                            if (timepoint_end_integer_c != "" && timepoint_end_integer_c != "0" && timepoint_end_integer_c != "undefined" && timepoint_end_integer_c != null) {                      
                               
                                if(timepoint_integer!='' && timepoint_integer!='0'  && timepoint_integer != "undefined" && timepoint_integer != null)
                                {
                                    console.log('timepoint_end_integer_c line 737',timepoint_end_integer_c); 
                                    timepoint_end_integer_c = "-" + timepoint_end_integer_c;
                                }
                                else
                                {
                                    console.log('timepoint_end_integer_c line 741',timepoint_end_integer_c); 
                                    if(timepoint_integer == "0")
                                    {
                                        timepoint_end_integer_c = "-" + timepoint_end_integer_c;
                                    }
                                    else
                                    {
                                        timepoint_end_integer_c = "" + timepoint_end_integer_c;
                                    }
                                   
                                }
                                
                            }
                            else
                            {
                                if(timepoint_end_integer_c =='0')
                                {
                                    timepoint_end_integer_c = "-" + timepoint_end_integer_c;
                                }
                                else
                                {
                                    timepoint_end_integer_c ='';
                                }
                                
                            }    
                            console.log('timepoint_integer line 766',timepoint_integer);    
                            console.log('timepoint_end_integer_c line 867',timepoint_end_integer_c);                  
                            nametoupdate = timepoint_name_c + timepoint_unit + timepoint_integer + timepoint_end_integer_c;
                            console.log('line 869 ii nametoupdate',nametoupdate);
                            IINameNew += nametoupdate;
                        }

                        /**EOC ** 08 March 2022 : #2218 Changes : Update Inventory Item Name */

                        if (subtypeName != "" && subtypeName != "undefined" && subtypeName != null && type2 == "Equipment Facility Record") {
                            subtypeArr = App.lang.getAppListStrings("inventory_item_subtype_list");
                            IINameNew += " " + subtypeArr[subtypeName];
                        }

                        if (subtypeProductName != "" && subtypeProductName != "undefined" && subtypeProductName != null && category == "Product") {
                            subtypeProductArr = App.lang.getAppListStrings("pro_subtype_list");
                            if (subtypeProductName == 0) {
                                IINameNew += " ";
                            } else {
                                IINameNew += " " + subtypeProductArr[subtypeProductName];
                            }
                        }

                        if (subtypeSpecimenName != "" && subtypeSpecimenName != "undefined" && subtypeSpecimenName != null && (type2 == "Block" || type2 == "Culture" || type2 == "Slide" || type2 == "Tissue")) {
                            subtypeSpecimenArr = App.lang.getAppListStrings("tissues_list");
                            IINameNew += " " + subtypeSpecimenArr[subtypeSpecimenName];
                        }
                        self.model.set("name", IINameNew);
                        console.log('name', IINameNew);
                    }
                    self.model.set('ts_hidden_string_c', '');
                }, 500);
                //console.log('laxsdsd');
                setTimeout(function () {
                    self.initiateSave(_.bind(function () {
                        //console.log('lax');
                        var number_of_aliquots_c = self.model.get("number_of_aliquots_c");
                        var test_type_c = self.model.get("test_type_c");
                        var type_2 = self.model.get("type_2");
                        var category = self.model.get("category");
                        var pk_samples_c = self.model.get("pk_samples_c");

                        console.log('other_test_type_c line 754', self.model.get("other_test_type_c"));

                        if (pk_samples_c == 'No') {

                            specimen_type = [];
                            test_type = [];

                            var specimen_type_1_c = self.model.get("specimen_type_1_c");
                            specimen_type.push(specimen_type_1_c);
                            var specimen_type_2_c = self.model.get("specimen_type_2_c");
                            specimen_type.push(specimen_type_2_c);
                            var specimen_type_3_c = self.model.get("specimen_type_3_c");
                            specimen_type.push(specimen_type_3_c);
                            var specimen_type_4_c = self.model.get("specimen_type_4_c");
                            specimen_type.push(specimen_type_4_c);
                            var specimen_type_5_c = self.model.get("specimen_type_5_c");
                            specimen_type.push(specimen_type_5_c);

                            var test_type_1_c = self.model.get("test_type_1_c");
                            test_type.push(test_type_1_c);
                            var test_type_2_c = self.model.get("test_type_2_c");
                            test_type.push(test_type_2_c);
                            var test_type_3_c = self.model.get("test_type_3_c");
                            test_type.push(test_type_3_c);
                            var test_type_4_c = self.model.get("test_type_4_c");
                            test_type.push(test_type_4_c);
                            var test_type_5_c = self.model.get("test_type_5_c");
                            test_type.push(test_type_5_c);

                        }

                        self.model.set('ts_hidden_string_c', '');
                        if (self.model.get("related_to_c") === "Sales") {
                            self.linkWorkProducts().then(function (result) {
                                if (result === "success") {
                                    app.alert.dismiss("link_wps");

                                    /*#1556 : Auto-create duplicate IIs for Aliquots : 16 Sep 2021 */
                                    if (category == 'Specimen' && (number_of_aliquots_c != "" || number_of_aliquots_c != null) && number_of_aliquots_c > 0) {
                                        if (subtype_str == "") {
                                            subtype_str = "0";
                                        }
                                        app.alert.show('SubtyprTslink_tsq', {
                                            level: 'info',
                                            messages: 'Creating Duplicates Records In Progress ...',
                                            //autoClose: true
                                        });

                                        app.api.call("create", "rest/v10/create_aliquots_duplicates", {
                                            module: 'II_Inventory_Item',
                                            record: self.model.get("id"),
                                            aliquots: number_of_aliquots_c,
                                            subtype: subtype_str,
                                        }, {
                                            success: function (res) {
                                                app.alert.dismiss('SubtyprTslink_tsq');
                                            }.bind(this),
                                            error: function (error) {

                                            }
                                        });

                                        app.navigate(self.context, self.model);
                                        app.router.redirect('#II_Inventory_Item');

                                    } else if (category == 'Specimen' && pk_samples_c == 'No') {
                                        console.log('specimen_type', specimen_type);
                                        console.log('test_type', test_type);

                                        var url = app.api.buildURL("specimen_type_duplicates");
                                        var method = "create";
                                        var data = {
                                            module: 'II_Inventory_Item',
                                            recordId: self.model.get("id"),
                                            specimen_type: specimen_type,
                                            test_type: test_type
                                        };

                                        var callback = {
                                            success: _.bind(function successCB(res) {
                                                console.log('API response', res);
                                            }, this)
                                        };
                                        app.api.call(method, url, data, callback);
                                        app.navigate(self.context, self.model);
                                        app.router.redirect('#II_Inventory_Item');
                                    } else if (self.model.get('quantity_c') > 0) {
                                        app.alert.show('duplicate_creation', {
                                            level: 'info',
                                            messages: 'Duplicates will be available for you shortly...'
                                        });
                                        app.api.call('read', 'rest/v10/II_Inventory_Item/' + self.model.get("id") + '/' + subtypefield + '/create_duplicates', null, {
                                            success: function (result) {
                                                app.alert.dismiss('duplicate_creation');
                                            },
                                            error: function (error) {
                                                app.alert.show('link_wps_error', {
                                                    level: 'error',
                                                    messages: 'Failed Creating Duplicates!'
                                                });
                                            }
                                        })
                                        app.navigate(self.context, self.model);
                                        app.router.redirect('#II_Inventory_Item');
                                    }
                                } else {
                                    app.alert.show('link_wps_error', {
                                        level: 'error',
                                        messages: 'Failed linking Work Products...'
                                    });
                                }

                            }.bind(self));
                        } else if (self.model.get("related_to_c") === "Multiple Test Systems") {

                            //console.log('lax1');
                            /*#1556 : Auto-create duplicate IIs for Aliquots : 16 Sep 2021 */
                            if ((number_of_aliquots_c != "" || number_of_aliquots_c != null) && number_of_aliquots_c > 0) {
                                if (subtype_str == "") {
                                    subtype_str = "0";
                                }
                                app.alert.show('SubtyprTslink_ts', {
                                    level: 'info',
                                    messages: 'Creating Duplicates Records In Progress ...',
                                    //autoClose: true
                                });

                                app.api.call("create", "rest/v10/create_aliquots_tsmulti_duplicates", {
                                    module: 'II_Inventory_Item',
                                    record: self.model.get("id"),
                                    tsId: tostrId,
                                    tsName: tostrName,
                                    aliquots: number_of_aliquots_c,
                                    subtype: subtype_str,
                                }, {
                                    success: function (res) {
                                        app.alert.dismiss('SubtyprTslink_ts');
                                    }.bind(this),
                                    error: function (error) {

                                    }
                                });

                                app.navigate(self.context, self.model);
                                app.router.redirect('#II_Inventory_Item');
                            } else if (category == 'Specimen' && pk_samples_c == 'No') {
                                console.log('specimen_type', specimen_type);
                                console.log('test_type', test_type);


                                app.alert.show('SubtyprTslink_sts', {
                                    level: 'info',
                                    messages: 'Creating Duplicates Records In Progress ...',
                                    //autoClose: true
                                });

                                app.api.call("create", "rest/v10/specimen_type_ts_duplicates", {
                                    module: 'II_Inventory_Item',
                                    recordId: self.model.get("id"),
                                    specimen_type: specimen_type,
                                    test_type: test_type,
                                    tsId: tostrId,
                                    tsName: tostrName,
                                }, {
                                    success: function (res) {
                                        app.alert.dismiss('SubtyprTslink_sts');
                                    }.bind(this),
                                    error: function (error) {

                                    }
                                });

                                app.navigate(self.context, self.model);
                                app.router.redirect('#II_Inventory_Item');
                            } else {

                                if (subtype_str == "") {
                                    subtype_str = "0";
                                }

                                app.alert.show('SubtyprTslinks_sts', {
                                    level: 'info',
                                    messages: 'Creating Duplicates Records In Progress ...',
                                    autoClose: true
                                });

                                app.api.call("create", "rest/v10/create_ts_duplicates", {
                                    module: 'II_Inventory_Item',
                                    record: self.model.get("id"),
                                    subtype: subtype_str,
                                    tsId: tostrId,
                                    tsName: tostrName,
                                }, {
                                    success: function (res) {
                                        app.alert.dismiss('SubtyprTslinks_sts');
                                    }.bind(this),
                                    error: function (error) {

                                    }
                                });

                                app.navigate(self.context, self.model);
                                app.router.redirect('#II_Inventory_Item');

                            }

                        } else if (self.model.get("related_to_c") === "Work Product") {
                            /*#1556 : Auto-create duplicate IIs for Aliquots : 16 Sep 2021 */
                            if ((number_of_aliquots_c != "" || number_of_aliquots_c != null) && number_of_aliquots_c > 0) {

                                if (subtype_str == "") {
                                    subtype_str = "0";
                                }
                                app.alert.show('SubtyprTslink_tsq', {
                                    level: 'info',
                                    messages: 'Creating Duplicates Records In Progress ...',
                                    //autoClose: true
                                });

                                app.api.call("create", "rest/v10/create_aliquots_duplicates", {
                                    module: 'II_Inventory_Item',
                                    record: self.model.get("id"),
                                    aliquots: number_of_aliquots_c,
                                    subtype: subtype_str,
                                }, {
                                    success: function (res) {
                                        app.alert.dismiss('SubtyprTslink_tsq');
                                    }.bind(this),
                                    error: function (error) {

                                    }
                                });

                                app.navigate(self.context, self.model);
                                app.router.redirect('#II_Inventory_Item');


                            } else if (category == 'Specimen' && pk_samples_c == 'No') {
                                console.log('specimen_type', specimen_type);
                                console.log('test_type', test_type);

                                var url = app.api.buildURL("specimen_type_duplicates");
                                var method = "create";
                                var data = {
                                    module: 'II_Inventory_Item',
                                    recordId: self.model.get("id"),
                                    specimen_type: specimen_type,
                                    test_type: test_type
                                };

                                var callback = {
                                    success: _.bind(function successCB(res) {
                                        console.log('API response', res);
                                    }, this)
                                };
                                app.api.call(method, url, data, callback);
                                app.navigate(self.context, self.model);
                                app.router.redirect('#II_Inventory_Item');
                            } else {
                                app.api.call('read', 'rest/v10/II_Inventory_Item/' + self.model.get("id") + '/' + subtypefield + '/create_duplicates', null, {
                                    success: function (result) {
                                        app.alert.dismiss('duplicate_creation');
                                    },
                                    error: function (error) {
                                        app.alert.show('link_wps_error', {
                                            level: 'error',
                                            messages: 'Failed Creating Duplicates!'
                                        });
                                    }
                                })
                                app.navigate(self.context, self.model);
                                app.router.redirect('#II_Inventory_Item');
                            }
                        } else if (self.model.get("related_to_c") === "Single Test System") {
                            /*#1556 : Auto-create duplicate IIs for Aliquots : 16 Sep 2021 */
                            if ((number_of_aliquots_c != "" || number_of_aliquots_c != null) && number_of_aliquots_c > 0) {

                                if (subtype_str == "") {
                                    subtype_str = "0";
                                }
                                app.alert.show('SubtyprTslink_tsq', {
                                    level: 'info',
                                    messages: 'Creating Duplicates Records In Progress ...',
                                    //autoClose: true
                                });

                                app.api.call("create", "rest/v10/create_aliquots_duplicates", {
                                    module: 'II_Inventory_Item',
                                    record: self.model.get("id"),
                                    aliquots: number_of_aliquots_c,
                                    subtype: subtype_str,
                                }, {
                                    success: function (res) {
                                        app.alert.dismiss('SubtyprTslink_tsq');
                                    }.bind(this),
                                    error: function (error) {

                                    }
                                });

                                app.navigate(self.context, self.model);
                                app.router.redirect('#II_Inventory_Item');
                            } else if (category == 'Specimen' && pk_samples_c == 'No') {
                                console.log('specimen_type', specimen_type);
                                console.log('test_type', test_type);

                                var url = app.api.buildURL("specimen_type_duplicates");
                                var method = "create";
                                var data = {
                                    module: 'II_Inventory_Item',
                                    recordId: self.model.get("id"),
                                    specimen_type: specimen_type,
                                    test_type: test_type
                                };

                                var callback = {
                                    success: _.bind(function successCB(res) {
                                        console.log('API response', res);
                                    }, this)
                                };
                                app.api.call(method, url, data, callback);
                                app.navigate(self.context, self.model);
                                app.router.redirect('#II_Inventory_Item');
                            } else if (self.model.get('quantity_c') > 0) {
                                app.api.call('read', 'rest/v10/II_Inventory_Item/' + self.model.get("id") + '/' + subtypefield + '/create_duplicates', null, {
                                    success: function (result) {
                                        app.alert.dismiss('duplicate_creation');
                                    },
                                    error: function (error) {
                                        app.alert.show('link_wps_error', {
                                            level: 'error',
                                            messages: 'Failed Creating Duplicates!'
                                        });
                                    }
                                })
                                app.navigate(self.context, self.model);
                                app.router.redirect('#II_Inventory_Item');
                            }
                            //app.drawer.close(self.context, self.model);
                        } else if (self.model.get("related_to_c") === "Internal Use") {
                            alert('in "rel to internal use line 795');

                            app.api.call('read', 'rest/v10/II_Inventory_Item/' + self.model.get("id") + '/' + subtypefield + '/create_duplicates', null, {
                                success: function (result) {
                                    app.alert.dismiss('duplicate_creation');
                                },
                                error: function (error) {
                                    app.alert.show('link_wps_error', {
                                        level: 'error',
                                        messages: 'Failed Creating Duplicates!'
                                    });
                                }
                            })
                            app.navigate(self.context, self.model);
                            app.router.redirect('#II_Inventory_Item');
                        } else {
                            //alert('in "else condition 811');
                            app.navigate(self.context, self.model);
                        }

                    }, self));
                }, 700);
            },

            /**
             * checks whether a wp was selected
             */
            wpSelected: function () {
                if (this.model.get("related_to_c") !== "Sales") {
                    return true;
                }
                var wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                var wpView = wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("customWp-selection");

                if (wpView.massCollection && wpView.massCollection.length === 0) {
                    return false;
                }

                return true;

            },

            tsSelected: function () {
                if (this.model.get("related_to_c") !== "Multiple Test Systems") {
                    return true;
                }
                var tsLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ts-selection");
                var tsView = tsLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("customTs-selection");

                if (tsView.massCollection && tsView.massCollection.length === 0) {
                    return false;
                }

                var selectedTestSystem = tsView.massCollection;
                if (selectedTestSystem && selectedTestSystem.length > 0) {
                    var ts_ids = _.map(selectedTestSystem.models, function (model) {
                        return model.get("id")
                    });

                    var ts_names = _.map(selectedTestSystem.models, function (model) {
                        return model.get("name")
                    });


                    var subtype_chk = [];
                    $.each($("input[name='checkst']:checked"), function () {
                        subtype_chk.push($(this).val());
                    });
                    var subtype = subtype_chk.join(",");
                    console.log("Subtype == " + subtype);

                    return { status: true, TsId: ts_ids, TsName: ts_names, subtype: subtype };
                    //return ts_ids;
                }
            },
            /**
             * Make the api call that creates Work Product links to the current bean
             */
            linkWorkProducts: function () {
                return new Promise(function (resolve, reject) {
                    var modelId = this.model.get("id");
                    var wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                    var wpView = wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("customWp-selection");

                    var selectedWorkProducts = wpView.massCollection;

                    if (selectedWorkProducts && selectedWorkProducts.length > 0) {
                        app.alert.show('link_wps', {
                            level: 'info',
                            messages: 'Linking Work Products...'
                        });
                        var wp_ids = _.map(selectedWorkProducts.models, function (model) {
                            return model.get("id")
                        });
                        app.api.call("create", "rest/v10/II_Inventory_Item/" + modelId + "/link", {
                            link_name: "m03_work_product_ii_inventory_item_2",
                            ids: wp_ids
                        }, {
                            success: function () {
                                resolve("success");
                            },
                            error: function (error) {
                                resolve(error);
                            }
                        })
                    } else {
                        resolve("success");
                    }
                }.bind(this));
            },
            /** Added by laxmichand saini */
            showTestSystemOfWorkProduct: function (model) {
                if (this.layout.layout.isSidePaneVisible() === false) {
                    this.layout.layout.toggleSidePane();
                }
                $('.iiloadmore').remove();
                $('.loading-ii').remove();
                this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();

                var related_to = this.model.get('related_to_c');
                if (related_to == 'Multiple Test Systems') {
                    $('.ts_selection').find('.search-name').attr('placeholder', 'Search Test System......');
                    $('.ts_selection').find('.search-name').attr('aria-label', 'Search Test System......');
                    $('.ts_selection').find('.table-cell .search-filter').css('display', 'none');
                    $('.ts_selection').find('.table-cell .choice-filter').css('display', 'none');
                    $('.ts_selection').find('.table-cell.full-width').css('width', '380px');

                    this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();

                    var type2 = this.model.get("type_2");
                    if (type2 != "" && (type2 == "Equipment Facility Record" || type2 == "Block" || type2 == "Culture" || type2 == "Slide" || type2 == "Tissue")) {
                        this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").show();
                        var related_to = this.model.get('related_to_c');
                        var work_product = this.model.get('m03_work_product_ii_inventory_item_1_name');
                        var sales = this.model.get('m01_sales_ii_inventory_item_1_name');
                        if (related_to == 'Multiple Test Systems' && work_product != "") {
                            this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").show();
                            this.layout.layout.$el.find(".ts_selection").css("height", "250px");
                            this.layout.layout.$el.find(".ts_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "250px");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        } else if (related_to == 'Sales' && sales != "") {
                            this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").show();
                            this.layout.layout.$el.find(".wp_selection").css("height", "250px");
                            this.layout.layout.$el.find(".wp_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "250px");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        } else {
                            this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                            this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").hide();
                            this.layout.layout.$el.find(".wp_selection").css("height", "unset");
                            this.layout.layout.$el.find(".wp_selection").css("overflow", "unset");
                            this.layout.layout.$el.find(".ts_selection").css("height", "unset");
                            this.layout.layout.$el.find(".ts_selection").css("overflow", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        }
                    } else {
                        this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").hide();
                    }

                    this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").show();

                    var workProduct = this.model.get('m03_work_product_ii_inventory_item_1m03_work_product_ida');

                    $('.ts_selection').find('.search-name').attr('wpId', workProduct);
                    if (workProduct != "" || workProduct != undefined || workProduct != null) {

                        app.api.call("create", "rest/v10/get_wpa_test_system", {
                            workProduct: workProduct,
                            record_count: 0,
                        }, {
                            success: function (tsRecords) {
                                console.log('tsRecords1', tsRecords);

                                tsLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ts-selection");
                                tsView = tsLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("customTs-selection");
                                var tsModels = [];

                                if (tsRecords.records && tsRecords.records.length > 0) {

                                    tsModels = tsRecords.records;
                                    setTimeout(function () {
                                        if ($('button[data-action="show-more-ii-sidecar"]').length == 0) {
                                            if (tsRecords.records.length > 19) {
                                                $("div.main-pane.ts_selection div.main-content").append('<div class="iiloadmore"><button record_count="20" wpId="' + workProduct + '" data-action="show-more-ii-sidecar" class="btn btn-link btn-invisible more padded get-ii-sidecar-count">More Inventory Item...</button></div><div class="block-footer loading-ii hide"><div class="loading">Loading...</div></div>');
                                            }
                                        }
                                    }, 500);
                                }

                                tsView.collection.reset();
                                tsView.collection.add(tsModels);
                                tsView.render();

                                this.layout.layout.$el.find(".ts_selection .dataTable td:first-child span").css("margin-left", "2px");
                                //this.layout.layout.$el.find(".ts_selection .dataTable td:first-child span").css("margin-top", "-13px");
                                //this.layout.layout.$el.find(".ts_selection .dataTable td:first-child span").css("padding", "0px 12px");
                                this.layout.layout.$el.find(".ts_selection .dataTable td:nth-child(2) span").css("line-height", "21px");
                                this.layout.layout.$el.find(".ts_selection .btn.checkall").css("border", "none");
                                this.layout.layout.$el.find(".ts_selection table th").removeClass("orderByname");
                                this.layout.layout.$el.find(".ts_selection table th").removeClass("orderByusda_id_c");
                                this.layout.layout.$el.find(".ts_selection table th.sorting").css("cursor", "unset");



                            }.bind(this),
                            error: function (error) {
                                app.alert.show("ts_wp_error", {
                                    level: "error",
                                    messages: "Error retrieving Test System of Work Products",
                                    autoClose: true
                                });
                            }
                        });
                    } else {
                        $('.iiloadmore').html('');
                        $('.loading-ii').remove();
                    }

                    /*var workProduct = this.model.get('m03_work_product_ii_inventory_item_1_name');

                    app.api.call("read", "rest/v10/ANML_Animals?fields=name,usda_id_c,enrollment_status&filter[0][assigned_to_wp_c][$equals]=" + workProduct, null, {
                        success: function(tsRecords) {
                            var tsLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ts-selection");
                            var tsView = tsLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("customTs-selection");
                            var tsModels = [];

                            if (tsRecords.records && tsRecords.records.length > 0) {
                                tsModels = tsRecords.records;
                            }

                            tsView.collection.reset();
                            tsView.collection.add(tsModels);
                            tsView.render();

                            this.layout.layout.$el.find(".ts_selection .dataTable td:first-child span").css("margin-top", "-13px");
                            this.layout.layout.$el.find(".ts_selection .dataTable td:first-child span").css("padding", "0px 12px");
                            this.layout.layout.$el.find(".ts_selection .dataTable td:nth-child(2) span").css("line-height", "21px");
                            this.layout.layout.$el.find(".ts_selection .btn.checkall").css("border", "none");
                        }.bind(this),
                        error: function(error) {
                            console.log("Error: " + error);
                            app.alert.show("ts_wp_error", {
                                level: "error",
                                messages: "Error retrieving Test System of Work Products",
                                autoClose: true
                            });
                        }
                    });*/
                } else if (related_to == 'Work Product') {
                    this.model.set("category", null);
                    this.model.set("type_2", null);
                } else if (related_to == 'Single Test System') {
                    this.model.set("category", null);
                    this.model.set("type_2", null);
                    this.model.set("anml_animals_ii_inventory_item_1anml_animals_ida", null);
                    this.model.set("anml_animals_ii_inventory_item_1_name", null);

                    var workProduct = this.model.get('m03_work_product_ii_inventory_item_1m03_work_product_ida');
                    var TsNameStr = '';
                    if (workProduct) {
                        console.log("workProduct1 ");

                        app.alert.show('single_testsystem_id', {
                            level: 'process',
                            title: 'In Process...'
                        });

                        app.api.call("create", "rest/v10/get_wpa_test_system", {
                            workProduct: workProduct,
                            relatedFrom: 'single_testsystem',
                            record_count: 'all',
                        }, {
                            success: function (tsRecords) {
                                console.log('tsRecords 1-> ', tsRecords);

                                if (tsRecords.records && tsRecords.records.length > 0) {

                                    this.model.set('ts_hidden_string_c', '');

                                    var AllTsName = [];
                                    $.each(tsRecords.records, function (index, item) {
                                        AllTsName.push(item.name);
                                    });

                                    console.log('length', AllTsName.length);
                                    TsNameStr = AllTsName.toString();
                                    this.model.set('ts_hidden_string_c', TsNameStr);
                                    if (this.model.get('ts_hidden_string_c') != "") {
                                        app.alert.dismiss('single_testsystem_id');
                                    }
                                    app.alert.dismiss('single_testsystem_id');

                                } else {
                                    app.alert.dismiss('single_testsystem_id');
                                    this.model.set('ts_hidden_string_c', '');
                                }

                            }.bind(this),
                            error: function (error) {

                            }
                        });
                    } else {
                        this.model.set('ts_hidden_string_c', '');
                    }
                }
            },
            /* Make subpanel for SubType*/
            getSubtypeSubpanel: function (model) {
                //alert('hi');
                var type2 = this.model.get("type_2");
                var category = this.model.get("category");
                var pk_samples_c = this.model.get("pk_samples_c");


                /*09 Dec 2020 :Gurpreet : #390 : Inventory Item - field dependencies/mirroring */
                if (category == 'Study Article' && type2 == 'Test Article') {
                    //alert('test article');
                    var II_Owner_List = app.lang.getAppListStrings('inventory_item_owner_list');

                    Object.keys(II_Owner_List).forEach(function (key) {
                        delete II_Owner_List[key];
                    });
                    //II_Owner_List[''] = "";
                    II_Owner_List['Client Owned'] = "Client Owned";
                    this.model.fields['owner'].options = II_Owner_List;
                    this.model.set('owner', "Client Owned");
                    this.render();
                    // $("div#s2id_autogen11").css('pointer-events', 'none');

                } else if ((category == 'Study Article') && (type2 == 'Control Article' || type2 == 'Accessory Article')) {
                    this.model.set('owner', '');
                    var II_Owner_List = app.lang.getAppListStrings('inventory_item_owner_list');
                    console.log('II_Owner_List', II_Owner_List);
                    Object.keys(II_Owner_List).forEach(function (key) {
                        delete II_Owner_List[key];
                    });
                    //II_Owner_List[''] = "";
                    II_Owner_List['APS Owned'] = "APS Owned";
                    II_Owner_List['APS Supplied Client Owned'] = "APS Supplied Client Owned";
                    II_Owner_List['Client Owned'] = "Client Owned";
                    this.model.fields['owner'].options = II_Owner_List;
                    //this.model.set('owner','');
                    this.render();
                    if (this.model.get('related_to_c') == 'Sales')
                        this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").show();

                }
                /* //////// */

                if (type2 == "Equipment Facility Record") {

                    if (this.layout.layout.isSidePaneVisible() === false) {
                        this.layout.layout.toggleSidePane();
                    }
                    $('.subtype_selection .main-content div').html('');

                    var related_to = this.model.get('related_to_c');
                    var work_product = this.model.get('m03_work_product_ii_inventory_item_1_name');
                    var sales = this.model.get('m01_sales_ii_inventory_item_1_name');
                    if (related_to == 'Multiple Test Systems') {

                        if (work_product != "") {
                            this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").show();
                            this.layout.layout.$el.find(".ts_selection").css("height", "250px");
                            this.layout.layout.$el.find(".ts_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "250px");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        } else {
                            this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").hide();
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        }


                    } else if (related_to == 'Sales') {

                        if (sales != "" && sales != null) {
                            this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").show();
                            this.layout.layout.$el.find(".wp_selection").css("height", "250px");
                            this.layout.layout.$el.find(".wp_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "250px");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        } else {
                            this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        }


                    } else {
                        this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").hide();
                        this.layout.layout.$el.find(".wp_selection").css("height", "unset");
                        this.layout.layout.$el.find(".wp_selection").css("overflow", "unset");
                        this.layout.layout.$el.find(".ts_selection").css("height", "unset");
                        this.layout.layout.$el.find(".ts_selection").css("overflow", "unset");
                        this.layout.layout.$el.find(".subtype_selection").css("margin-top", "unset");
                        this.layout.layout.$el.find(".subtype_selection").css("overflow", "unset");
                        this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                    }


                    this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").show();
                    this.layout.layout.$el.find(".subtype_selection").addClass("span12");
                    this.layout.layout.$el.find(".subtype_selection").css("width", "100%");


                    var dropdowndata = App.lang.getAppListStrings("inventory_item_subtype_list");

                    $('.subtype_selection .main-content div').html('');
                    var data = "";
                    data += '<table class="table table-striped dataTable search-and-select"><thead><th style="width: 5%;min-width:55px;border-top: 1px solid #ddd;"><div class="checkall" data-check="all" tabindex="-1"><input type="checkbox" name="checkAllst" id="checkAllst" class="toggle-all checkAllst subtypecls" onchange="onChangeSubtype(this);" style="margin-left:26px;"></div></th><th style="width: 95%;border-top: 1px solid #ddd;"><span>Subtype Record</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                    var inc = 1;
                    $.each(dropdowndata, function (key, item) {
                        if (item != '') {
                            data += '<tr name="' + item + '" class="single" tabindex="-1"><td data-column=""><input class="subtypecls" data-check="one" type="checkbox" name="checkst" value="' + key + '" id="st' + inc + '" style="margin-left:26px;"></td><td data-type="name" data-column="name"><div class="ellipsis_inline" data-placement="bottom" title="' + item + '">' + item + '</div></td></tr>';
                        }
                        inc++;
                    });
                    data += '</tbody><tfoot> </tfoot></table>';
                    $('.subtype_selection .main-content div').html(data);


                } else if (type2 == "Block" || type2 == "Culture" || type2 == "Slide" || type2 == "Tissue" && pk_samples_c != "No") {

                    if (this.layout.layout.isSidePaneVisible() === false) {
                        this.layout.layout.toggleSidePane();
                    }
                    $('.subtype_selection .main-content div').html('');

                    var related_to = this.model.get('related_to_c');
                    var work_product = this.model.get('m03_work_product_ii_inventory_item_1_name');
                    var sales = this.model.get('m01_sales_ii_inventory_item_1_name');
                    if (related_to == 'Multiple Test Systems') {
                        if (work_product != "") {
                            this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").show();
                            this.layout.layout.$el.find(".ts_selection").css("height", "250px");
                            this.layout.layout.$el.find(".ts_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "250px");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        } else {
                            this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").hide();
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        }


                    } else if (related_to == 'Sales') {

                        if (sales != "" && sales != null) {
                            this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").show();
                            this.layout.layout.$el.find(".wp_selection").css("height", "250px");
                            this.layout.layout.$el.find(".wp_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "250px");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "scroll");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        } else {
                            this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                            this.layout.layout.$el.find(".subtype_selection").css("margin-top", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("overflow", "unset");
                            this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                        }


                    } else {
                        this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ts_selection").hide();
                        this.layout.layout.$el.find(".wp_selection").css("height", "unset");
                        this.layout.layout.$el.find(".wp_selection").css("overflow", "unset");
                        this.layout.layout.$el.find(".ts_selection").css("height", "unset");
                        this.layout.layout.$el.find(".ts_selection").css("overflow", "unset");
                        this.layout.layout.$el.find(".subtype_selection").css("margin-top", "unset");
                        this.layout.layout.$el.find(".subtype_selection").css("overflow", "unset");
                        this.layout.layout.$el.find(".subtype_selection").css("width", "100%");
                    }

                    this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").show();
                    this.layout.layout.$el.find(".subtype_selection").addClass("span12");
                    this.layout.layout.$el.find(".subtype_selection").css("width", "100%");


                    var dropdowndata = App.lang.getAppListStrings("tissues_list");

                    $('.subtype_selection .main-content div').html('');
                    var data = "";
                    data += '<table class="table table-striped dataTable search-and-select"><thead><tr><th style="width: 5%;min-width:55px;border-top: 1px solid #ddd;"><div class="checkall" data-check="all" tabindex="-1"><input type="checkbox" name="checkAllst" id="checkAllst" class="toggle-all checkAllst subtypecls" onchange="onChangeSubtype(this);" style="margin-left:26px;"></div></th><th style="width: 95%;border-top: 1px solid #ddd;"><span>Subtype Specimen</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                    var inc = 1;
                    $.each(dropdowndata, function (key, item) {
                        if (item != '') {
                            data += '<tr name="' + item + '" class="single" tabindex="-1"><td data-column=""><input class="subtypecls" data-check="one" type="checkbox" name="checkst" value="' + key + '" id="st' + inc + '" style="margin-left:26px;"></td><td data-type="name" data-column="name"><div class="ellipsis_inline" data-placement="bottom" title="' + item + '">' + item + '</div></td></tr>';
                        }
                        inc++;
                    });
                    data += '</tbody><tfoot> </tfoot></table>';
                    $('.subtype_selection .main-content div').html(data);

                } else if (category == "Product") { } else {
                    if (this.layout.layout.isSidePaneVisible() === false) {
                        this.layout.layout.toggleSidePane();
                    }
                    $('.subtype_selection .main-content div').html('');
                    this.layout.layout.$el.find(".wp_selection").css("height", "unset");
                    this.layout.layout.$el.find(".wp_selection").css("overflow", "unset");
                    this.layout.layout.$el.find(".ts_selection").css("height", "unset");
                    this.layout.layout.$el.find(".ts_selection").css("overflow", "unset");
                    this.layout.layout.$el.find("[data-component=sidebar] .subtype_selection").hide();
                }

            },

            /*Added by gsing : #149 : 24 Aug 2021 */
            getReceivedItemDetails: function (model) {
                var related_to = this.model.get('related_to_c');
                var rid = this.model.get("ri_received_items_ii_inventory_item_1ri_received_items_ida");
                if (rid != "") {
                    var url = app.api.buildURL("pull_poi_ori_from_ri");
                    var method = "create";
                    var data = {
                        module: 'RI_Received_Items',
                        recordId: rid,
                    };
                    var callback = {
                        success: _.bind(function successCB(res) {
                            if (related_to == "Purchase Order Item") {
                                console.log('related_to poi : ', related_to);
                                this.model.set("poi_purcha8945er_item_ida", res.poi_id);
                                this.model.set("poi_purchase_order_item_ii_inventory_item_1_name", res.poi_name);
                            }
                            else if (related_to == "Order Request Item") {
                                console.log('related_to ori : ', related_to);
                                this.model.set("ori_order_2123st_item_ida", res.ori_id);
                                this.model.set("ori_order_request_item_ii_inventory_item_1_name", res.ori_name);
                            }
                            if (res.sale_id != "" && res.sale_name != "") {
                                console.log('sale id : ', res.sale_id);
                                this.model.set("m01_sales_ii_inventory_item_1m01_sales_ida", res.sale_id);
                                this.model.set("m01_sales_ii_inventory_item_1_name", res.sale_name);
                            }
                            if (res.wp_id != "" && res.wp_name != "") {
                                console.log('wp id : ', res.wp_id);
                                this.model.set("m03_work_product_ii_inventory_item_1m03_work_product_ida", res.wp_id);
                                this.model.set("m03_work_product_ii_inventory_item_1_name", res.wp_name);
                            }
                            /**#390 : 26 aug 2021 Pull product information and set to II products fields */
                            if (res.type != '') {
                                this.model.set('type_2', res.type);
                                $('input[name="type_2"]').prop('disabled', true);
                            }
                            if (res.subtype != '') {
                                this.model.set('subtype', res.subtype);
                                $('input[name="subtype"]').prop('disabled', true);
                            }
                            if (res.owner != '') {
                                this.model.set('owner', res.owner);
                                $('input[name="owner"]').prop('disabled', true);
                            }
                            if (res.product_id != '') {
                                this.model.set('product_id_2_c', res.product_id);
                                $('input[name="product_id_2_c"]').prop('disabled', true);
                            }
                            if (res.product_name != '') {
                                this.model.set('product_name', res.product_name);
                                $('input[name="product_name"]').prop('disabled', true);
                            }
                            if (res.manufacturer != '') {
                                this.model.set('manufacturer_c', res.manufacturer);
                                $('input[name="manufacturer_c"]').prop('disabled', true);
                            }
                            if (res.external_barcode != '') {
                                this.model.set('external_barcode', res.external_barcode);
                                $('input[name="external_barcode"]').prop('disabled', true);
                            }
                            if (res.concentration != '') {
                                this.model.set('concentration_c', res.concentration);
                                $('input[name="concentration_c"]').prop('disabled', true);
                            }
                            if (res.concentration_unit != '') {
                                this.model.set('u_units_id_c', res.u_units_id_c);
                                this.model.set('concentration_unit_c', res.concentration_unit);
                                $('input[name="concentration_unit_c"]').prop('disabled', true);
                            }
                            if (res.allowed_storage != '') {
                                this.model.set('allowed_storage_conditions', res.allowed_storage);
                                $('input[name="allowed_storage_conditions"]').prop('disabled', true);
                            }

                        }, this)
                    };
                    app.api.call(method, url, data, callback);
                }
            },
        })
    });
})(SUGAR.App);