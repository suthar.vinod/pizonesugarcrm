/**
 * Extend the subpanel layout in order to remove the Work Products Subpanel if the "Related To" field is Work Product
 */

;
(function (app) {
    app.events.on("app:start", function () {
        if (!app.view.layouts.BaseSubpanelsLayout && !app.view.layouts.BaseCustomSubpanelsLayout) {
            app.view.declareComponent("layout", "subpanels", undefined, undefined, false, "base");
        }

        var subpanelsLayout = "BaseSubpanelsLayout";

        if (app.view.layouts.BaseCustomSubpanelsLayout) {
            subpanelsLayout = "BaseCustomSubpanelsLayout";
        }

        if (App.view.layouts[subpanelsLayout].subpanelsExtended === true) {
            return;
        }

        App.view.layouts[subpanelsLayout] = App.view.layouts[subpanelsLayout].extend({
            subpanelsExtended: true,
            _hiddenSubpanels: [],
            hideLink: "m03_work_product_ii_inventory_item_1",

            initialize: function (options) {
                this._super("initialize", arguments);
                if (this.model.module == "II_Inventory_Item") {

                    app.api.call("read", "rest/v10/II_Inventory_Item/" + this.model.id + "?fields=related_to_c", null, {
                        success: function (model) {
                            if (model && model.related_to_c && (model.related_to_c === "Work Product" || model.related_to_c === "Single Test System" || model.related_to_c === "Multiple Test Systems" || model.related_to_c === "Internal Use")) {
                                this._hideSubpanel(this.hideLink);
                            } else if (model && model.related_to_c && model.related_to_c === "Sales") {
                                this._removeActionButtons(this.hideLink);
                            }
                        }.bind(this)
                    })
                }
            },


            _removeActionButtons: function (link) {

                var component = this._getSubpanelByLink(link);
                component.$el.find('.pull-right').hide();
                component.getComponent("subpanel-for-ii_inventory_item-m03_work_product_ii_inventory_item_1").on("render:rows",
                    function () {
                        this.$el.find('[data-subpanel-link="m03_work_product_ii_inventory_item_1"] a.dropdown-toggle').hide();
                    }.bind(this));
            },

            /**
             * Add to the _hiddenSubpanels array, and hide the subpanel
             */
            _hideSubpanel: function (link) {
                this._hiddenSubpanels.push(link);
                var component = this._getSubpanelByLink(link);
                if (!_.isUndefined(component)) {
                    component.hide();
                }
                this._hiddenSubpanels = _.unique(this._hiddenSubpanels);
            },

            /**
             * Helper for getting the Subpanel View for a specific link
             */
            _getSubpanelByLink: function (link) {
                return this._components.find(function (component) {
                    return component.context.get('link') === link;
                });
            },

        })
    });
})(SUGAR.App);