<?php

namespace Sugarcrm\Sugarcrm\custom\wsystems\InventoryItemCreation\Utils;

class InventoryItemCreateDuplicatesOriUtils {

    public function __construct(String $module, String $record_id, String $oriName, String $oriType2, String $oriSubtype, String $orisalesId, String $oriwpId, String $oriId) {
        $this->module = $module;
        $this->record_id = $record_id;
        $this->oriName = $oriName;
        $this->oriType2 = $oriType2;
        $this->oriSubtype = $oriSubtype;
        $this->orisalesId = $orisalesId;
        $this->oriId = $oriId;

        $this->bean = \BeanFactory::retrieveBean($module, $record_id);
    }

    public function duplicate() {

        global $db, $app_list_strings;

        $II_product_category_list_dom = $app_list_strings['product_category_list'];
        $II_pro_subtype_list_dom = $app_list_strings['pro_subtype_list'];

        $stroriName = explode(',', $this->oriName);
        $stroriType2 = explode(',', $this->oriType2);
        $stroriSubtype = explode(',', $this->oriSubtype);
        $strorisalesId = explode(',', $this->orisalesId);
        $stroriwpId = explode(',', $this->oriwpId);
        $stroriId = explode(',', $this->oriId);

        $clonedBean = \BeanFactory::getBean('II_Inventory_Item', $this->bean->id);
        $clonedBeanName = substr($clonedBean->name, 0, -3);

        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
        $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
        $clonedBean->load_relationship("ori_order_request_item_ii_inventory_item_1");

        //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");
        //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

        $clonedBean->ori_order_request_item_ii_inventory_item_1->add($stroriId[0]);

        if (count($stroriName) > 1) {

            for ($i = 1; $i < count($stroriName); $i++) {

                $SaleName = "";
                $WpName = "";
                if ($strorisalesId[$i] != '' && $strorisalesId[$i] == '0') {
                    
                } else {
                    $sqlSales = 'SELECT name FROM m01_sales WHERE id = "' . $strorisalesId[$i] . '"';
                    $resultSales = $db->query($sqlSales);
                    if ($resultSales->num_rows > 0) {
                        $rowSales = $db->fetchByAssoc($resultSales);
                        $SaleName = $rowSales['name'];
                    }
                }

                if ($stroriwpId[$i] != '' && $stroriwpId[$i] == '0') {
                    
                } else {
                    $sqlWp = 'SELECT name FROM m03_work_product WHERE id = "' . $stroriwpId[$i] . '"';
                    $resultWp = $db->query($sqlWp);
                    if ($resultWp->num_rows > 0) {
                        $rowWp = $db->fetchByAssoc($resultWp);
                        $WpName = $rowWp['name'];
                    }
                }

                $ProductName = " " . $clonedBean->product_name;

                $tp = $stroriType2[$i];
                if ($stroriType2[$i] != '' && $stroriType2[$i] == '0') {
                    
                } else {
                    $Type_2 = $II_product_category_list_dom[$tp];
                    $TypeName = " " . $Type_2;
                }

                $Subtype = $stroriSubtype[$i];
                if ($Subtype != '' && $Subtype == '0') {
                    $SubtypeName = "";
                    $Subtype_Name = "";
                } else {
                    $SubType_2 = $II_pro_subtype_list_dom[$Subtype];
                    $SubtypeName = " " . $SubType_2;
                    $Subtype_Name = $Subtype;
                }

                $BeanName = $SaleName . $WpName . $ProductName . $TypeName . $SubtypeName;


                $bid = create_guid();
                $clonedBean->id = $bid;
                $clonedBean->new_with_id = true;

                $clonedBean->name = $BeanName;
                $clonedBean->type_2 = $stroriType2[$i];
                $clonedBean->subtype = $Subtype_Name;
                $clonedBean->fetched_row = null;
                $clonedBean->unique_id_c = $this->generateRandomString(6);
                $clonedBean->save();

                $clonedBean->ori_order_request_item_ii_inventory_item_1->add($stroriId[$i]);

                if ($strorisalesId[$i] != '' && $strorisalesId[$i] == '0') {
                    
                } else {
                    $clonedBean->m01_sales_ii_inventory_item_1->add($strorisalesId[$i]);
                }

                if ($stroriwpId[$i] != '' && $stroriwpId[$i] == '0') {
                    
                } else {
                    $clonedBean->m03_work_product_ii_inventory_item_1->add($stroriwpId[$i]);
                }

                //$clonedBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id);

                if ($i > 1) {
                    //Audit log for related_to_c
                    if ($clonedBean->related_to_c != "" || $clonedBean->related_to_c != null) {
                        $this->createAuditLog($bid, "related_to_c", $clonedBean->related_to_c, 'enum');
                    }
                    //Audit log for category
                    if ($clonedBean->category != "" || $clonedBean->category != null) {
                        $this->createAuditLog($bid, "category", $clonedBean->category, 'enum');
                    }
                    //Audit log for description
                    if ($clonedBean->description != "" || $clonedBean->description != null) {
                        $this->createAuditLog($bid, "description", $clonedBean->description, 'varchar');
                    }
                    //Audit log for owner
                    if ($clonedBean->owner != "" || $clonedBean->owner != null) {
                        $this->createAuditLog($bid, "owner", $clonedBean->owner, 'enum');
                    }
                    //Audit log for lot_number_c
                    if ($clonedBean->lot_number_c != "" || $clonedBean->lot_number_c != null) {
                        $this->createAuditLog($bid, "lot_number_c", $clonedBean->lot_number_c, 'varchar');
                    }
                    //Audit log for manufacturer_c
                    if ($clonedBean->manufacturer_c != "" || $clonedBean->manufacturer_c != null) {
                        $this->createAuditLog($bid, "manufacturer_c", $clonedBean->manufacturer_c, 'varchar');
                    }
                    //Audit log for external_barcode
                    if ($clonedBean->external_barcode != "" || $clonedBean->external_barcode != null) {
                        $this->createAuditLog($bid, "external_barcode", $clonedBean->external_barcode, 'varchar');
                    }
                    //Audit log for concentration_c
                    if ($clonedBean->concentration_c != "" || $clonedBean->concentration_c != null) {
                        $this->createAuditLog($bid, "concentration_c", $clonedBean->concentration_c, 'decimal');
                    }
                    //Audit log for concentration_unit_c
                    if ($clonedBean->concentration_unit_c != "" || $clonedBean->concentration_unit_c != null) {
                        $this->createAuditLog($bid, "concentration_unit_c", $clonedBean->concentration_unit_c, 'relate');
                    }
                    //Audit log for sterilization
                    if ($clonedBean->sterilization != "" || $clonedBean->sterilization != null) {
                        $this->createAuditLog($bid, "sterilization", $clonedBean->sterilization, 'enum');
                    }
                    //Audit log for timepoint_type
                    if ($clonedBean->timepoint_type != "" || $clonedBean->timepoint_type != null) {
                        $this->createAuditLog($bid, "timepoint_type", $clonedBean->timepoint_type, 'enum');
                    }
                    //Audit log for timepoint_integer
                    if ($clonedBean->timepoint_integer != "" || $clonedBean->timepoint_integer != null) {
                        $this->createAuditLog($bid, "timepoint_integer", $clonedBean->timepoint_integer, 'int');
                    }
                    //Audit log for timepoint_unit
                    if ($clonedBean->timepoint_unit != "" || $clonedBean->timepoint_unit != null) {
                        $this->createAuditLog($bid, "timepoint_unit", $clonedBean->timepoint_unit, 'relate');
                    }
                    //Audit log for processing_method
                    if ($clonedBean->processing_method != "" || $clonedBean->processing_method != null) {
                        $this->createAuditLog($bid, "processing_method", $clonedBean->processing_method, 'enum');
                    }
                    //Audit log for collection_date_time
                    if ($clonedBean->collection_date_time != "" || $clonedBean->collection_date_time != null) {
                        $this->createAuditLog($bid, "collection_date_time", $clonedBean->collection_date_time, 'datetime');
                    }
                    //Audit log for expiration_test_article
                    if ($clonedBean->expiration_test_article != "" || $clonedBean->expiration_test_article != null) {
                        $this->createAuditLog($bid, "expiration_test_article", $clonedBean->expiration_test_article, 'enum');
                    }
                    //Audit log for expiration_date
                    if ($clonedBean->expiration_date != "" || $clonedBean->expiration_date != null) {
                        $this->createAuditLog($bid, "expiration_date", $clonedBean->expiration_date, 'date');
                    }
                    //Audit log for days_to_expiration
                    if ($clonedBean->days_to_expiration != "" || $clonedBean->days_to_expiration != null) {
                        $this->createAuditLog($bid, "days_to_expiration", $clonedBean->days_to_expiration, 'int');
                    }
                    //Audit log for location_building
                    if ($clonedBean->location_building != "" || $clonedBean->location_building != null) {
                        $this->createAuditLog($bid, "location_building", $clonedBean->location_building, 'enum');
                    }
                    //Audit log for location_room
                    if ($clonedBean->location_room != "" || $clonedBean->location_room != null) {
                        $this->createAuditLog($bid, "location_room", $clonedBean->location_room, 'relate');
                    }
                    //Audit log for location_equipment
                    if ($clonedBean->location_equipment != "" || $clonedBean->location_equipment != null) {
                        $this->createAuditLog($bid, "location_equipment", $clonedBean->location_equipment, 'relate');
                    }
                    //Audit log for location_shelf
                    if ($clonedBean->location_shelf != "" || $clonedBean->location_shelf != null) {
                        $this->createAuditLog($bid, "location_shelf", $clonedBean->location_shelf, 'enum');
                    }
                    //Audit log for location_cabinet
                    if ($clonedBean->location_cabinet != "" || $clonedBean->location_cabinet != null) {
                        $this->createAuditLog($bid, "location_cabinet", $clonedBean->location_cabinet, 'enum');
                    }
                    //Audit log for ship_to_contact
                    if ($clonedBean->ship_to_contact != "" || $clonedBean->ship_to_contact != null) {
                        $this->createAuditLog($bid, "ship_to_contact", $clonedBean->ship_to_contact, 'relate');
                    }
                    //Audit log for ship_to_company
                    if ($clonedBean->ship_to_company != "" || $clonedBean->ship_to_company != null) {
                        $this->createAuditLog($bid, "ship_to_company", $clonedBean->ship_to_company, 'relate');
                    }
                    //Audit log for ship_to_address
                    if ($clonedBean->ship_to_address != "" || $clonedBean->ship_to_address != null) {
                        $this->createAuditLog($bid, "ship_to_address", $clonedBean->ship_to_address, 'relate');
                    }
                    //Audit log for retention_start_date
                    if ($clonedBean->retention_start_date != "" || $clonedBean->retention_start_date != null) {
                        $this->createAuditLog($bid, "retention_start_date", $clonedBean->retention_start_date, 'date');
                    }
                    //Audit log for retention_interval_days
                    if ($clonedBean->retention_interval_days != "" || $clonedBean->retention_interval_days != null) {
                        $this->createAuditLog($bid, "retention_interval_days", $clonedBean->retention_interval_days, 'int');
                    }
                    //Audit log for planned_discard_date
                    if ($clonedBean->planned_discard_date != "" || $clonedBean->planned_discard_date != null) {
                        $this->createAuditLog($bid, "planned_discard_date", $clonedBean->planned_discard_date, 'date');
                    }
                    //Audit log for actual_discard_date
                    if ($clonedBean->actual_discard_date != "" || $clonedBean->actual_discard_date != null) {
                        $this->createAuditLog($bid, "actual_discard_date", $clonedBean->actual_discard_date, 'date');
                    }
                    //Audit log for quantity_c
                    if ($clonedBean->quantity_c != "" || $clonedBean->quantity_c != null) {
                        $this->createAuditLog($bid, "quantity_c", $clonedBean->quantity_c, 'int');
                    }
                    //Audit log for expiration_date_time
                    if ($clonedBean->expiration_date_time != "" || $clonedBean->expiration_date_time != null) {
                        $this->createAuditLog($bid, "expiration_date_time", $clonedBean->expiration_date_time, 'datetmie');
                    }
                    //Audit log for type_2
                    if ($clonedBean->type_2 != "" || $clonedBean->type_2 != null) {
                        $this->createAuditLog($bid, "type_2", $stroriType2[$i], 'enum');
                    }
                    //Audit log for product_name
                    if ($clonedBean->product_name != "" || $clonedBean->product_name != null) {
                        $this->createAuditLog($bid, "product_name", $clonedBean->product_name, 'varchar');
                    }
                    //Audit log for allowed_storage_condition
                    if ($clonedBean->allowed_storage_conditions != "" || $clonedBean->allowed_storage_conditions != null) {
                        $this->createAuditLog($bid, "allowed_storage_conditions", $clonedBean->allowed_storage_conditions, 'multienum');
                    }
                    //Audit log for allowed_storage_medium
                    if ($clonedBean->allowed_storage_medium != "" || $clonedBean->allowed_storage_medium != null) {
                        $this->createAuditLog($bid, "allowed_storage_medium", $clonedBean->allowed_storage_medium, 'multienum');
                    }
                    //Audit log for subtype
                    if ($clonedBean->subtype != "" || $clonedBean->subtype != null) {
                        $this->createAuditLog($bid, "subtype", $Subtype_Name, 'enum');
                    }
                    //Audit log for collection_date_time_type
                    if ($clonedBean->collection_date_time_type != "" || $clonedBean->collection_date_time_type != null) {
                        $this->createAuditLog($bid, "collection_date_time_type", $clonedBean->collection_date_time_type, 'enum');
                    }
                    //Audit log for storage_medium
                    if ($clonedBean->storage_medium != "" || $clonedBean->storage_medium != null) {
                        $this->createAuditLog($bid, "storage_medium", $clonedBean->storage_medium, 'enum');
                    }
                    //Audit log for collection_date
                    if ($clonedBean->collection_date != "" || $clonedBean->collection_date != null) {
                        $this->createAuditLog($bid, "collection_date", $clonedBean->collection_date, 'date');
                    }
                    //Audit log for received_date
                    if ($clonedBean->received_date != "" || $clonedBean->received_date != null) {
                        $this->createAuditLog($bid, "received_date", $clonedBean->received_date, 'date');
                    }
                    //Audit log for storage_condition
                    if ($clonedBean->storage_condition != "" || $clonedBean->storage_condition != null) {
                        $this->createAuditLog($bid, "storage_condition", $clonedBean->storage_condition, 'enum');
                    }
                    //Audit log for location_type
                    if ($clonedBean->location_type != "" || $clonedBean->location_type != null) {
                        $this->createAuditLog($bid, "location_type", $clonedBean->location_type, 'enum');
                    }
                    //Audit log for status
                    if ($clonedBean->status != "" || $clonedBean->status != null) {
                        $this->createAuditLog($bid, "status", $clonedBean->status, 'enum');
                    }
                    //Audit log for stability_considerations_c
                    if ($clonedBean->stability_considerations_c != "" || $clonedBean->stability_considerations_c != null) {
                        $this->createAuditLog($bid, "stability_considerations_c", $clonedBean->stability_considerations_c, 'enum');
                    }
                    //Audit log for analyze_by_date_c
                    if ($clonedBean->analyze_by_date_c != "" || $clonedBean->analyze_by_date_c != null) {
                        $this->createAuditLog($bid, "analyze_by_date_c", $clonedBean->analyze_by_date_c, 'date');
                    }
                    //Audit log for test_type_c
                    if ($clonedBean->test_type_c != "" || $clonedBean->test_type_c != null) {
                        $this->createAuditLog($bid, "test_type_c", $clonedBean->test_type_c, 'enum');
                    }
                    //Audit log for sterilization_exp_date_c
                    if ($clonedBean->sterilization_exp_date_c != "" || $clonedBean->sterilization_exp_date_c != null) {
                        $this->createAuditLog($bid, "sterilization_exp_date_c", $clonedBean->sterilization_exp_date_c, 'date');
                    }
                    //Audit log for product_id_2_c
                    if ($clonedBean->product_id_2_c != "" || $clonedBean->product_id_2_c != null) {
                        $this->createAuditLog($bid, "product_id_2_c", $clonedBean->product_id_2_c, 'varchar');
                    }
                }
            }
        } else {
            if (is_numeric($this->bean->quantity_c) && intval($this->bean->quantity_c) > 0) {
                $quantity = intval($this->bean->quantity_c);
                for ($i = 0; $i < $quantity - 1; $i++) {
                    $bid = create_guid();
                    $clonedBean->id = $bid;
                    $clonedBean->new_with_id = true;

                    $clonedBean->name = substr($clonedBean->name, 0, -3);
                    $clonedBean->fetched_row = null;

                    $clonedBean->unique_id_c = $this->generateRandomString(6);

                    $clonedBean->save();

                    $clonedBean->ori_order_request_item_ii_inventory_item_1->add($stroriId[0]);

                    $clonedBean->m01_sales_ii_inventory_item_1->add($strorisalesId[0]);
                    $clonedBean->m03_work_product_ii_inventory_item_1->add($stroriwpId[0]);

                    //$clonedBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id);

                    if ($i > 1) {
                        //Audit log for related_to_c
                        if ($clonedBean->related_to_c != "" || $clonedBean->related_to_c != null) {
                            $this->createAuditLog($bid, "related_to_c", $clonedBean->related_to_c, 'enum');
                        }
                        //Audit log for category
                        if ($clonedBean->category != "" || $clonedBean->category != null) {
                            $this->createAuditLog($bid, "category", $clonedBean->category, 'enum');
                        }
                        //Audit log for description
                        if ($clonedBean->description != "" || $clonedBean->description != null) {
                            $this->createAuditLog($bid, "description", $clonedBean->description, 'varchar');
                        }
                        //Audit log for owner
                        if ($clonedBean->owner != "" || $clonedBean->owner != null) {
                            $this->createAuditLog($bid, "owner", $clonedBean->owner, 'enum');
                        }
                        //Audit log for lot_number_c
                        if ($clonedBean->lot_number_c != "" || $clonedBean->lot_number_c != null) {
                            $this->createAuditLog($bid, "lot_number_c", $clonedBean->lot_number_c, 'varchar');
                        }
                        //Audit log for manufacturer_c
                        if ($clonedBean->manufacturer_c != "" || $clonedBean->manufacturer_c != null) {
                            $this->createAuditLog($bid, "manufacturer_c", $clonedBean->manufacturer_c, 'varchar');
                        }
                        //Audit log for external_barcode
                        if ($clonedBean->external_barcode != "" || $clonedBean->external_barcode != null) {
                            $this->createAuditLog($bid, "external_barcode", $clonedBean->external_barcode, 'varchar');
                        }
                        //Audit log for concentration_c
                        if ($clonedBean->concentration_c != "" || $clonedBean->concentration_c != null) {
                            $this->createAuditLog($bid, "concentration_c", $clonedBean->concentration_c, 'decimal');
                        }
                        //Audit log for concentration_unit_c
                        if ($clonedBean->concentration_unit_c != "" || $clonedBean->concentration_unit_c != null) {
                            $this->createAuditLog($bid, "concentration_unit_c", $clonedBean->concentration_unit_c, 'relate');
                        }
                        //Audit log for sterilization
                        if ($clonedBean->sterilization != "" || $clonedBean->sterilization != null) {
                            $this->createAuditLog($bid, "sterilization", $clonedBean->sterilization, 'enum');
                        }
                        //Audit log for timepoint_type
                        if ($clonedBean->timepoint_type != "" || $clonedBean->timepoint_type != null) {
                            $this->createAuditLog($bid, "timepoint_type", $clonedBean->timepoint_type, 'enum');
                        }
                        //Audit log for timepoint_integer
                        if ($clonedBean->timepoint_integer != "" || $clonedBean->timepoint_integer != null) {
                            $this->createAuditLog($bid, "timepoint_integer", $clonedBean->timepoint_integer, 'int');
                        }
                        //Audit log for timepoint_unit
                        if ($clonedBean->timepoint_unit != "" || $clonedBean->timepoint_unit != null) {
                            $this->createAuditLog($bid, "timepoint_unit", $clonedBean->timepoint_unit, 'relate');
                        }
                        //Audit log for processing_method
                        if ($clonedBean->processing_method != "" || $clonedBean->processing_method != null) {
                            $this->createAuditLog($bid, "processing_method", $clonedBean->processing_method, 'enum');
                        }
                        //Audit log for collection_date_time
                        if ($clonedBean->collection_date_time != "" || $clonedBean->collection_date_time != null) {
                            $this->createAuditLog($bid, "collection_date_time", $clonedBean->collection_date_time, 'datetime');
                        }
                        //Audit log for expiration_test_article
                        if ($clonedBean->expiration_test_article != "" || $clonedBean->expiration_test_article != null) {
                            $this->createAuditLog($bid, "expiration_test_article", $clonedBean->expiration_test_article, 'enum');
                        }
                        //Audit log for expiration_date
                        if ($clonedBean->expiration_date != "" || $clonedBean->expiration_date != null) {
                            $this->createAuditLog($bid, "expiration_date", $clonedBean->expiration_date, 'date');
                        }
                        //Audit log for days_to_expiration
                        if ($clonedBean->days_to_expiration != "" || $clonedBean->days_to_expiration != null) {
                            $this->createAuditLog($bid, "days_to_expiration", $clonedBean->days_to_expiration, 'int');
                        }
                        //Audit log for location_building
                        if ($clonedBean->location_building != "" || $clonedBean->location_building != null) {
                            $this->createAuditLog($bid, "location_building", $clonedBean->location_building, 'enum');
                        }
                        //Audit log for location_room
                        if ($clonedBean->location_room != "" || $clonedBean->location_room != null) {
                            $this->createAuditLog($bid, "location_room", $clonedBean->location_room, 'relate');
                        }
                        //Audit log for location_equipment
                        if ($clonedBean->location_equipment != "" || $clonedBean->location_equipment != null) {
                            $this->createAuditLog($bid, "location_equipment", $clonedBean->location_equipment, 'relate');
                        }
                        //Audit log for location_shelf
                        if ($clonedBean->location_shelf != "" || $clonedBean->location_shelf != null) {
                            $this->createAuditLog($bid, "location_shelf", $clonedBean->location_shelf, 'enum');
                        }
                        //Audit log for location_cabinet
                        if ($clonedBean->location_cabinet != "" || $clonedBean->location_cabinet != null) {
                            $this->createAuditLog($bid, "location_cabinet", $clonedBean->location_cabinet, 'enum');
                        }
                        //Audit log for ship_to_contact
                        if ($clonedBean->ship_to_contact != "" || $clonedBean->ship_to_contact != null) {
                            $this->createAuditLog($bid, "ship_to_contact", $clonedBean->ship_to_contact, 'relate');
                        }
                        //Audit log for ship_to_company
                        if ($clonedBean->ship_to_company != "" || $clonedBean->ship_to_company != null) {
                            $this->createAuditLog($bid, "ship_to_company", $clonedBean->ship_to_company, 'relate');
                        }
                        //Audit log for ship_to_address
                        if ($clonedBean->ship_to_address != "" || $clonedBean->ship_to_address != null) {
                            $this->createAuditLog($bid, "ship_to_address", $clonedBean->ship_to_address, 'relate');
                        }
                        //Audit log for retention_start_date
                        if ($clonedBean->retention_start_date != "" || $clonedBean->retention_start_date != null) {
                            $this->createAuditLog($bid, "retention_start_date", $clonedBean->retention_start_date, 'date');
                        }
                        //Audit log for retention_interval_days
                        if ($clonedBean->retention_interval_days != "" || $clonedBean->retention_interval_days != null) {
                            $this->createAuditLog($bid, "retention_interval_days", $clonedBean->retention_interval_days, 'int');
                        }
                        //Audit log for planned_discard_date
                        if ($clonedBean->planned_discard_date != "" || $clonedBean->planned_discard_date != null) {
                            $this->createAuditLog($bid, "planned_discard_date", $clonedBean->planned_discard_date, 'date');
                        }
                        //Audit log for actual_discard_date
                        if ($clonedBean->actual_discard_date != "" || $clonedBean->actual_discard_date != null) {
                            $this->createAuditLog($bid, "actual_discard_date", $clonedBean->actual_discard_date, 'date');
                        }
                        //Audit log for quantity_c
                        if ($clonedBean->quantity_c != "" || $clonedBean->quantity_c != null) {
                            $this->createAuditLog($bid, "quantity_c", $clonedBean->quantity_c, 'int');
                        }
                        //Audit log for expiration_date_time
                        if ($clonedBean->expiration_date_time != "" || $clonedBean->expiration_date_time != null) {
                            $this->createAuditLog($bid, "expiration_date_time", $clonedBean->expiration_date_time, 'datetmie');
                        }
                        //Audit log for type_2
                        if ($clonedBean->type_2 != "" || $clonedBean->type_2 != null) {
                            $this->createAuditLog($bid, "type_2", $clonedBean->type_2, 'enum');
                        }
                        //Audit log for product_name
                        if ($clonedBean->product_name != "" || $clonedBean->product_name != null) {
                            $this->createAuditLog($bid, "product_name", $clonedBean->product_name, 'varchar');
                        }
                        //Audit log for allowed_storage_condition
                        if ($clonedBean->allowed_storage_conditions != "" || $clonedBean->allowed_storage_conditions != null) {
                            $this->createAuditLog($bid, "allowed_storage_conditions", $clonedBean->allowed_storage_conditions, 'multienum');
                        }
                        //Audit log for allowed_storage_medium
                        if ($clonedBean->allowed_storage_medium != "" || $clonedBean->allowed_storage_medium != null) {
                            $this->createAuditLog($bid, "allowed_storage_medium", $clonedBean->allowed_storage_medium, 'multienum');
                        }
                        //Audit log for subtype
                        if ($clonedBean->subtype != "" || $clonedBean->subtype != null) {
                            $this->createAuditLog($bid, "subtype", $clonedBean->subtype, 'enum');
                        }
                        //Audit log for collection_date_time_type
                        if ($clonedBean->collection_date_time_type != "" || $clonedBean->collection_date_time_type != null) {
                            $this->createAuditLog($bid, "collection_date_time_type", $clonedBean->collection_date_time_type, 'enum');
                        }
                        //Audit log for storage_medium
                        if ($clonedBean->storage_medium != "" || $clonedBean->storage_medium != null) {
                            $this->createAuditLog($bid, "storage_medium", $clonedBean->storage_medium, 'enum');
                        }
                        //Audit log for collection_date
                        if ($clonedBean->collection_date != "" || $clonedBean->collection_date != null) {
                            $this->createAuditLog($bid, "collection_date", $clonedBean->collection_date, 'date');
                        }
                        //Audit log for received_date
                        if ($clonedBean->received_date != "" || $clonedBean->received_date != null) {
                            $this->createAuditLog($bid, "received_date", $clonedBean->received_date, 'date');
                        }
                        //Audit log for storage_condition
                        if ($clonedBean->storage_condition != "" || $clonedBean->storage_condition != null) {
                            $this->createAuditLog($bid, "storage_condition", $clonedBean->storage_condition, 'enum');
                        }
                        //Audit log for location_type
                        if ($clonedBean->location_type != "" || $clonedBean->location_type != null) {
                            $this->createAuditLog($bid, "location_type", $clonedBean->location_type, 'enum');
                        }
                        //Audit log for status
                        if ($clonedBean->status != "" || $clonedBean->status != null) {
                            $this->createAuditLog($bid, "status", $clonedBean->status, 'enum');
                        }
                        //Audit log for stability_considerations_c
                        if ($clonedBean->stability_considerations_c != "" || $clonedBean->stability_considerations_c != null) {
                            $this->createAuditLog($bid, "stability_considerations_c", $clonedBean->stability_considerations_c, 'enum');
                        }
                        //Audit log for analyze_by_date_c
                        if ($clonedBean->analyze_by_date_c != "" || $clonedBean->analyze_by_date_c != null) {
                            $this->createAuditLog($bid, "analyze_by_date_c", $clonedBean->analyze_by_date_c, 'date');
                        }
                        //Audit log for test_type_c
                        if ($clonedBean->test_type_c != "" || $clonedBean->test_type_c != null) {
                            $this->createAuditLog($bid, "test_type_c", $clonedBean->test_type_c, 'enum');
                        }
                        //Audit log for sterilization_exp_date_c
                        if ($clonedBean->sterilization_exp_date_c != "" || $clonedBean->sterilization_exp_date_c != null) {
                            $this->createAuditLog($bid, "sterilization_exp_date_c", $clonedBean->sterilization_exp_date_c, 'date');
                        }
                        //Audit log for product_id_2_c
                        if ($clonedBean->product_id_2_c != "" || $clonedBean->product_id_2_c != null) {
                            $this->createAuditLog($bid, "product_id_2_c", $clonedBean->product_id_2_c, 'varchar');
                        }
                    }
                }
            }
        }
        return true;
    }

    public function createAuditLog($bid, $field_name, $after_field_value, $data_type) {
        global $current_user, $db;
        $source = '{"subject":{"_type":"user","id":"' . $current_user->id . '","_module":"Users","client":{"_type":"rest-api"}},"attributes":{"platform":"base"}}';
        $event_id = create_guid();
        $audit_id = create_guid();

        $auditsql = 'INSERT INTO ii_inventory_item_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . $audit_id . '","' . $bid . '","' . $event_id . '",now(),"' . $current_user->id . '",now(),"' . $field_name . '","' . $data_type . '","","' . $after_field_value . '")';
        $auditsqlResult = $db->query($auditsql);

        //Inserting audit data in audit_events table
        $sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $bid . "','II_Inventory_Item','" . $source . "',NULL,now())";
        $db->query($sql);
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return "AP-" . $randomString;
    }

}
