<?php

if (!defined('sugarEntry') || !sugarEntry) {
  die('Not A Valid Entry Point');
}

class CalculateLocation
{

  public function calculate($bean, $event, $arguments)
  {
    global $db, $current_user;
    $module = $arguments['module'];
    $modelId = $arguments['id'];

    $relatedModule = $arguments['related_module'];
    $relatedId = $arguments['related_id'];

    if ($module === 'II_Inventory_Item' && $relatedModule === 'IM_Inventory_Management') {
      if ($bean->rms_room_id_c != $bean->fetched_row['rms_room_id_c']) {
        $sqlRoom = 'SELECT name FROM rms_room WHERE id = "' . $bean->rms_room_id_c . '"';
        $resultRoom = $db->query($sqlRoom);
        $rowRoom = $db->fetchByAssoc($resultRoom);
        $RoomName = $rowRoom['name'];

        $sqlRowRoom = 'SELECT name FROM rms_room WHERE id = "' . $bean->fetched_row['rms_room_id_c'] . '"';
        $resultRowRoom = $db->query($sqlRowRoom);
        $rowRowRoom = $db->fetchByAssoc($resultRowRoom);
        $RoomRowName = $rowRowRoom['name'];

        //$auditsqlRoom = 'INSERT INTO ii_inventory_item_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $bean->id . '","65f3b16c-178e-11eb-b297-069273903c1e",now(),"' . $current_user->id . '",now(),"location_room","relate","' . $RoomRowName . '","' . $RoomName . '")';
        //$Room = $db->query($auditsqlRoom);
      }

      if ($bean->equip_equipment_id_c != $bean->fetched_row['equip_equipment_id_c']) {
        $sqlEE = 'SELECT name FROM equip_equipment WHERE id = "' . $bean->equip_equipment_id_c . '"';
        $resultEE = $db->query($sqlEE);
        $rowEE = $db->fetchByAssoc($resultEE);
        $EEName = $rowEE['name'];

        $sqlRowEE = 'SELECT name FROM equip_equipment WHERE id = "' . $bean->fetched_row['equip_equipment_id_c'] . '"';
        $resultRowEE = $db->query($sqlRowEE);
        $rowRowEE = $db->fetchByAssoc($resultRowEE);
        $EERowName = $rowRowEE['name'];

      
      }
     
      $this->calcLocation($modelId, $relatedId);
    } else if ($relatedModule === 'II_Inventory_Item' && $module === 'IM_Inventory_Management') {
      $this->calcLocation($relatedId, $modelId);
    }
  }

  public function calcLocation($id, $rel_id)
  {
    global $db, $current_user;
    $inventory_item = BeanFactory::retrieveBean('II_Inventory_Item', $id);
    $inventory_manager = $this->retrieveMostRecentIm($inventory_item);

    $location_equipment_old = $inventory_item->equip_equipment_id_c;
    $rms_room_old = $inventory_item->rms_room_id_c;

    if ($inventory_item && $inventory_manager) {
      $inventory_item->dontChangeName = true;
      if ($inventory_manager->location_type != "") {
        $location_type = $inventory_manager->location_type;
        $location_building = $inventory_manager->location_building;
        $location_room_id = $inventory_manager->rms_room_id_c;
        $location_room_name = $inventory_manager->location_room;
        $location_shelf = $inventory_manager->location_shelf;
        $location_cabinet = $inventory_manager->location_cabinet;
        $location_equipment_name = $inventory_manager->location_equipment;
        $location_equipment_id = $inventory_manager->equip_equipment_id_c;

        /**11 nov 2021 : 1117 bug fix */
        $location_bin_c = $inventory_manager->location_bin_c;
        $location_cubby_c = $inventory_manager->location_cubby_c;
        $location_rack_c = $inventory_manager->location_rack_c;


        $inventory_item->location_type = $location_type;
        $inventory_item->location_building = $location_building;
        $inventory_item->rms_room_id_c = $location_room_id;
        $inventory_item->location_room = $location_room_name;
        $inventory_item->location_shelf = $location_shelf;
        $inventory_item->location_cabinet = $location_cabinet;
        $inventory_item->location_equipment = $location_equipment_name;
        $inventory_item->equip_equipment_id_c = $location_equipment_id;
        /**11 nov 2021 : 1117 bug fix */
        $inventory_item->location_bin_c = $location_bin_c;
        $inventory_item->location_cubby_c = $location_cubby_c;
        $inventory_item->location_rack_c = $location_rack_c;

        $inventory_item->test_ii_c = "test_ii_c test";

        if ($inventory_manager->action != 'Archive Offsite' && $inventory_manager->action != 'External Transfer' && $inventory_manager->action!='Obsolete' && $inventory_manager->action!='Discard') {
          $inventory_item->ship_to_contact = null;
          $inventory_item->contact_id_c = null;

          $inventory_item->ship_to_address = null;
          $inventory_item->ca_company_address_id_c = null;

          $inventory_item->ship_to_company = null;
          $inventory_item->account_id_c = null;
        } else if ($inventory_manager->action == 'Archive Offsite' || $inventory_manager->action == 'External Transfer' ||  $inventory_manager->action=='Obsolete'|| $inventory_manager->action=='Discard' ||  $inventory_manager->action == "" || $inventory_manager->action == 'Exhausted') {
         
          $inventory_item->location_type = null;
          $inventory_item->location_building = null;
          $inventory_item->rms_room_id_c = null;
          $inventory_item->location_room = null;
          $inventory_item->location_equipment = null;
          $inventory_item->equip_equipment_id_c = null;
          $inventory_item->location_shelf = null;
          $inventory_item->location_cabinet = null;
          /* $inventory_item->location_bin_c = null;
          $inventory_item->location_cubby_c = null;
          $inventory_item->location_rack_c = null; */
          $inventory_item->test_ii_c = null;
        }
        
        $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="equip_equipment_id_c" order by date_created desc limit 1';
        $resultEquipAudit = $db->query($sqlEquipAudit);
        $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
        $before_value_string = $rowEquipAudit['before_value_string'];
        $after_value_string_e = $rowEquipAudit['after_value_string'];

        $sqlRoomAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="rms_room_id_c" order by date_created desc limit 1';
        $resultRoomAudit = $db->query($sqlRoomAudit);
        $rowRoomAudit = $db->fetchByAssoc($resultRoomAudit);
        $before_value_string = $rowRoomAudit['before_value_string'];
        $after_value_string_r = $rowRoomAudit['after_value_string'];

        $inventory_item->save();

        $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "equip_equipment_id_c",
                         before_value_string="' . $after_value_string_e . '",
                         after_value_string="' . $inventory_item->equip_equipment_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_equipment" 
                         order by date_created desc
                         limit 1';
        $db->query($auditsql);

        $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "rms_room_id_c",
                         before_value_string="' . $after_value_string_r . '",
                         after_value_string="' . $inventory_item->rms_room_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_room" 
                         order by date_created desc
                         limit 1';
        $db->query($auditsql);
      }
    } else {
      if ($inventory_manager->action == 'Archive Offsite' || $inventory_manager->action == 'External Transfer' || $inventory_manager->action == 'Obsolete' || $inventory_manager->action == 'Discard' || $inventory_manager->action == 'Exhausted') {
       
        $inventory_item->location_type = null;
        $inventory_item->location_building = null;
        $inventory_item->rms_room_id_c = null;
        $inventory_item->location_room = null;
        $inventory_item->location_equipment = null;
        $inventory_item->equip_equipment_id_c = null;
        $inventory_item->location_shelf = null;
        $inventory_item->location_cabinet = null;
        /* $inventory_item->location_bin_c = null;
        $inventory_item->location_cubby_c = null;
        $inventory_item->location_rack_c = null; */
        $inventory_item->test_ii_c = null;
      } else {
       
        $location_values = explode('#', $inventory_item->location_values_c);
        $inventory_item->location_type = $location_values[0];
        $inventory_item->location_building = $location_values[1];
        $inventory_item->rms_room_id_c = $location_values[2];
        $inventory_item->location_room = $location_values[3];
        $inventory_item->location_equipment = $location_values[4];
        $inventory_item->equip_equipment_id_c = $location_values[5];
        $inventory_item->location_shelf = $location_values[6];
        $inventory_item->location_cabinet = $location_values[7];
        $inventory_item->location_bin_c = $location_values[8];
        $inventory_item->location_cubby_c = $location_values[9];
        $inventory_item->location_rack_c = $location_values[10];
        $inventory_item->test_ii_c = null;

      }
     
      $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="equip_equipment_id_c" order by date_created desc limit 1';
      $resultEquipAudit = $db->query($sqlEquipAudit);
      $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
      $before_value_string = $rowEquipAudit['before_value_string'];
      $after_value_string_e = $rowEquipAudit['after_value_string'];

      $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="rms_room_id_c" order by date_created desc limit 1';
      $resultEquipAudit = $db->query($sqlEquipAudit);
      $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
      $before_value_string = $rowEquipAudit['before_value_string'];
      $after_value_string_r = $rowEquipAudit['after_value_string'];

      $inventory_item->save();
      
      $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "equip_equipment_id_c",
                         before_value_string="' . $after_value_string_e . '",
                         after_value_string="' . $inventory_item->equip_equipment_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_equipment" 
                         order by date_created desc
                         limit 1';
      $db->query($auditsql);

      $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "rms_room_id_c",
                         before_value_string="' . $after_value_string_r . '",
                         after_value_string="' . $inventory_item->rms_room_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_room" 
                         order by date_created desc
                         limit 1';
      $db->query($auditsql);
      
    }
   
    if ($inventory_item->status == "Discarded" || $inventory_item->status == "Obsolete" || $inventory_item->status == "Exhausted") {
      $inventory_item->location_type = null;
      $inventory_item->location_building = null;
      $inventory_item->rms_room_id_c = null;
      $inventory_item->location_room = null;
      $inventory_item->location_equipment = null;
      $inventory_item->equip_equipment_id_c = null;
      $inventory_item->location_shelf = null;
      $inventory_item->location_cabinet = null;
      $inventory_item->location_bin_c = null;
      $inventory_item->location_cubby_c = null;
      $inventory_item->location_rack_c = null;
      $inventory_item->test_ii_c = null;
     
      $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="equip_equipment_id_c" order by date_created desc limit 1';
      $resultEquipAudit = $db->query($sqlEquipAudit);
      $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
      $before_value_string = $rowEquipAudit['before_value_string'];
      $after_value_string_e = $rowEquipAudit['after_value_string'];

      $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="rms_room_id_c" order by date_created desc limit 1';
      $resultEquipAudit = $db->query($sqlEquipAudit);
      $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
      $before_value_string = $rowEquipAudit['before_value_string'];
      $after_value_string_r = $rowEquipAudit['after_value_string'];

       /** Get Related fields of shipping  */

       $sqlShipping_contactid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="ship_to_contact" order by date_created desc limit 1';
       $resultShippingcidAudit = $db->query($sqlShipping_contactid);
       $rowShippingcidAudit = $db->fetchByAssoc($resultShippingcidAudit);
       $before_value_string_ship_to_contact_id = $rowShippingcidAudit['before_value_string'];
       $after_value_string_ship_to_contact_id = $rowShippingcidAudit['after_value_string'];

       $sqlShipping_addressid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="ship_to_address" order by date_created desc limit 1';
       $resultShippingaddidAudit = $db->query($sqlShipping_addressid);
       $rowShippingaddidAudit = $db->fetchByAssoc($resultShippingaddidAudit);
       $before_value_string_ship_to_address_id = $rowShippingaddidAudit['before_value_string'];
       $after_value_string_ship_to_address_id = $rowShippingaddidAudit['after_value_string'];

       $sqlShipping_companyid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="ship_to_company" order by date_created desc limit 1';
       $resultShippingcompanyAudit = $db->query($sqlShipping_companyid);
       $rowShippingcompanyAudit = $db->fetchByAssoc($resultShippingcompanyAudit);
       $before_value_string_ship_to_company_id = $rowShippingcompanyAudit['before_value_string'];
       $after_value_string_ship_to_company_id = $rowShippingcompanyAudit['after_value_string'];

      $inventory_item->save();

      $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "equip_equipment_id_c",
                         before_value_string="' . $after_value_string_e . '",
                         after_value_string="' . $inventory_item->equip_equipment_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_equipment" 
                         order by date_created desc
                         limit 1';
      $db->query($auditsql);



      $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "rms_room_id_c",
                         before_value_string="' . $after_value_string_r . '",
                         after_value_string="' . $inventory_item->rms_room_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_room" 
                         order by date_created desc
                         limit 1';
      $db->query($auditsql);     
      /*Update relate fields of shipping */
       /*Update relate fields of shipping */
       $auditsql_contactid = 'update ii_inventory_item_audit 
       set field_name= "ship_to_contact",
       before_value_string="' . $after_value_string_ship_to_contact_id . '",
       after_value_string="' . $inventory_item->contact_id_c . '"
       Where parent_id = "' . $inventory_item->id . '"
       AND field_name="ship_to_contact" 
       order by date_created desc
       limit 1';
       $db->query($auditsql_contactid);
        /*Update shipping company id */
       $auditsql_companyid = 'update ii_inventory_item_audit 
       set field_name= "ship_to_company",
       before_value_string="' . $after_value_string_ship_to_company_id . '",
       after_value_string="' . $inventory_item->account_id_c . '"
       Where parent_id = "' . $inventory_item->id . '"
       AND field_name="ship_to_company" 
       order by date_created desc
       limit 1';
       $db->query($auditsql_companyid);
        /*Update shipping address id */
       $auditsql_addressid = 'update ii_inventory_item_audit 
       set field_name= "ship_to_address",
       before_value_string="' . $after_value_string_ship_to_address_id . '",
       after_value_string="' . $inventory_item->ca_company_address_id_c . '"
       Where parent_id = "' . $inventory_item->id . '"
       AND field_name="ship_to_address" 
       order by date_created desc
       limit 1';
       $db->query($auditsql_addressid);

    }
  }
  /* On link IM record : will sync the data based on the IM fields */
  function calculateBeforeSave($bean, $event, $arguments)
  {
    global $db, $current_user;
    $itemId = $bean->id;
    //$sql = "SELECT * FROM `ii_inventory_item_im_inventory_management_1_c` WHERE ii_inventory_item_im_inventory_management_1ii_inventory_item_ida='" . $itemId . "' AND deleted=0 order by ii_invento5fdeagement_idb desc limit 1";
    $sql = "SELECT im_cus.ii_invento5fdeagement_idb,im_cus.ii_inventory_item_im_inventory_management_1ii_inventory_item_ida,im_cus.deleted, im.id,im.date_entered FROM `ii_inventory_item_im_inventory_management_1_c` AS  im_cus
        left join im_inventory_management as im on im.id = im_cus.ii_invento5fdeagement_idb 
        WHERE im_cus.ii_inventory_item_im_inventory_management_1ii_inventory_item_ida='" . $itemId . "'
        AND im.action!='Processing'
        AND im_cus.deleted=0 order by im.date_entered desc limit 1";

    $result = $db->query($sql);
    $mostRecentIm = null;
    $mostRecentImName = null;
    $previousDate = "";
    while ($row = $db->fetchByAssoc($result)) {
      $im_id = $row['ii_invento5fdeagement_idb'];
      $inventory_manager = BeanFactory::retrieveBean('IM_Inventory_Management', $im_id);
      $date_entered = $inventory_manager->date_entered;
    
      if ($previousDate == "" || $previousDate < strtotime($date_entered)) {
       
        $mostRecentIm = $inventory_manager->id;
        $mostRecentImName = $inventory_manager->name;
        $previousDate = strtotime($date_entered);

        if (($inventory_manager->location_type != "") && ($inventory_manager->action != 'Archive Offsite' &&  $inventory_manager->action != 'External Transfer' && $inventory_manager->action!='Obsolete' && $inventory_manager->action!='Discard' && $inventory_manager->action!='Exhausted')) {
          /* Commented the below code to fix the issue of #391 : 01 March 2021 */
          $location_type = $inventory_manager->location_type;
          $location_building = $inventory_manager->location_building;
          $location_room_id = $inventory_manager->rms_room_id_c;
          $location_room_name = $inventory_manager->location_room;
          $location_shelf = $inventory_manager->location_shelf;
          $location_cabinet = $inventory_manager->location_cabinet;
          $location_equipment_name = $inventory_manager->location_equipment;
          $location_equipment_id = $inventory_manager->equip_equipment_id_c;         
          $bean->test_ii_c = "test_ii_c test";
           /**11 nov 201 : 1117 bug fix */
          $location_bin_c = $inventory_manager->location_bin_c;
          $location_cubby_c = $inventory_manager->location_cubby_c;
          $location_rack_c = $inventory_manager->location_rack_c;

        } else if ($inventory_manager->action == 'Archive Offsite' || $inventory_manager->action == 'External Transfer' || $inventory_manager->action == 'Obsolete' || $inventory_manager->action == 'Discard' || $inventory_manager->action == "Exhausted") {
          $bean->test_ii_c = null;
          $bean->location_type = null;
          $bean->location_building = null;
          $bean->rms_room_id_c = null;
          $bean->location_room = null;
          $bean->location_equipment = null;
          $bean->equip_equipment_id_c = null;
          $bean->location_shelf = null;
          $bean->location_cabinet = null;
          /* $bean->location_bin_c = null;
          $bean->location_cubby_c = null;
          $bean->location_rack_c = null; */
        
          $ship_to_contact_id = $inventory_manager->contact_id_c;
          $ship_to_contact_name = $inventory_manager->ship_to_contact;

          $ship_to_address_id = $inventory_manager->ca_company_address_id_c;
          $ship_to_address_name = $inventory_manager->ship_to_address;

          $ship_to_company_id = $inventory_manager->account_id_c;
          $ship_to_company_name = $inventory_manager->ship_to_company;

          $bean->contact_id_c = $ship_to_contact_id;
          $bean->ship_to_contact = $ship_to_contact_name;

          $bean->ca_company_address_id_c = $ship_to_address_id;
          $bean->ship_to_address = $ship_to_address_name;

          $bean->account_id_c = $ship_to_company_id;
          $bean->ship_to_company = $ship_to_company_name;

          //$inventory_item->test_ii_c = null;

        }
        else {
          $location_values = explode('#', $bean->location_values_c);
          $location_type = $location_values[0];
          $location_building = $location_values[1];
          $location_room_id = $location_values[2];
          $location_room_name = $location_values[3];
          $location_equipment_name = $location_values[4];
          $location_equipment_id = $location_values[5];
          $location_shelf = $location_values[6];
          $location_cabinet = $location_values[7];
          $location_bin_c = $location_values[8];
          $location_cubby_c = $location_values[9];
          $location_rack_c = $location_values[10];

          $bean->ship_to_contact = null;
          $bean->contact_id_c = null;

          $bean->ship_to_address = null;
          $bean->ca_company_address_id_c = null;

          $bean->ship_to_company = null;
          $bean->account_id_c = null;
        }
       
      } else {    
        if ($bean->location_values_c != "" && $inventory_manager->location_type == "") {
          $location_values = explode('#', $bean->location_values_c);
          $location_type = $location_values[0];
          $location_building = $location_values[1];
          $location_room_id = $location_values[2];
          $location_room_name = $location_values[3];
          $location_equipment_name = $location_values[4];
          $location_equipment_id = $location_values[5];
          $location_shelf = $location_values[6];
          $location_cabinet = $location_values[7];
          $location_bin_c = $location_values[8];
          $location_cubby_c = $location_values[9];
          $location_rack_c = $location_values[10];
          $bean->test_ii_c = null;
           
        }
      }
    }

    if ($location_bin_c != "")
      $bean->location_bin_c = $location_bin_c;
    if ($location_cubby_c != "")
      $bean->location_cubby_c = $location_cubby_c;
    if ($location_rack_c != "")
      $bean->location_rack_c = $location_rack_c;


    if ($location_type != "") {
      $bean->location_type = $location_type;
    }
    if ($location_building != "") {
      $bean->location_building = $location_building;
    }

    if ($location_room_id != "") {
      $bean->rms_room_id_c = $location_room_id;
      $bean->location_room = $location_room_name;
    }

    if ($location_shelf != "") {
      $bean->location_shelf = $location_shelf;
    }

    if ($location_cabinet != "") {
      $bean->location_cabinet = $location_cabinet;
    }

    if ($location_equipment_id != "") {
      $bean->location_equipment = $location_equipment_name;
      $bean->equip_equipment_id_c = $location_equipment_id;
    }

    if ($bean->location_type == "Known" || $bean->location_type == "Unknown") {
      if ($bean->rms_room_id_c != "") {
        $sqlRoom = 'SELECT name FROM rms_room WHERE id = "' . $bean->rms_room_id_c . '"';
        $resultRoom = $db->query($sqlRoom);
        if ($resultRoom->num_rows > 0) {
          $rowRoom = $db->fetchByAssoc($resultRoom);
          $RoomName = $rowRoom['name'];
        }
      }
      if ($bean->equip_equipment_id_c != "") {
        $sqlEE = 'SELECT name FROM equip_equipment WHERE id = "' . $bean->equip_equipment_id_c . '"';
        $resultEE = $db->query($sqlEE);
        if ($resultEE->num_rows > 0) {
          $rowEE = $db->fetchByAssoc($resultEE);
          $EEName = $rowEE['name'];
        }
      }
      $location_values = $bean->location_type . '#' . $bean->location_building . '#' . $bean->rms_room_id_c . '#' . $RoomName . '#' . $EEName . '#' . $bean->equip_equipment_id_c . '#' . $bean->location_shelf . '#' . $bean->location_cabinet . '#' . $bean->location_bin_c . '#' . $bean->location_cubby_c . '#' . $bean->location_rack_c;
      $bean->location_values_c = $location_values;

      $location_equipment_old = $bean->equip_equipment_id_c;
      $rms_room_old = $bean->rms_room_id_c;
      if ($bean->status == "Discarded" || $bean->status == "Obsolete" || $bean->status == "Exhausted") {
        $bean->location_type = null;
        $bean->location_building = null;
        $bean->rms_room_id_c = null;
        $bean->location_room = null;
        $bean->location_equipment = null;
        $bean->equip_equipment_id_c = null;
        $bean->location_shelf = null;
        $bean->location_cabinet = null;
        $bean->location_bin_c = null;
        $bean->location_cubby_c = null;
        $bean->location_rack_c = null;
        $bean->test_ii_c = null;

      }
     
    }
  }

  function calculateAfterSave($bean, $event, $arguments)
  {
 
    global $db, $current_user;
    $auditsql = 'DELETE FROM ii_inventory_item_audit WHERE before_value_string IS NULL AND  after_value_string IS NULL AND parent_id = "' . $bean->id . '"';
    $db->query($auditsql);

    $queryAuditLogEquip = "SELECT id,count(*) as NUM FROM `ii_inventory_item_audit` WHERE `field_name`='equip_equipment_id_c' 
		AND `parent_id`='".$bean->id."' group by before_value_string,after_value_string,date_created";
		$auditLogResultEquip = $db->query($queryAuditLogEquip);
		
		if($auditLogResultEquip->num_rows > 0 ){
			while ($fetchAuditLogEquip = $db->fetchByAssoc($auditLogResultEquip)) {
				if($fetchAuditLogEquip['NUM']>1){
					$recordIDEquip = $fetchAuditLogEquip['id'];
					$sql_DeleteAuditEquip = "DELETE from `ii_inventory_item_audit` WHERE `field_name`='equip_equipment_id_c' AND `parent_id`='".$bean->id."' AND id = '".$recordIDEquip."'";
					$db->query($sql_DeleteAuditEquip);
				}				
			}
		}

    $queryAuditLog = "SELECT id,count(*) as NUM FROM `ii_inventory_item_audit` WHERE `field_name`='rms_room_id_c' 
		AND `parent_id`='".$bean->id."' group by before_value_string,after_value_string,date_created";
		$auditLogResult = $db->query($queryAuditLog);
		
		if($auditLogResult->num_rows > 0 ){
			while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
				if($fetchAuditLog['NUM']>1){
					$recordID = $fetchAuditLog['id'];
					$sql_DeleteAudit = "DELETE from `ii_inventory_item_audit` WHERE `field_name`='rms_room_id_c' AND `parent_id`='".$bean->id."' AND id = '".$recordID."'";
					$db->query($sql_DeleteAudit);
				}				
			}
    }
    /*Delete shipping where before and after are blank */
    $auditsqlshiptoc = 'DELETE FROM ii_inventory_item_audit WHERE field_name="ship_to_contact" AND before_value_string IS NULL AND  after_value_string IS NULL AND parent_id = "' . $bean->id . '"';
    $db->query($auditsqlshiptoc);

    $auditsqlshiptoaddress = 'DELETE FROM ii_inventory_item_audit WHERE field_name="ship_to_address" AND before_value_string IS NULL AND  after_value_string IS NULL AND parent_id = "' . $bean->id . '"';
    $db->query($auditsqlshiptoaddress);
    
    $auditsqlshiptocompany = 'DELETE FROM ii_inventory_item_audit WHERE field_name="ship_to_company" AND before_value_string IS NULL AND  after_value_string IS NULL AND parent_id = "' . $bean->id . '"';
    $db->query($auditsqlshiptocompany);
  }

  private function retrieveMostRecentIm($item)
  {
    if ($item->load_relationship("ii_inventory_item_im_inventory_management_1")) {
      $ims = $item->ii_inventory_item_im_inventory_management_1->getBeans();

      $mostRecentIm = null;
      foreach ($ims as $im) {
        $date_entered = $im->date_entered;
        if ($mostRecentIm === null) {
          $mostRecentIm = $im;
        }

        if (strtotime($mostRecentIm->date_entered) < strtotime($date_entered)) {
          $mostRecentIm = $im;
        }
      }

      return $mostRecentIm;
    } else {
      return null;
    }
  }
}
