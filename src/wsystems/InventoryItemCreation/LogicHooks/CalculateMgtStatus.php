<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class CalculateMgtStatus
{

    function calculateAfterSave($bean, $event, $arguments)
    {

        global $db;
        $itemMgtId = $bean->id;

        $sqlToGetInventoryItem = "SELECT * FROM `ii_inventory_item_im_inventory_management_1_c` WHERE `ii_invento5fdeagement_idb`='" . $itemMgtId . "' AND deleted=0";
        $resultInventoryItem = $db->query($sqlToGetInventoryItem);
        while ($resultInventoryItemRow = $db->fetchByAssoc($resultInventoryItem)) {
            if ($bean->action != 'Processing' && $bean->action != 'Obsolete' && $bean->action != 'Discard') {
                $inventoryItemId = $resultInventoryItemRow['ii_inventory_item_im_inventory_management_1ii_inventory_item_ida'];
                $inventory_Item = BeanFactory::retrieveBean('II_Inventory_Item', $inventoryItemId);
                /*#391 : To fix the issue of blank value from fields on location type change : 01 March 2021 */
                if ($bean->location_type == "Room") {
                    $location_room_id = $bean->rms_room_id_c;
                    $location_room_name = $bean->location_room;
                    $location_building = $bean->location_building;
                    $location_equipment_name = '';
                    $location_equipment_id = '';
                    $location_cabinet = '';
                    $location_shelf = '';
                     
                } else if ($bean->location_type == "RoomCabinet") {
                    $location_room_id = $bean->rms_room_id_c;
                    $location_room_name = $bean->location_room;
                    $location_building = $bean->location_building;
                    $location_equipment_name = '';
                    $location_equipment_id = '';
                    $location_cabinet = $bean->location_cabinet;
                    $location_shelf = '';
                     
                } else if ($bean->location_type == "RoomShelf") {
                    $location_room_id = $bean->rms_room_id_c;
                    $location_room_name = $bean->location_room;
                    $location_building = $bean->location_building;
                    $location_equipment_name = '';
                    $location_equipment_id = '';
                    $location_cabinet = '';
                    $location_shelf = $bean->location_shelf;
                     
                } else if ($bean->location_type == "Equipment") {
                    $location_equipment_name = $bean->location_equipment;
                    $location_equipment_id = $bean->equip_equipment_id_c;
                    $location_building = $bean->location_building;
                    $location_room_id = '';
                    $location_room_name = '';
                    $location_cabinet = '';
                    $location_shelf = '';
                    
                } else if ($bean->location_type == "Cabinet") {
                    $location_cabinet = $bean->location_cabinet;
                    $location_building = $bean->location_building;
                    $location_room_id = '';
                    $location_room_name = '';
                    $location_equipment_name = '';
                    $location_equipment_id = '';
                    $location_shelf = '';
                     
                } else if ($bean->location_type == "Shelf") {
                    $location_shelf = $bean->location_shelf;
                    $location_building = $bean->location_building;
                    $location_room_id = '';
                    $location_room_name = '';
                    $location_equipment_name = '';
                    $location_equipment_id = '';
                    $location_cabinet = '';
                    
                }
               
                $ship_to_contact_id = $bean->contact_id_c;
                $ship_to_contact_name = $bean->ship_to_contact;

                $ship_to_address_id = $bean->ca_company_address_id_c;
                $ship_to_address_name = $bean->ship_to_address;

                $ship_to_company_id = $bean->account_id_c;
                $ship_to_company_name = $bean->ship_to_company;

                if ($bean->action != 'Archive Offsite' && $bean->action != 'External Transfer') {
                     
                    $inventory_Item->contact_id_c = null;
                    $inventory_Item->ship_to_contact = null;

                    $inventory_Item->ca_company_address_id_c = null;
                    $inventory_Item->ship_to_address = null;
                    
                    $inventory_Item->account_id_c = null;
                    $inventory_Item->ship_to_company = null;
                } else {
                    
                    $inventory_Item->contact_id_c = $ship_to_contact_id;
                    $inventory_Item->ship_to_contact = $ship_to_contact_name;

                    $inventory_Item->ca_company_address_id_c = $ship_to_address_id;
                    $inventory_Item->ship_to_address = $ship_to_address_name;

                    $inventory_Item->account_id_c = $ship_to_company_id;
                    $inventory_Item->ship_to_company = $ship_to_company_name;
                }
                
                $location_type = $bean->location_type;
                $inventory_Item->location_type = $location_type;
                $inventory_Item->location_building = $location_building;
                $inventory_Item->rms_room_id_c = $location_room_id;
                $inventory_Item->location_room = $location_room_name;
                $inventory_Item->location_shelf = $location_shelf;
                $inventory_Item->location_cabinet = $location_cabinet;
                $inventory_Item->location_equipment = $location_equipment_name;
                $inventory_Item->equip_equipment_id_c = $location_equipment_id;
                $inventory_Item->test_ii_c = "test_ii_c test";
                
                $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_Item->id . '" and field_name="equip_equipment_id_c" order by date_created desc limit 1';
                $resultEquipAudit = $db->query($sqlEquipAudit);
                $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
                $before_value_string = $rowEquipAudit['before_value_string'];
                $after_value_string_e = $rowEquipAudit['after_value_string'];

                $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_Item->id . '" and field_name="rms_room_id_c" order by date_created desc limit 1';
                $resultEquipAudit = $db->query($sqlEquipAudit);
                $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
                $before_value_string = $rowEquipAudit['before_value_string'];
                $after_value_string_r = $rowEquipAudit['after_value_string'];

                 /** Get Related fields of shipping  */
                $sqlShipping_contactid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_Item->id . '" and field_name="ship_to_contact_id" order by date_created desc limit 1';
                $resultShippingcidAudit = $db->query($sqlShipping_contactid);
                $rowShippingcidAudit = $db->fetchByAssoc($resultShippingcidAudit);
                $before_value_string_ship_to_contact_id = $rowShippingcidAudit['before_value_string'];
                $after_value_string_ship_to_contact_id = $rowShippingcidAudit['after_value_string'];
    
                $sqlShipping_addressid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_Item->id . '" and field_name="ship_to_address_id" order by date_created desc limit 1';
                $resultShippingaddidAudit = $db->query($sqlShipping_addressid);
                $rowShippingaddidAudit = $db->fetchByAssoc($resultShippingaddidAudit);
                $before_value_string_ship_to_address_id = $rowShippingaddidAudit['before_value_string'];
                $after_value_string_ship_to_address_id = $rowShippingaddidAudit['after_value_string'];
    
                $sqlShipping_companyid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_Item->id . '" and field_name="ship_to_company_id" order by date_created desc limit 1';
                $resultShippingcompanyAudit = $db->query($sqlShipping_companyid);
                $rowShippingcompanyAudit = $db->fetchByAssoc($resultShippingcompanyAudit);
                $before_value_string_ship_to_company_id = $rowShippingcompanyAudit['before_value_string'];
                $after_value_string_ship_to_company_id = $rowShippingcompanyAudit['after_value_string']; 

                $inventory_Item->save();

                //$GLOBALS['log']->fatal('CMGStatrus 155 :  ');
                $auditsql = 'update ii_inventory_item_audit 
                             set field_name= "equip_equipment_id_c",
                             before_value_string="' . $after_value_string_e . '",
                             after_value_string="' . $inventory_Item->equip_equipment_id_c . '"
                             Where parent_id = "' . $inventory_Item->id . '"
                             AND field_name="location_equipment" 
                             order by date_created desc
                             limit 1';
                $db->query($auditsql);

                $auditsql = 'update ii_inventory_item_audit 
                             set field_name= "rms_room_id_c",
                             before_value_string="' . $after_value_string_r . '",
                             after_value_string="' . $inventory_Item->rms_room_id_c . '"
                             Where parent_id = "' . $inventory_Item->id . '"
                             AND field_name="location_room" 
                             order by date_created desc
                             limit 1';
                $db->query($auditsql);

                 /*Update relate fields of shipping */
            $auditsql_contactid = 'update ii_inventory_item_audit 
            set field_name= "ship_to_contact_id",
            before_value_string="' . $after_value_string_ship_to_contact_id . '",
            after_value_string="' . $bean->contact_id_c . '"
            Where parent_id = "' . $inventory_Item->id . '"
            AND field_name="ship_to_contact" 
            order by date_created desc
            limit 1';
            $db->query($auditsql_contactid);
             /*Update shipping company id */
            $auditsql_companyid = 'update ii_inventory_item_audit 
            set field_name= "ship_to_company_id",
            before_value_string="' . $after_value_string_ship_to_company_id . '",
            after_value_string="' . $bean->account_id_c . '"
            Where parent_id = "' . $inventory_Item->id . '"
            AND field_name="ship_to_company" 
            order by date_created desc
            limit 1';
            $db->query($auditsql_companyid);
             /*Update shipping address id */
            $auditsql_addressid = 'update ii_inventory_item_audit 
            set field_name= "ship_to_address_id",
            before_value_string="' . $after_value_string_ship_to_address_id . '",
            after_value_string="' . $bean->ca_company_address_id_c . '"
            Where parent_id = "' . $inventory_Item->id . '"
            AND field_name="ship_to_address" 
            order by date_created desc
            limit 1';
            $db->query($auditsql_addressid);
            
            }
        }
    }
}
