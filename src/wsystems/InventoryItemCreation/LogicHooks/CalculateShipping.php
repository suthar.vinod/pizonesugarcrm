<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class CalculateShipping {

    public function calculate($bean, $event, $arguments) {
        $module = $arguments['module'];
        $modelId = $arguments['id'];

        $relatedModule = $arguments['related_module'];
        $relatedId = $arguments['related_id'];

        if ($module === 'II_Inventory_Item' && $relatedModule === 'IM_Inventory_Management') {
            $this->calcShipping($modelId, $relatedId);
        } else if ($relatedModule === 'II_Inventory_Item' && $module === 'IM_Inventory_Management') {
            $this->calcShipping($relatedId, $modelId);
        }
    }

    public function calcShipping($id, $rel_id) {
        global $db, $current_user;
        $inventory_item = BeanFactory::retrieveBean('II_Inventory_Item', $id);
        $inventory_manager = $this->retrieveMostRecentIm($inventory_item);

        if ($inventory_item && $inventory_manager && $inventory_manager->action!='Processing' && $inventory_manager->action!='Obsolete' && $inventory_manager->action!='Discard') {
            $inventory_item->dontChangeName = true;

            $ship_to_contact_id = $inventory_manager->contact_id_c;
            $ship_to_contact_name = $inventory_manager->ship_to_contact;

            $ship_to_address_id = $inventory_manager->ca_company_address_id_c;
            $ship_to_address_name = $inventory_manager->ship_to_address;
            
            $ship_to_company_id = $inventory_manager->account_id_c;
            $ship_to_company_name = $inventory_manager->ship_to_company;
            
            $inventory_item->contact_id_c = $ship_to_contact_id;
            $inventory_item->ship_to_contact = $ship_to_contact_name;
            
            $inventory_item->ca_company_address_id_c = $ship_to_address_id;
            $inventory_item->ship_to_address = $ship_to_address_name;
            
           $inventory_item->account_id_c = $ship_to_company_id;
           $inventory_item->ship_to_company = $ship_to_company_name;

            if($inventory_manager->action=='Archive Offsite' || $inventory_manager->action=='External Transfer' || $inventory_manager->action=='Obsolete' || $inventory_manager->action=='Discard')
			{
				$inventory_item->location_type = null;
                $inventory_item->location_building = null;
                $inventory_item->rms_room_id_c = null;
                $inventory_item->location_room = null;
                $inventory_item->location_equipment = null;
                $inventory_item->equip_equipment_id_c = null;
                $inventory_item->location_shelf = null;
                $inventory_item->location_cabinet = null;
			}
			 
            
            $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="equip_equipment_id_c" order by date_created desc limit 1';
            $resultEquipAudit = $db->query($sqlEquipAudit);
            $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
            $before_value_string = $rowEquipAudit['before_value_string'];
            $after_value_string_e = $rowEquipAudit['after_value_string'];
    
            $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="rms_room_id_c" order by date_created desc limit 1';
            $resultEquipAudit = $db->query($sqlEquipAudit);
            $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
            $before_value_string = $rowEquipAudit['before_value_string'];
            $after_value_string_r = $rowEquipAudit['after_value_string'];
    

            /** Get Related fields of shipping  */

            $sqlShipping_contactid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="ship_to_contact_id" order by date_created desc limit 1';
            $resultShippingcidAudit = $db->query($sqlShipping_contactid);
            $rowShippingcidAudit = $db->fetchByAssoc($resultShippingcidAudit);
            $before_value_string_ship_to_contact_id = $rowShippingcidAudit['before_value_string'];
            $after_value_string_ship_to_contact_id = $rowShippingcidAudit['after_value_string'];

            $sqlShipping_addressid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="ship_to_address_id" order by date_created desc limit 1';
            $resultShippingaddidAudit = $db->query($sqlShipping_addressid);
            $rowShippingaddidAudit = $db->fetchByAssoc($resultShippingaddidAudit);
            $before_value_string_ship_to_address_id = $rowShippingaddidAudit['before_value_string'];
            $after_value_string_ship_to_address_id = $rowShippingaddidAudit['after_value_string'];

            $sqlShipping_companyid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="ship_to_company_id" order by date_created desc limit 1';
            $resultShippingcompanyAudit = $db->query($sqlShipping_companyid);
            $rowShippingcompanyAudit = $db->fetchByAssoc($resultShippingcompanyAudit);
            $before_value_string_ship_to_company_id = $rowShippingcompanyAudit['before_value_string'];
            $after_value_string_ship_to_company_id = $rowShippingcompanyAudit['after_value_string'];

            $inventory_item->save();

            $auditsql_equip = 'update ii_inventory_item_audit 
                             set field_name= "equip_equipment_id_c",
                             before_value_string="' . $after_value_string_e . '",
                             after_value_string="' . $inventory_item->equip_equipment_id_c . '"
                             Where parent_id = "' . $inventory_item->id . '"
                             AND field_name="location_equipment" 
                             order by date_created desc
                             limit 1';
            $db->query($auditsql_equip);
    
    
    
            $auditsql_room = 'update ii_inventory_item_audit 
                             set field_name= "rms_room_id_c",
                             before_value_string="' . $after_value_string_r . '",
                             after_value_string="' . $inventory_item->rms_room_id_c . '"
                             Where parent_id = "' . $inventory_item->id . '"
                             AND field_name="location_room" 
                             order by date_created desc
                             limit 1';
            $db->query($auditsql_room);
            /*Update relate fields of shipping */
            /*Update relate fields of shipping */
            $auditsql_contactid = 'update ii_inventory_item_audit 
            set field_name= "ship_to_contact",
            before_value_string="' . $after_value_string_ship_to_contact_id . '",
            after_value_string="' . $inventory_manager->contact_id_c . '"
            Where parent_id = "' . $inventory_item->id . '"
            AND field_name="ship_to_contact" 
            order by date_created desc
            limit 1';
            $db->query($auditsql_contactid);
             /*Update shipping company id */
            $auditsql_companyid = 'update ii_inventory_item_audit 
            set field_name= "ship_to_company",
            before_value_string="' . $after_value_string_ship_to_company_id . '",
            after_value_string="' . $inventory_manager->account_id_c . '"
            Where parent_id = "' . $inventory_item->id . '"
            AND field_name="ship_to_company" 
            order by date_created desc
            limit 1';
            $db->query($auditsql_companyid);
             /*Update shipping address id */
            $auditsql_addressid = 'update ii_inventory_item_audit 
            set field_name= "ship_to_address",
            before_value_string="' . $after_value_string_ship_to_address_id . '",
            after_value_string="' . $inventory_manager->ca_company_address_id_c . '"
            Where parent_id = "' . $inventory_item->id . '"
            AND field_name="ship_to_address" 
            order by date_created desc
            limit 1';
            $db->query($auditsql_addressid);
           

        } else {
            if($inventory_manager->action!='Processing' && $inventory_manager->action!='Obsolete' && $inventory_manager->action!='Discard')
            {
                
                   /** Get Related fields of shipping  */
            $sqlShipping_contactid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="ship_to_contact" order by date_created desc limit 1';
            $resultShippingcidAudit = $db->query($sqlShipping_contactid);
            $rowShippingcidAudit = $db->fetchByAssoc($resultShippingcidAudit);
            $before_value_string_ship_to_contact_id = $rowShippingcidAudit['before_value_string'];
            $after_value_string_ship_to_contact_id = $rowShippingcidAudit['after_value_string'];

            $sqlShipping_addressid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="ship_to_address" order by date_created desc limit 1';
            $resultShippingaddidAudit = $db->query($sqlShipping_addressid);
            $rowShippingaddidAudit = $db->fetchByAssoc($resultShippingaddidAudit);
            $before_value_string_ship_to_address_id = $rowShippingaddidAudit['before_value_string'];
            $after_value_string_ship_to_address_id = $rowShippingaddidAudit['after_value_string'];

            $sqlShipping_companyid = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="ship_to_company" order by date_created desc limit 1';
            $resultShippingcompanyAudit = $db->query($sqlShipping_companyid);
            $rowShippingcompanyAudit = $db->fetchByAssoc($resultShippingcompanyAudit);
            $before_value_string_ship_to_company_id = $rowShippingcompanyAudit['before_value_string'];
            $after_value_string_ship_to_company_id = $rowShippingcompanyAudit['after_value_string'];

                $inventory_item->contact_id_c = null;
                $inventory_item->ship_to_contact = null;
    
                $inventory_item->ca_company_address_id_c = null;
                $inventory_item->ship_to_address = null;
    
                $inventory_item->account_id_c = null;
                $inventory_item->ship_to_company = null;
               
                $inventory_item->save();

                 /*Update relate fields of shipping */
            /*Update relate fields of shipping */
            $auditsql_contactid = 'update ii_inventory_item_audit 
            set field_name= "ship_to_contact",
            before_value_string="' . $after_value_string_ship_to_contact_id . '",
            after_value_string="' . $inventory_item->contact_id_c . '"
            Where parent_id = "' . $inventory_item->id . '"
            AND field_name="ship_to_contact" 
            order by date_created desc
            limit 1';
            $db->query($auditsql_contactid);
             /*Update shipping company id */
            $auditsql_companyid = 'update ii_inventory_item_audit 
            set field_name= "ship_to_company",
            before_value_string="' . $after_value_string_ship_to_company_id . '",
            after_value_string="' . $inventory_item->account_id_c . '"
            Where parent_id = "' . $inventory_item->id . '"
            AND field_name="ship_to_company" 
            order by date_created desc
            limit 1';
            $db->query($auditsql_companyid);
             /*Update shipping address id */
            $auditsql_addressid = 'update ii_inventory_item_audit 
            set field_name= "ship_to_address",
            before_value_string="' . $after_value_string_ship_to_address_id . '",
            after_value_string="' . $inventory_item->ca_company_address_id_c . '"
            Where parent_id = "' . $inventory_item->id . '"
            AND field_name="ship_to_address" 
            order by date_created desc
            limit 1';
            $db->query($auditsql_addressid);

            }
           
        }
    }

    function calculateBeforeSave($bean, $event, $arguments) {

        global $db;
        
        $itemId = $bean->id;

        $sql = "SELECT * FROM `ii_inventory_item_im_inventory_management_1_c` WHERE ii_inventory_item_im_inventory_management_1ii_inventory_item_ida='" . $itemId . "' AND deleted=0";

        $result = $db->query($sql);
        $mostRecentIm = null;
        $mostRecentImName = null;
        $previousDate = "";
        while ($row = $db->fetchByAssoc($result)) {
            $im_id = $row['ii_invento5fdeagement_idb'];

            $inventory_manager = BeanFactory::retrieveBean('IM_Inventory_Management', $im_id);
            $date_entered = $inventory_manager->date_entered;

            if (($previousDate == "" || $previousDate < strtotime($date_entered)) && ($inventory_manager->action!='Processing' && $inventory_manager->action!='Obsolete' && $inventory_manager->action!='Discard')) {
                $mostRecentIm = $inventory_manager->id;
                $mostRecentImName = $inventory_manager->name;
                $previousDate = strtotime($date_entered);

               
                $ship_to_contact_id = $inventory_manager->contact_id_c;
                $ship_to_contact_name = $inventory_manager->ship_to_contact;                
                 
                $ship_to_address_id = $inventory_manager->ca_company_address_id_c;
                $ship_to_address_name = $inventory_manager->ship_to_address;
               
                $ship_to_company_id = $inventory_manager->account_id_c;
                $ship_to_company_name = $inventory_manager->ship_to_company;
            }
        }
        //if($inventory_manager->action!='Processing' && $inventory_manager->action!='Obsolete' &&  $inventory_manager->action!='Discard')
        /*07 June 2021 : to fix the issue of shipping field before and after string blank on unlink shipping IM record */
        if($inventory_manager->action=='Archive Offsite' || $inventory_manager->action=='External Transfer')
        {
            $bean->contact_id_c = $ship_to_contact_id;
            $bean->ship_to_contact = $ship_to_contact_name;            
            $bean->ca_company_address_id_c = $ship_to_address_id;
            $bean->ship_to_address = $ship_to_address_name;
            $bean->account_id_c = $ship_to_company_id;
            $bean->ship_to_company = $ship_to_company_name;           

        }
        
    }

    private function retrieveMostRecentIm($item) {
        if ($item->load_relationship("ii_inventory_item_im_inventory_management_1")) {
            $ims = $item->ii_inventory_item_im_inventory_management_1->getBeans();

            $mostRecentIm = null;
            foreach ($ims as $im) {
                $date_entered = $im->date_entered;
                if ($mostRecentIm === null) {
                    $mostRecentIm = $im;
                }

                if (strtotime($mostRecentIm->date_entered) < strtotime($date_entered)) {
                    $mostRecentIm = $im;
                }
            }

            return $mostRecentIm;
        } else {
            return null;
        }
    }

}
