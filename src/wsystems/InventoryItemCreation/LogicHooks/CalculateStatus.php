<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class CalculateStatus
{

    public function calculate($bean, $event, $arguments)
    {
        $module = $arguments['module'];
        $modelId = $arguments['id'];

        $relatedModule = $arguments['related_module'];
        $relatedId = $arguments['related_id'];

        if ($module === 'II_Inventory_Item' && $relatedModule === 'IM_Inventory_Management') {
            $this->calcStatus($modelId, $relatedId);
        } else if ($relatedModule === 'II_Inventory_Item' && $module === 'IM_Inventory_Management') {
            $this->calcStatus($relatedId, $modelId);
        }
    }

    public function calcStatus($id, $rel_id)
    {
        global $db, $current_user;
        $inventory_item = BeanFactory::retrieveBean('II_Inventory_Item', $id);
        $inventory_manager = $this->retrieveMostRecentIm($inventory_item);

        if ($inventory_item && $inventory_manager) {
            $inventory_item->dontChangeName = true;
            $exp_Date_time = date('Y-m-d H:i:s', strtotime($inventory_item->expiration_date_time));
            $expDatetime = date('Y-m-d 0:0:0', strtotime($inventory_item->expiration_date));
            $currentTime = date('Y-m-d 0:0:0', time());
            $currentDateTime = date('Y-m-d H:i:s', time());

            $type = $inventory_manager->action;

            $status = 'Planned Inventory';
            if ($type === 'In Use' || $type === 'Initial Storage' || $type === 'Processing' || $type === 'Internal Transfer' || $type === 'Create Inventory Collection' || $type === 'Separate Inventory Items') {
                if (isset($inventory_item->expiration_date) && $expDatetime < $currentTime && $inventory_item->expiration_date != "") {
                    $status = "Onsite Inventory Expired";
                } else if (isset($inventory_item->expiration_date_time) && $exp_Date_time < $currentDateTime && $inventory_item->expiration_date_time != "") {
                    $status = "Onsite Inventory Expired";
                } else {
                    $status = 'Onsite Inventory';
                }
            } else if ($type === 'External Transfer') {

                if (isset($inventory_item->expiration_date) && $expDatetime < $currentTime && $inventory_item->expiration_date != "") {
                    $status = "Offsite Inventory Expired";
                } else if (isset($inventory_item->expiration_date_time) && $exp_Date_time < $currentDateTime && $inventory_item->expiration_date_time != "") {
                    $status = "Offsite Inventory Expired";
                } else {
                    $status = 'Offsite Inventory';
                }
            } else if ($type === 'Archive Onsite') {

                if (isset($inventory_item->expiration_date) && $expDatetime < $currentTime && $inventory_item->expiration_date != "") {
                    $status = "Onsite Archived Expired";
                } else if (isset($inventory_item->expiration_date_time) && $exp_Date_time < $currentDateTime && $inventory_item->expiration_date_time != "") {
                    $status = "Onsite Archived Expired";
                } else {
                    $status = 'Onsite Archived';
                }
            } else if ($type === 'Archive Offsite') {

                if (isset($inventory_item->expiration_date) && $expDatetime < $currentTime && $inventory_item->expiration_date != "") {
                    $status = "Offsite Archived Expired";
                } else if (isset($inventory_item->expiration_date_time) && $exp_Date_time < $currentDateTime && $inventory_item->expiration_date_time != "") {
                    $status = "Offsite Archived Expired";
                } else {
                    $status = 'Offsite Archived';
                }
            } else if ($type === 'Discard') {
                $status = 'Discarded';
            } else if ($type === 'Obsolete' || $type === 'Missing') {
                $status = 'Obsolete';
            }else if ($type === 'Exhausted' || $type === 'Internal Transfer Analysis') {
                $status = 'Exhausted';
            }
    
            $inventory_item->status = $status;
            if ($inventory_manager->end_condition != "") {
                $inventory_item->storage_condition = $inventory_manager->end_condition;
            }
            if ($inventory_item->type_2 == "Tissue" && $inventory_manager->ending_storage_media != "") {
                $inventory_item->storage_medium = $inventory_manager->ending_storage_media;
            }
            $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="equip_equipment_id_c" order by date_created desc limit 1';
            $resultEquipAudit = $db->query($sqlEquipAudit);
            $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
            $before_value_string = $rowEquipAudit['before_value_string'];
            $after_value_string_e = $rowEquipAudit['after_value_string'];

            $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="rms_room_id_c" order by date_created desc limit 1';
            $resultEquipAudit = $db->query($sqlEquipAudit);
            $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
            $before_value_string = $rowEquipAudit['before_value_string'];
            $after_value_string_r = $rowEquipAudit['after_value_string'];

            $inventory_item->save();

            $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "equip_equipment_id_c",
                         before_value_string="' . $after_value_string_e . '",
                         after_value_string="' . $inventory_item->equip_equipment_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_equipment" 
                         order by date_created desc
                         limit 1';
            $db->query($auditsql);

            
            $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "rms_room_id_c",
                         before_value_string="' . $after_value_string_r . '",
                         after_value_string="' . $inventory_item->rms_room_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_room" 
                         order by date_created desc
                         limit 1';
            $db->query($auditsql);
        } else {
            $status = 'Planned Inventory';
            $inventory_item->status = $status;
           
            $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="equip_equipment_id_c" order by date_created desc limit 1';
            $resultEquipAudit = $db->query($sqlEquipAudit);
            $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
            $before_value_string = $rowEquipAudit['before_value_string'];
            $after_value_string_e = $rowEquipAudit['after_value_string'];

            $sqlEquipAudit = 'select * from ii_inventory_item_audit where parent_id="' . $inventory_item->id . '" and field_name="rms_room_id_c" order by date_created desc limit 1';
            $resultEquipAudit = $db->query($sqlEquipAudit);
            $rowEquipAudit = $db->fetchByAssoc($resultEquipAudit);
            $before_value_string = $rowEquipAudit['before_value_string'];
            $after_value_string_r = $rowEquipAudit['after_value_string'];

            $inventory_item->save();           

            $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "equip_equipment_id_c",
                         before_value_string="' . $after_value_string_e . '",
                         after_value_string="' . $inventory_item->equip_equipment_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_equipment" 
                         order by date_created desc
                         limit 1';
            $db->query($auditsql);

            $auditsql = 'update ii_inventory_item_audit 
                         set field_name= "rms_room_id_c",
                         before_value_string="' . $after_value_string_r . '",
                         after_value_string="' . $inventory_item->rms_room_id_c . '"
                         Where parent_id = "' . $inventory_item->id . '"
                         AND field_name="location_room" 
                         order by date_created desc
                         limit 1';
            $db->query($auditsql);
        }
    }

    function calculateBeforeSave($bean, $event, $arguments)
    {
        global $db;
        $itemId = $bean->id;
        $expDatetime = date('Y-m-d 0:0:0', strtotime($bean->expiration_date));
        $exp_Date_time = date('Y-m-d H:i:s', strtotime($bean->expiration_date_time));
        $currentTime = date('Y-m-d 0:0:0', time());
        $currentDateTime = date('Y-m-d H:i:s', time());
        $sql = "SELECT * FROM `ii_inventory_item_im_inventory_management_1_c` WHERE ii_inventory_item_im_inventory_management_1ii_inventory_item_ida='" . $itemId . "' AND deleted=0";
        $result = $db->query($sql);
        $mostRecentIm = null;
        $mostRecentImName = null;
        $previousDate = "";
        while ($row = $db->fetchByAssoc($result)) {
            $im_id = $row['ii_invento5fdeagement_idb'];

            $inventory_Mgt = BeanFactory::retrieveBean('IM_Inventory_Management', $im_id);
            $date_entered = $inventory_Mgt->date_entered;

            if ($previousDate == "" || $previousDate < strtotime($date_entered)) {
                $mostRecentIm = $inventory_Mgt->id;
                $mostRecentImName = $inventory_Mgt->name;
                $previousDate = strtotime($date_entered);

                $type = $inventory_Mgt->action;
                $end_condition = $inventory_Mgt->end_condition;
                $ending_storage_media = $inventory_Mgt->ending_storage_media;
            }
        }

        $status = 'Planned Inventory';
        if ($type === 'In Use' || $type === 'Initial Storage' || $type === 'Processing' || $type === 'Internal Transfer' || $type === 'Create Inventory Collection' || $type === 'Separate Inventory Items') {
            if (isset($bean->expiration_date) && $expDatetime < $currentTime && $bean->expiration_date != "") {
                $status = "Onsite Inventory Expired";
            } else if (isset($bean->expiration_date_time) && $exp_Date_time < $currentDateTime && $bean->expiration_date_time != "") {
                $status = "Onsite Inventory Expired";
            } else {
                $status = 'Onsite Inventory';
            }
        } else if ($type === 'External Transfer') {

            if (isset($bean->expiration_date) && $expDatetime < $currentTime && $bean->expiration_date != "") {
                $status = "Offsite Inventory Expired";
            } else if (isset($bean->expiration_date_time) && $exp_Date_time < $currentDateTime && $bean->expiration_date_time != "") {
                $status = "Offsite Inventory Expired";
            } else {
                $status = 'Offsite Inventory';
            }
        } else if ($type === 'Archive Onsite') {

            if (isset($bean->expiration_date) && $expDatetime < $currentTime && $bean->expiration_date != "") {
                $status = "Onsite Archived Expired";
            } else if (isset($bean->expiration_date_time) && $exp_Date_time < $currentDateTime && $bean->expiration_date_time != "") {
                $status = "Onsite Archived Expired";
            } else {
                $status = 'Onsite Archived';
            }
        } else if ($type === 'Archive Offsite') {

            if (isset($bean->expiration_date) && $expDatetime < $currentTime && $bean->expiration_date != "") {
                $status = "Offsite Archived Expired";
            } else if (isset($bean->expiration_date_time) && $exp_Date_time < $currentDateTime && $bean->expiration_date_time != "") {
                $status = "Offsite Archived Expired";
            } else {
                $status = 'Offsite Archived';
            }
        } else if ($type === 'Discard') {
            $status = 'Discarded';
        } else if ($type === 'Obsolete' || $type === 'Missing') {
            $status = 'Obsolete';            
        }else if ($type === 'Exhausted' || $type === 'Internal Transfer Analysis') {
            $status = 'Exhausted';
            $bean->location_type = null;
            $bean->location_building = null;
            $bean->rms_room_id_c = null;
            $bean->location_room = null;
            $bean->location_equipment = null;
            $bean->equip_equipment_id_c = null;
            $bean->location_shelf = null;
            $bean->location_cabinet = null;
            $bean->location_bin_c = null;
            $bean->location_cubby_c = null;
            $bean->location_rack_c = null;
            $bean->test_ii_c = null;
        }

        if ($end_condition != "") {
            $bean->storage_condition = $end_condition;
        }
        if ($bean->type_2 == "Tissue" && $ending_storage_media != "") {
            $bean->storage_medium = $ending_storage_media;
        }
        
        $bean->status = $status;
    }

    private function retrieveMostRecentIm($item)
    {
        if ($item->load_relationship("ii_inventory_item_im_inventory_management_1")) {
            $ims = $item->ii_inventory_item_im_inventory_management_1->getBeans();

            $mostRecentIm = null;
            foreach ($ims as $im) {
                $date_entered = $im->date_entered;
                if ($mostRecentIm === null) {
                    $mostRecentIm = $im;
                }

                if (strtotime($mostRecentIm->date_entered) < strtotime($date_entered)) {
                    $mostRecentIm = $im;
                }
            }

            return $mostRecentIm;
        } else {
            return null;
        }
    }
}
