<?php

/**
 * When a record is saved, identify the next number sequence and append it to the name
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class SetNameSequence
{
    public function setName($bean, $event, $arguments)
    {

        //remove extra spaces
        $bean->name = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $bean->name)));

        $existingSequence = null;
        if ($bean->fetched_row) {
            $previous_name = $bean->fetched_row['name'];

            $existingSequence = substr($previous_name, -3);
            if (!is_numeric($existingSequence) === true) {
                $existingSequence = null;
            }
        }
        $seq        = $this->getSequence($bean, $existingSequence);
        $bean->name = trim($bean->name);

        if ($existingSequence === null) {
            $bean->name .= " " . $seq;
        } else if ($existingSequence !== $seq) {
            $bean->name = substr($bean->name, 0, -3) . " " . $seq;
        } else if ($existingSequence === $seq) {
            $eseq = substr($bean->name, -3);
            if (is_numeric($eseq) === true) {
                $bean->name = substr($bean->name, 0, -3) . " " . $seq;
            } else {
                $bean->name .= " " . $seq;
            }
        }

        $bean->name = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $bean->name)));
        //Update name on Edit time bug 2062
        if ($arguments['isUpdate'] == true && $bean->id == $bean->fetched_row['id']) {
            $bean_Name = $this->updateIIName($bean);
            $bean->type_2 = $bean->fetched_row['type_2'];
            $bean->name = $bean_Name;
        }
    }

    public function updateIIName($bean)
    {
        global $db, $app_list_strings;

        $II_type_dom = $app_list_strings['inventory_item_type_list'];
        $II_Subtype_dom = $app_list_strings['inventory_item_subtype_list'];
        $II_product_dom = $app_list_strings['pro_subtype_list'];
        $II_specimen_dom = $app_list_strings['tissues_list'];
        /**10 March 2022 : #2218 : Name updation changes */
        $II_test_type_dom = $app_list_strings['ii_test_type_list'];
        $II_timepoint_dom = $app_list_strings['timepoint_type_list'];
        $II_time_unit_dom = $app_list_strings['ii_test_type_list'];
        /**/ //////////////////////////////////////////// */


        $IINameNew = "";
        $relatedName = "";
        $nameLast3Digit = substr($bean->name, -3);
        if ($bean->related_to_c == "Sales") {
            $sqlSales = 'SELECT name FROM m01_sales WHERE id = "' . $bean->m01_sales_ii_inventory_item_1m01_sales_ida . '"';
            $resultSales = $db->query($sqlSales);
            if ($resultSales->num_rows > 0) {
                $rowSales = $db->fetchByAssoc($resultSales);
                $SaleName = $rowSales['name'];
                $relatedName = $SaleName;
            }           
        }

        if ($bean->related_to_c == "Work Product" || $bean->related_to_c == "Single Test System" || $bean->related_to_c == "Multiple Test Systems") {
            $sqlWp = 'SELECT name FROM m03_work_product WHERE id = "' . $bean->m03_work_product_ii_inventory_item_1m03_work_product_ida . '"';
            $resultWp = $db->query($sqlWp);
            if ($resultWp->num_rows > 0) {
                $rowWp = $db->fetchByAssoc($resultWp);
                $WpName = $rowWp['name'];
                $relatedName = $WpName;
            }

        }

        if ($bean->anml_animals_ii_inventory_item_1anml_animals_ida != "") {
            $arrPullTSId = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
            $arrPullUSDAId = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

            $testSystemBean = BeanFactory::retrieveBean("ANML_Animals", $bean->anml_animals_ii_inventory_item_1anml_animals_ida);
            $species = $testSystemBean->species_2_c;
            $TsNameAppend = "";
            if (in_array($species, $arrPullTSId)) {
                $TsNameAppend = $testSystemBean->name;
            } else if (in_array($species, $arrPullUSDAId)) {
                if ($testSystemBean->usda_id_c != "") {
                    $TsNameAppend = $testSystemBean->usda_id_c;
                } else {
                    $TsNameAppend = $testSystemBean->name;
                }
            } else {
                $TsNameAppend = $testSystemBean->name;
            }

        }

        if ($relatedName != "") {
            $IINameNew = $relatedName;
        }

        if (!empty($TsNameAppend)) {
            $IINameNew .= " " . $TsNameAppend;
        }

        if (!empty($bean->product_name)) {
            $IINameNew .= " " . $bean->product_name;
        }

        if (!empty($bean->fetched_row['type_2'])) {
            if ($bean->fetched_row['type_2'] == "Other") {
                if (($bean->fetched_row['category'] == "Specimen" && $bean->fetched_row['type_2'] == "Other") || ($bean->fetched_row['specimen_type_1_c'] == "Other")) {
                    $IINameNew .= " " . $bean->other_type_c;
                } else {
                    $IINameNew .= " " . $II_type_dom[$bean->fetched_row['type_2']];
                }
            } else {
                $IINameNew .= " " . $II_type_dom[$bean->fetched_row['type_2']];
            }
        }
        /**10 March 2022 : #2218 : Name updation changes */
        if (!empty($bean->fetched_row['test_type_c'])) {
             if ($bean->fetched_row['test_type_c'] == "Other") {
                if (($bean->fetched_row['category'] == "Specimen" && $bean->fetched_row['test_type_c'] == "Other") || ($bean->fetched_row['specimen_type_1_c'] == "Other")) {
                    $IINameNew .= " " . $bean->other_test_type_c;
                } else {
                    $IINameNew .= " " . $II_test_type_dom[$bean->fetched_row['test_type_c']];
                }
            } else {
                $IINameNew .= " " . $II_test_type_dom[$bean->fetched_row['test_type_c']];
            }
        }

        if ($bean->timepoint_type != "undefined" && $bean->timepoint_type != "Defined" && $bean->timepoint_type != "Ad Hoc") {
             $IINameNew .= " " . $II_timepoint_dom[$bean->timepoint_type];            
        } else if (($bean->timepoint_type == "Ad Hoc" || $bean->timepoint_type == "Defined")) {
            //$IINameNew .= " " . $bean->timepoint_name_c;
            if ($bean->timepoint_name_c != "") {                                
                $timepoint_name_c = " " .$bean->timepoint_name_c;
            }
            else
            {
                $timepoint_name_c = $bean->fetched_row['timepoint_name_c'];
            }
            if ($bean->timepoint_unit != "") {
                $timepoint_unit = " " . $bean->timepoint_unit;               
            }
            else
            {
                $timepoint_unit = $bean->fetched_row['timepoint_unit'];              
            }
            if ($bean->timepoint_integer != "") {                                
                $timepoint_integer = " " . $bean->timepoint_integer;
            }
            else
            {
                $timepoint_integer = $bean->fetched_row['timepoint_integer'];
            }
            if ($bean->timepoint_end_integer_c != "") {                      
               
                if($bean->timepoint_integer!='')
                {
                    $timepoint_end_integer_c = "-" . $bean->timepoint_end_integer_c;
                }
                else
                {
                    $timepoint_end_integer_c = "" . $bean->fetched_row['timepoint_integer'];
                }
                
            }
            else
            {
                $timepoint_end_integer_c = $bean->fetched_row['timepoint_end_integer_c'];
            }   
            $IINameNew .= " " . $timepoint_name_c . $timepoint_unit . $timepoint_integer . $timepoint_end_integer_c;
           
        }

        /**/ //////////////////////////////////////////// */

        if (!empty($bean->fetched_row['subtype'])) {
            if ($bean->fetched_row['type_2'] == "Equipment Facility Record") {
                $IINameNew .= " " . $II_Subtype_dom[$bean->fetched_row['subtype']];
            }
            if ($bean->fetched_row['type_2'] == "Block" || $bean->fetched_row['type_2'] == "Culture" || $bean->fetched_row['type_2'] == "Slide" || $bean->fetched_row['type_2'] == "Tissue") {
                $IINameNew .= " " . $II_specimen_dom[$bean->fetched_row['subtype']];
            }
            if ($bean->fetched_row['category'] == "Product") {
                $IINameNew .= " " . $II_product_dom[$bean->fetched_row['subtype']];
            }
        }
        $beanName = $IINameNew . " " . $nameLast3Digit;
        return $beanName;
    }

    private function getSequence($bean, $existingSequence)
    {
        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::newBean($bean->module_dir));
        $sugarQuery->select(array("id", "name"));
        $sugarQuery->where()->contains("name", $bean->name);
        $resultSet = $sugarQuery->execute();

        $sequences = array();
        foreach ($resultSet as $row) {
            $row_name = $row["name"];
            $row_seq  = substr($row_name, -3);
            if (is_numeric($row_seq)) {
                $sequences[] = intval($row_seq);
            }
        }

        if (count($sequences) > 0) {
            $max_seq = max($sequences);

            if ($existingSequence === null) {
                $max_seq += 1;
            }

            if ($max_seq < 100 && $max_seq > 9) {
                $seq = "0" . $max_seq;
            } else if ($max_seq <= 9) {
                $seq = "00" . $max_seq;
            } else {
                $seq = $max_seq;
            }
        } else {
            $seq = "001";
        }
        return $seq;
    }
}
