<?php

namespace Sugarcrm\Sugarcrm\custom\wsystems\PopulateInventorySubpanelsFromWorkProduct\LogicHooks;

class IIDeleteRelationshipHook
{
    public function deleteIISubpanelRelationship($bean, $event, $arguments)
    {
        $conn = \DBManagerFactory::getInstance()->getConnection();

        if ($arguments["related_module"] === "M03_Work_Product") {

            $inventoryItemsId = $arguments["id"];
            $workProductId    = $arguments['related_id'];

            $getInventoryItemId = <<<SQL
            SELECT
                *
            FROM
                m03_work_product_ii_inventory_item_2_c
            WHERE
                m03_work_product_ii_inventory_item_2ii_inventory_item_idb = ?
                AND m03_work_product_ii_inventory_item_2m03_work_product_ida = ?
                AND deleted = ?
SQL;

            $stmt = $conn->executeQuery($getInventoryItemId, array($inventoryItemsId, $workProductId, 0));

            while ($row = $stmt->fetch()) {
                if (isset($row['id'])) {
                    $unlinkIIRelationship = <<<SQL
UPDATE m03_work_product_ii_inventory_item_2_c
SET
    deleted = ?
WHERE
    m03_work_product_ii_inventory_item_2ii_inventory_item_idb = ?
        AND m03_work_product_ii_inventory_item_2m03_work_product_ida = ?
        AND deleted = ?
SQL;

                    $conn->executeQuery($unlinkIIRelationship, array(1, $inventoryItemsId, $workProductId, 0));
                }
            }
        }
    }
}
