<?php
/*
namespace Sugarcrm\Sugarcrm\custom\wsystems\PopulateInventorySubpanelsFromWorkProduct\LogicHooks;

class IMDeleteRelationshipHook {

    public function deleteIMSubpanelRelationship($bean, $event, $arguments) {
        $conn = \DBManagerFactory::getInstance()->getConnection();

        if ($arguments["related_module"] === "M03_Work_Product") {
            $inventoryManagementId = $arguments["id"];
            $workProductId = $arguments['related_id'];

            $getInventoryManagementId = <<<SQL
SELECT
    *
FROM
m03_work_product_im_inventory_management_2_c
WHERE
    m03_work_p7b19agement_idb = ?
    AND m03_work_product_im_inventory_management_2m03_work_product_ida = ?
    AND deleted = ?
SQL;

            $stmt = $conn->executeQuery($getInventoryManagementId, array($inventoryManagementId, $workProductId, 0));

            while ($row = $stmt->fetch()) {
                if (isset($row['id'])) {
                    $unlinkIMRelationship = <<<SQL
UPDATE m03_work_product_im_inventory_management_2_c
SET
    deleted = ?
WHERE
m03_work_p7b19agement_idb = ?
        AND m03_work_product_im_inventory_management_2m03_work_product_ida = ?
        AND deleted = ?
SQL;

                    $conn->executeQuery($unlinkIMRelationship, array(1, $inventoryManagementId, $workProductId, 0));
                }
            }
        }
    }

}

*/