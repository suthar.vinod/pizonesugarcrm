(function jsExtension(app) {
    app.events.on("app:sync:complete", function appSyncCompleteEvt() {
        if (!app.view.layouts.BaseM03_Work_ProductSubpanelsLayout && !app.view.layouts.BaseM03_Work_ProductCustomSubpanelsLayout) {
            app.view.declareComponent("layout", "subpanels", "M03_Work_Product", undefined, false, "base");
        }

        var workProductSubpanelsLayout = "BaseM03_Work_ProductSubpanelsLayout";

        if (app.view.layouts.BaseM03_Work_ProductCustomSubpanelsLayout) {
            workProductSubpanelsLayout = "BaseM03_Work_ProductCustomSubpanelsLayout";
        }

        if (App.view.layouts[workProductSubpanelsLayout].hideSubpanelsOnWorkProduct === true) {
            return;
        }

        App.view.layouts[workProductSubpanelsLayout] = App.view.layouts[workProductSubpanelsLayout].extend({
            hideSubpanelsOnWorkProduct: true,

            subpanelLinksToRemove: ["m03_work_product_ii_inventory_item_2"],

            initialize: function (options) {
                var parent = this._super("initialize", arguments);

                if (this.canRemoveSubpanels()) {
                    this.removeSubpanels();
                }

                return parent;
            },

            canRemoveSubpanels: function () {
                var context = this.context;

                return context.get("module") === "M03_Work_Product" && context.get("layout") === "record" && this.name === "subpanels";
            },

            removeSubpanels: function () {
                var subpanels = this.meta.components;

                if (_.isEmpty(subpanels)) {
                    return;
                }

                _.each(
                    subpanels,
                    function removeSubpanel(subpanel, index) {
                        var subpanelLink;

                        try {
                            subpanelLink = subpanel.context.link;
                        } catch (e) {
                            subpanelLink = undefined;
                        }

                        if (_.isUndefined(subpanelLink)) {
                            return;
                        }

                        if (this.subpanelLinksToRemove.indexOf(subpanelLink) !== -1) {
                            this.meta.components.splice(index, 1);
                        }
                    }.bind(this)
                );
            }
        });
    });
})(SUGAR.App);