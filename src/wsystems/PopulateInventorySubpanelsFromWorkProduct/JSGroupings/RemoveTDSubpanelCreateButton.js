(function appFunction(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseTaskD_Task_DesignPanelTopView &&
            !app.view.views.BaseTaskD_Task_DesignCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "TaskD_Task_Design", undefined, false, "base");
        }

        var TDPanelTop = "BaseTaskD_Task_DesignPanelTopView";

        if (app.view.views.BaseTaskD_Task_DesignCustomPanelTopView) {
            TDPanelTop = "BaseTaskD_Task_DesignCustomPanelTopView";
        }

        if (App.view.views[TDPanelTop].TDRmoveCreateButton === true) {
            return;
        }

        App.view.views[TDPanelTop] = App.view.views[TDPanelTop].extend({
            TDRmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "M03_Work_Product") {
                    this._removeTDSubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeTDSubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);