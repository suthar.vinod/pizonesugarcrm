(function appFunction(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseII_Inventory_ItemPanelTopView &&
            !app.view.views.BaseII_Inventory_ItemCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "II_Inventory_Item", undefined, false, "base");
        }

        var IIPanelTop = "BaseII_Inventory_ItemPanelTopView";

        if (app.view.views.BaseII_Inventory_ItemCustomPanelTopView) {
            IIPanelTop = "BaseII_Inventory_ItemCustomPanelTopView";
        }

        if (App.view.views[IIPanelTop].IIRmoveCreateButton === true) {
            return;
        }

        App.view.views[IIPanelTop] = App.view.views[IIPanelTop].extend({
            IIRmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "M03_Work_Product") {
                    this._removeIISubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeIISubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);