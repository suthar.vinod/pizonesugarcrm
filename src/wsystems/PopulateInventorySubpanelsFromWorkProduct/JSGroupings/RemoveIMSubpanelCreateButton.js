(function appFunction(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseIM_Inventory_ManagementPanelTopView &&
            !app.view.views.BaseIM_Inventory_ManagementCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "IM_Inventory_Management", undefined, false, "base");
        }

        var IMPanelTop = "BaseIM_Inventory_ManagementPanelTopView";

        if (app.view.views.BaseIM_Inventory_ManagementCustomPanelTopView) {
            IMPanelTop = "BaseIM_Inventory_ManagementCustomPanelTopView";
        }

        if (App.view.views[IMPanelTop].IMRmoveCreateButton === true) {
            return;
        }

        App.view.views[IMPanelTop] = App.view.views[IMPanelTop].extend({
            IMRmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "M03_Work_Product") {
                    this._removeIMSubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeIMSubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);