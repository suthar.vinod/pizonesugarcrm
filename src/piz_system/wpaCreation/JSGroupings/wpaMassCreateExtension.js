(function appFunct(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        //alert("xvxc");
        if (!app.view.views.BaseWPE_Work_Product_EnrollmentCreateView && !app.view.views.BaseWPE_Work_Product_EnrollmentCustomWpemasscreateView) {
            app.view.declareComponent("view", "create", "WPE_Work_Product_Enrollment", undefined, false, "base");
        }
        var createView = "BaseWPE_Work_Product_EnrollmentCreateView";
        if (app.view.views.BaseWPE_Work_Product_EnrollmentCustomWpemasscreateView) {
            createView = "BaseWPE_Work_Product_EnrollmentCustomWpemasscreateView";
        }
        if (App.view.views[createView].createExtended === true) {
            return;
        }

        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function () {
                this._super("initialize", arguments);
                this.model.on('change:m03_work_product_wpe_work_product_enrollment_1_name', this.function_usda1, this);

                //this.context.on('button:save_button:click', this._save_desc, this);


                //this.model.on("change:related_to", this._hideSidebarMainPane, this);
                this.on('render', _.bind(this.onload_function, this));

            },
            onload_function: function () {
                var self = this;
                var WPE_Status_List = app.lang.getAppListStrings('enrollment_status_list');
                //console.log('WPE_Status_List',WPE_Status_List);
                Object.keys(WPE_Status_List).forEach(function (key) {
                    delete WPE_Status_List[key];
                });
                //WPE_Status_List[''] = "";
                WPE_Status_List['On Study'] = "On Study";
                this.model.fields['enrollment_status_c'].options = WPE_Status_List;
                this.model.set('enrollment_status_c', "On Study");
                this._render();
            },
            function_usda1: function () {

                if (this.model.get('m03_work_product_wpe_work_product_enrollment_1_name') != "") {
                    var self = this;
                    var wpid = this.model.get('m03_work_p7d13product_ida');
                    var self = this;
                    App.api.call("get", "rest/v10/M03_Work_Product/" + wpid + "?fields=usda_category_multiselect_c", null, {
                        success: function (Data) {
                            if (Data.usda_category_multiselect_c != "") {
                                var ch1 = '';
                                var catArr = Data.usda_category_multiselect_c;
                                catArr.sort();
                                ch = catArr[catArr.length - 1];
                                res = ch.replace("^", "");
                                ch1 = res.replace("^", "");
                                self.model.set('usda_category_c', ch1);
                            }
                        }
                    });

                    app.api.call("GET", app.api.buildURL("M03_Work_Product/" + wpid + "/get_blanket_product"), null, {
                        success: function (data) {
                            self.model.set('m03_work_p9f23product_ida', data.workID);
                            self.model.set('m03_work_product_wpe_work_product_enrollment_2_name', data.blanket_protocol_c);
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                    /*23 March 2021 : #980 */
                    var models = self.context.parent.get('mass_collection').models;
                    var totalCount = 0;
                    _.each(models, function (selectedModel) {
                        var tsName = selectedModel.get('name');
                        var tsAssignedWP = selectedModel.get('assigned_to_wp_c');
                        var tsAllocatedWP = selectedModel.get('m03_work_product_id_c');
                        var tsEnrollmentStatus = selectedModel.get('enrollment_status');
                        var tsTerminationDate = selectedModel.get('termination_date_c');
                        totalCount++;

                    });
                    //alert(totalCount);
                    var WP_API = "rest/v10/M03_Work_Product/" + wpid + "?fields=primary_animals_countdown_c";

                    App.api.call("get", WP_API, null, {
                        success: function (WPData) {
                            console.log('WP_API data : ', WPData);
                            console.log('WP_API data : primary_animals_countdown_c : ', WPData.primary_animals_countdown_c);
                            if (totalCount > WPData.primary_animals_countdown_c) {
                                app.alert.show("NotEnoughAnimalRemain", {
                                    level: "error",
                                    messages: "There are not enough Animals Remaining on the chosen Work Product to allow creation of Work Product Assignment(s). Please update Work Product."
                                });
                                self.model.set('m03_work_p7d13product_ida', '');
                                self.model.set('m03_work_product_wpe_work_product_enrollment_1_name', '');

                            }
                        }
                    });
                }
            },
        });
    });
})(SUGAR.App);