(function appFunct(app) {
  app.events.on("app:sync:complete", function appSyncComplete() {
    if (!app.view.views.BaseWPE_Work_Product_EnrollmentCreateView && !app.view.views.BaseWPE_Work_Product_EnrollmentCustomCreateView) {
      app.view.declareComponent("view", "create", "WPE_Work_Product_Enrollment", undefined, false, "base");
    }
    var createView = "BaseWPE_Work_Product_EnrollmentCreateView";
    if (app.view.views.BaseWPE_Work_Product_EnrollmentCustomCreateView) {
      createView = "BaseWPE_Work_Product_EnrollmentCustomCreateView";
    }
    if (App.view.views[createView].createExtended === true) {
      return;
    }

    App.view.views[createView] = App.view.views[createView].extend({
      createExtended: true,
      initialize: function () {
        this._super("initialize", arguments);
        this.model.on('change:anml_animals_wpe_work_product_enrollment_1_name', this.function_TestSystem, this);
        this.model.on('change:m03_work_product_wpe_work_product_enrollment_1_name', this.function_WP, this);
        this.model.on('change:select_aps001_ah01_c', this.function_APS001, this);
        this.model.on('change:select_aps177_ah01_c', this.function_APS177, this);
      },

      render: function () {
        this._super("render", arguments);
        $('div[data-name="select_aps001_ah01_c"]').hide();
        $('div[data-name="select_aps177_ah01_c"]').hide();
        var ts = this.model.get('anml_animals_wpe_work_product_enrollment_1_name');
        if (ts) {
          this.function_TestSystem();
        }

        if (this.module == 'WPE_Work_Product_Enrollment' && this.context.parent && this.context.parent.get('module') == 'M03_Work_Product') {
          if (this.model.get('m03_work_product_wpe_work_product_enrollment_1_name') != "") {
            var self = this;
            var wpid = this.model.get('m03_work_p7d13product_ida');
            app.api.call("GET", app.api.buildURL("M03_Work_Product/" + wpid + "/get_blanket_product"), null, {
              success: function (data) {
                self.model.set('m03_work_p9f23product_ida', data.workID);
                self.model.set('m03_work_product_wpe_work_product_enrollment_2_name', data.blanket_protocol_c);
              },
              error: function (err) {
                console.log(err);
              }
            });
          }
        }
      },

      function_APS001: function () {
        if (this.model.get('select_aps001_ah01_c')) {
          this.model.set('m03_work_p7d13product_ida', "4a529aae-5165-3149-6db7-5702c7b98a5e");
          this.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "APS001-AH01");
        } else {
          this.model.set('m03_work_p7d13product_ida', "");
          this.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "");
        }
      },
      function_APS177: function () {
        if (this.model.get('select_aps177_ah01_c')) {
          this.model.set('m03_work_p7d13product_ida', "99ac34ca-e074-c580-0c73-5702c7e2f7c0");
          this.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "APS177-AH15");
        } else {
          this.model.set('m03_work_p7d13product_ida', "");
          this.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "");
        }
      },

      function_TestSystem: function () {
        this.model.set('m03_work_p7d13product_ida', "");
        this.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "");
        this.model.set('m03_work_p9f23product_ida', "");
        this.model.set('m03_work_product_wpe_work_product_enrollment_2_name', "");
        if (this.model.get('anml_animals_wpe_work_product_enrollment_1_name') != "") {
          var tsid = this.model.get('anml_animals_wpe_work_product_enrollment_1anml_animals_ida');
          var self = this;
          App.api.call("get", "rest/v10/ANML_Animals/" + tsid + "?fields=assigned_to_wp_c,termination_date_c,allocated_work_product_c", null, {
            success: function (Data) {
              if (Data.assigned_to_wp_c == "APS001-AH01" && Data.allocated_work_product_c != "") {
                //alert("Work Product has already Allocated for this Test System!");
                app.alert.show("NotAlloted_WP", {
                  level: "error",
                  messages: "Test System's Allocated Work Product is not null. Please clear the Allocated Work Product before creating a WPA."
                });

                self.model.set('anml_animals_wpe_work_product_enrollment_1anml_animals_ida', "");
                self.model.set('anml_animals_wpe_work_product_enrollment_1_name', "");
              }
              else if (Data.termination_date_c != "") {
                //alert("Termination Date for that Test System is filled out, Please select other Test System");
                app.alert.show("TerminationDate_TS", {
                  level: "error",
                  messages: "Test System is deceased (Termination Date is not null). WPA cannot be created."
                });
                self.model.set('anml_animals_wpe_work_product_enrollment_1anml_animals_ida', "");
                self.model.set('anml_animals_wpe_work_product_enrollment_1_name', "");
              }
            }
          });

          var APIURL1 = "rest/v10/ANML_Animals/" + tsid + "/link/anml_animals_wpe_work_product_enrollment_1?erased_fields=true&max_num=1&order_by=date_entered:desc";

          App.api.call("get", APIURL1, null, {
            success: function (Data) {
              $.each(Data.records, function (key, item) {
                var lastwpName = item.m03_work_product_wpe_work_product_enrollment_1_name;
                var lastwp_id = item.m03_work_p7d13product_ida;
                var lastStatus = item.enrollment_status_c;
                var WP_SponsorAPI = "rest/v10/M03_Work_Product/" + lastwp_id + "?fields=accounts_m03_work_product_1_name,work_product_code_2_c&max_num=1&order_by=date_entered:desc";

                App.api.call("get", WP_SponsorAPI, null, {
                  success: function (Data1) {
                    var sponsorname = Data1.accounts_m03_work_product_1.name;
                    var wpCode = Data1.work_product_code_2_c;
                    //alert(wpCode);
                    if ((lastwpName != "APS001-AH01" && lastStatus == 'On Study Transferred') && (wpCode == 'ST01' || wpCode == 'ST01a' || wpCode == 'ST02' || wpCode == 'ST09')) {
                      $('div[data-name="select_aps177_ah01_c"]').show();
                      $('div[data-name="select_aps001_ah01_c"]').hide();
                      //alert('show aps177');

                    }
                    else if ((lastwpName != "APS001-AH01" && lastStatus == 'On Study Transferred') && (wpCode != 'ST01' || wpCode != 'ST01a' || wpCode != 'ST02' || wpCode != 'ST09')) {
                      $('div[data-name="select_aps001_ah01_c"]').show();
                      $('div[data-name="select_aps177_ah01_c"]').hide();
                      //alert('show aps001');
                    }
                    else {
                      $('div[data-name="select_aps177_ah01_c"]').hide();
                      $('div[data-name="select_aps001_ah01_c"]').hide();
                      //alert('none');
                    }
                    self.model.set('testsystem_hidden_c', lastwpName + "$$" + lastStatus + "$$" + sponsorname + "$$" + wpCode);

                  }
                });
              });
            }
          });

        }
      },
      /*17 Nov 2020 : By Gurpreet : for last wp and wpe status condition */
      function_WP: function () {
        if (this.model.get('m03_work_product_wpe_work_product_enrollment_1_name') != "") {
          var self = this;
          var wpid = this.model.get('m03_work_p7d13product_ida');
          var tsid = self.model.get('anml_animals_wpe_work_product_enrollment_1anml_animals_ida');
          if (self.model.get('m03_work_product_wpe_work_product_enrollment_1_name') != null) {
            var wpid = self.model.get('m03_work_p7d13product_ida');
            var tsid = self.model.get('anml_animals_wpe_work_product_enrollment_1anml_animals_ida');

            App.api.call("get", "rest/v10/M03_Work_Product/" + wpid + "?fields=usda_category_multiselect_c", null, {
              success: function (Data) {

                if (Data.usda_category_multiselect_c != "") {
                  var ch1 = '';
                  var catArr = Data.usda_category_multiselect_c;
                  catArr.sort();
                  ch = catArr[catArr.length - 1];
                  res = ch.replace("^", "");
                  ch1 = res.replace("^", "");
                  self.model.set('usda_category_c', ch1);
                } else {
                  self.model.set('usda_category_c', "B");
                }
              }
            });
            app.api.call("GET", app.api.buildURL("M03_Work_Product/" + wpid + "/get_blanket_product"), {
            }, {
              success: function (data) {

                console.log(JSON.stringify(data));

                self.model.set('m03_work_p9f23product_ida', data.workID);
                self.model.set('m03_work_product_wpe_work_product_enrollment_2_name', data.blanket_protocol_c);
                // return;
              },
              error: function (err) {
                console.log(err);
                return;
              }
            });
          }
          if (tsid != undefined) {
            App.api.call("get", "rest/v10/ANML_Animals/" + tsid + "/link/anml_animals_wpe_work_product_enrollment_1?erased_fields=true&max_num=1&order_by=date_entered:desc", null, {
              success: function (Data) {
                var lastwp = Data.records[0].m03_work_product_wpe_work_product_enrollment_1_name;
                var lastStatus = Data.records[0].enrollment_status_c;
                //var lastwp_id = self.model.get('m03_work_p7d13product_ida');
                var lastwp_id = Data.records[0].m03_work_p7d13product_ida;
                var WP_Code_API = "rest/v10/M03_Work_Product/" + lastwp_id + "?fields=accounts_m03_work_product_1_name,work_product_code_2_c&max_num=1&order_by=date_entered:desc";
                App.api.call("get", WP_Code_API, null, {
                  success: function (Data) {
                    var WPCodeVal = Data.work_product_code_2_c;
                    if ((lastwp != "APS001-AH01" && lastwp != "APS177-AH15" && lastStatus == 'On Study') && (WPCodeVal != 'ST01' && WPCodeVal != 'ST01a' && WPCodeVal != 'ST02' && WPCodeVal != 'ST09')) {
                      app.alert.show("Onstudy_WP", {
                        level: "error",
                        messages: "Current Work Product Assignment is not APS001-AH01 and the Status is On Study. Please review existing WPAs prior to creating this record."
                      });
                      self.model.set('m03_work_p7d13product_ida', "");
                      self.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "");
                    }
                  }
                });


              }
            });

            if (wpid != null) {
              /* 947 Prakash Jangir  Start*/
              app.api.call("GET", app.api.buildURL("M03_Work_Product/" + wpid + "/" + tsid + "/get_test_system_design"), {
              }, {
                success: function (data) {
                  console.log(JSON.stringify(data));
                  if (data == 0) {
                    allowCreation = 0;
                    self.model.set('m03_work_p7d13product_ida', null);
                    self.model.set('m03_work_product_wpe_work_product_enrollment_1_name', null);

                    //alert("Work Product has already Allocated for this Test System!");
                    app.alert.show("NotMatched_TSD", {
                      level: "error",
                      messages: "Sex of chosen Animal(s) not approved per Work Product's Test System Design(s).  Please review."
                    });
                    self.model.set('m03_work_p9f23product_ida', null);
                    self.model.set('m03_work_product_wpe_work_product_enrollment_2_name', null);
                  } else {
                    allowCreation = 1;

                  }
                },
                error: function (err) {
                  allowCreation = 1;
                  console.log(err);
                  return;
                }
              });
              /* 947 Prakash Jangir  End */
            }
          }
        }
      },
      /*EOC : 17 Nov 2020 : By Gurpreet : for last wp and wpe status condition */
      saveAndClose: function () {
        console.log("Triggered")
        var errorTypeArr = []
        var errorCategoryArr = []
        var ts_Id = this.model.get("anml_animals_wpe_work_product_enrollment_1anml_animals_ida")
        var self = this;
        App.api.call("get", "rest/v10/ANML_Animals/" + ts_Id + "/link/m06_error_anml_animals_1", null, {
          success: function (Data) {
            console.log('com Record', Data);
            if (Data.records.length > 0) {
              $.each(Data.records, function (key, item) {
                console.log('com Record item ', item);
                errorTypeArr.push(item.error_type_c);
                errorCategoryArr.push(item.error_category_c);

              });

              var errorType = (errorTypeArr.indexOf("Rejected") > -1);
              var errorCategory = (errorCategoryArr.indexOf("Rejected Animal") > -1);
              console.log('errorType', errorType);
              console.log('errorCategory', errorCategory);

              if (errorType == true || errorCategory == true) {
                console.log('con match');
                app.alert.show('Comm_record_check_pop_up', {
                  level: 'confirmation',
                  messages: 'This animal was previously rejected.  Are you sure you want to assign? <br><b>' + self.model.get("anml_animals_wpe_work_product_enrollment_1_name") + '</b>',
                  autoClose: false,
                  confirm: {
                    label: "Yes"
                  },
                  cancel: {
                    label: "No"
                  },
                  onCancel: function () {
                    return;
                  },
                  onConfirm: function () {
                    self.saveWPA();
                  }
                });
              } else {
                console.log('con not match');
                self.saveWPA();
              }
            } else {
              self.saveWPA();
            }
          }
        });
      },

      saveWPA: function () {
        var tsid = this.model.get('anml_animals_wpe_work_product_enrollment_1anml_animals_ida');
        if (this.model.get('anml_animals_wpe_work_product_enrollment_1_name') != "") {

          var tsid = this.model.get('anml_animals_wpe_work_product_enrollment_1anml_animals_ida');
          var self = this;
          App.api.call("get", "rest/v10/ANML_Animals/" + tsid + "?fields=assigned_to_wp_c,termination_date_c,allocated_work_product_c", null, {
            success: function (Data) {
              if (Data.assigned_to_wp_c == "APS001-AH01" && Data.allocated_work_product_c != "") {
                app.alert.show("NotAlloted_WP", {
                  level: "error",
                  messages: "Test System's Allocated Work Product is not null. Please clear the Allocated Work Product before creating a WPA."
                });

              }
              else if (Data.termination_date_c != "") {
                app.alert.show("NotAlloted_WP", {
                  level: "error",
                  messages: "Test System is deceased (Termination Date is not null). WPA cannot be created."
                });

              } else if (self.model.get('m03_work_product_wpe_work_product_enrollment_1_name') != "" && typeof allowCreation !== 'undefined' && allowCreation == 0) {
                app.alert.show("NotMatched_TSD", {
                  level: "error",
                  messages: "Sex of chosen Animal(s) not approved per Work Product's Test System Design(s).  Please review."
                });
              }
              else {
                self.initiateSave(_.bind(function initSave() {
                  if (self.closestComponent("drawer")) {
                    app.drawer.close(self.context, self.model);
                  }
                }, self));
              }
            }
          });
        }
      },

    });
  });
})(SUGAR.App);