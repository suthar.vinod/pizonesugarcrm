/**
 * Extend the create view in order to pupolate the name on create
 */

;
(function (app) {
    app.events.on("app:sync:complete", function () {
        if (!app.view.views.BaseM03_Work_Product_DeliverableCreateView && !app.view.views.BaseM03_Work_Product_DeliverableCustomCreateView) {
            app.view.declareComponent("view", "create", "M03_Work_Product_Deliverable", undefined, false, "base");
        }

        var createView = "BaseM03_Work_Product_DeliverableCreateView";

        if (app.view.views.BaseM03_Work_Product_DeliverableCustomCreateView) {
            createView = "BaseM03_Work_Product_DeliverableCustomCreateView";
        }

        if (App.view.views[createView].createExtended === true) {
            return;
        }

        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function () {
                this._super("initialize", arguments);
                this.model.on('change:deliverable_c change:m03_work_product_m03_work_product_deliverable_1_name', this.function_work_product_deliverable, this);
            },


            render: function () {
                this._super("render", arguments);

                var self = this;
                var work_product_id = this.model.get('m03_work_p0b66product_ida');
                var deliverable_c = this.model.get('deliverable_c');
                if (work_product_id) {
                    var url = app.api.buildURL("getWPDDueDates");
                    var method = "create";
                    var data = {
                        work_product_id: work_product_id,
                        deliverable_c: deliverable_c
                    };
                    var callback = {
                        success: _.bind(function successCB(res) {
                            if (res != null) {
                                if ((res.final_due_date_c != null || res.final_due_date_c != '') && (res.due_date_c != null || res.due_date_c != '')) {
                                    this.model.set("final_due_date_c", res.final_due_date_c);
                                    this.model.set("due_date_c", res.due_date_c);
                                }
                            }
                        }, this)
                    };
                    app.api.call(method, url, data, callback);
                }
            },

            function_work_product_deliverable: function () {

                var self = this;
                var work_product_id = this.model.get('m03_work_p0b66product_ida');
                var deliverable_c = this.model.get('deliverable_c');
                if (work_product_id) {
                    var url = app.api.buildURL("getWPDDueDates");
                    var method = "create";
                    var data = {
                        work_product_id: work_product_id,
                        deliverable_c: deliverable_c
                    };
                    var callback = {
                        success: _.bind(function successCB(res) {
                            if (res != null) {
                                if ((res.final_due_date_c != null || res.final_due_date_c != '') && (res.due_date_c != null || res.due_date_c != '')) {
                                    this.model.set("final_due_date_c", res.final_due_date_c);
                                    this.model.set("due_date_c", res.due_date_c);
                                }
                            }
                        }, this)
                    };
                    app.api.call(method, url, data, callback);
                }
            },

        })
    });
})(SUGAR.App);