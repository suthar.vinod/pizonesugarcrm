/**
 * Handlebars helpers.
 *
 * These functions are to be used in handlebars templates.
 * @class Handlebars.helpers
 * @singletonsp
 */
(function handlebarHelper(app) {
    app.events.on("app:init", function handlebarHelpers() {

        Handlebars.registerHelper('ifeq', function (a, b, options) {
            if (a == b) { return options.fn(this); }
            return options.inverse(this);
        });
    });
})(SUGAR.App);