/**
 * Add the copy button on the Task design Subpanel in order to create multiple items by copyning.
 */
;
(function (app) {
    app.events.on("app:sync:complete", function () {
        if (!app.view.views.BaseTaskD_Task_DesignPanelTopView && !app.view.views.BaseTaskD_Task_DesignCustomPanelTopView) {
            app.view.declareComponent("view", "panel-top", "TaskD_Task_Design", undefined, false, "base");
        }

        var panelTopView = "BaseTaskD_Task_DesignPanelTopView";

        if (app.view.views.BaseTaskD_Task_DesignCustomPanelTopView) {
            panelTopView = "BaseTaskD_Task_DesignCustomPanelTopView";
        }

        if (App.view.views[panelTopView].copyButtonAdded === true) {
            return;
        }

        App.view.views[panelTopView] = App.view.views[panelTopView].extend({
            copyButtonAdded: true,

            /**
             * Override the initialize function in order to add the copy button at the top of the subpanel.
             * @param {Object} options 
             */
            initialize: function (options) {
                this._super("initialize", arguments);
                var parent_module = this.context.parent.get('module');
                //alert('parent_module '+parent_module);
                if (parent_module == 'M03_Work_Product') {
                    var copyButton = {
                        name: "copy_all_wp",
                        type: "rowaction",
                        label: "Copy All to WP",
                        events: {
                            //click: "subpanellist:copy:fire"
                            click: "button:copy_all_wp:click"
                        }
                    };

                    /**
                     * Making sure the necessary structure for pushing our button, exists.
                     * Might need improvement.
                     */
                    if (this.meta.buttons[0] && typeof this.meta.buttons[0].buttons !== "undefined" &&
                        typeof this.meta.buttons[0].buttons !== "undefined" && typeof this.meta.buttons[0].buttons.push == "function") {

                        if (!this.copyButtonExists(this.meta.buttons[0].buttons) === true) {
                            this.meta.buttons[0].buttons.push(copyButton);
                        }
                        this.context.on('button:copy_all_wp:click', this._openWPModal, this);
                    }

                }

            },

            /**
             * Checks wheter the copy button already exists in the dropdown
             * @param {array} buttons 
             */
            copyButtonExists: function (buttons) {
                for (var index in buttons) {
                    var button = buttons[index];
                    if (button.name === "copy_all_wp") {
                        return true;
                    }
                }

                return false;
            },
            /**Function to open the note create pop-up*/
            _openWPModal: function () {
                /**add class content-overflow-visible if client has touch feature*/
                if (Modernizr.touch) {
                    app.$contentEl.addClass('content-overflow-visible');
                }
                /**check whether the view already exists in the layout.
                 * If not we will create a new view and will add to the components list of the record layout
                 * */
                var quickCreateView = this.layout.getComponent('quick-create-wp');
                if (!quickCreateView) {
                    var parent_model = this.context.parent.get('model');
                    var ParentID = parent_model.get('id');
                    /** Prepare the context object for the new quick create view*/
                    var context = this.context.getChildContext({
                        module: 'TaskD_Task_Design',
                        forceNew: true,
                        create: true,
                        link: 'm03_work_product_taskd_task_design_1', //relationship name 
                        WPId: ParentID, // to paas the current id on create view
                    });
                    context.prepare();
                    /** Create a new view object */
                    quickCreateView = app.view.createView({
                        context: context,
                        name: 'quick-create-wp',
                        layout: this.layout,
                        module: context.module,
                    });
                    /** add the new view to the components list of the record layout*/
                    this.layout._components.push(quickCreateView);
                    this.layout.$el.append(quickCreateView.$el);
                }
                /**triggers an event to show the pop up quick create view*/
                this.layout.trigger("app:view:quick-create-wp");
            },
        });
    });
})(SUGAR.App);