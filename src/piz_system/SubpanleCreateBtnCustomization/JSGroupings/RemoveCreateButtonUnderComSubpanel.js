(function appFunction(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseBID_Batch_IDPanelTopView &&
            !app.view.views.BaseBID_Batch_IDCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "BID_Batch_ID", undefined, false, "base");
        }

        var BIPanelTop = "BaseBID_Batch_IDPanelTopView";

        if (app.view.views.BaseBID_Batch_IDCustomPanelTopView) {
            BIPanelTop = "BaseBID_Batch_IDCustomPanelTopView";
        }

        if (App.view.views[BIPanelTop].BIRmoveCreateButton === true) {
            return;
        }

        App.view.views[BIPanelTop] = App.view.views[BIPanelTop].extend({
            BIRmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "M06_Error") {
                    this._removeTDSubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeTDSubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);