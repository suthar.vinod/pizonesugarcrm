(function appFunction(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseII_Inventory_ItemPanelTopView &&
            !app.view.views.BaseII_Inventory_ItemCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "II_Inventory_Item", undefined, false, "base");
        }

        var TDPanelTop = "BaseII_Inventory_ItemPanelTopView";

        if (app.view.views.BaseII_Inventory_ItemCustomPanelTopView) {
            TDPanelTop = "BaseII_Inventory_ItemCustomPanelTopView";
        }

        if (App.view.views[TDPanelTop].TDRmoveCreateButton === true) {
            return;
        }

        App.view.views[TDPanelTop] = App.view.views[TDPanelTop].extend({
            TDRmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "M03_Work_Product_Deliverable") {
                    this._removeTDSubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeTDSubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);