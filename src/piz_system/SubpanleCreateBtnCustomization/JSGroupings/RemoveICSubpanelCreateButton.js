(function appFunction(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseIC_Inventory_CollectionPanelTopView &&
            !app.view.views.BaseIC_Inventory_CollectionCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "IC_Inventory_Collection", undefined, false, "base");
        }

        var TDPanelTop = "BaseIC_Inventory_CollectionPanelTopView";

        if (app.view.views.BaseIC_Inventory_CollectionCustomPanelTopView) {
            TDPanelTop = "BaseIC_Inventory_CollectionCustomPanelTopView";
        }

        if (App.view.views[TDPanelTop].TDRmoveCreateButton === true) {
            return;
        }

        App.view.views[TDPanelTop] = App.view.views[TDPanelTop].extend({
            TDRmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "M03_Work_Product_Deliverable") {
                    this._removeTDSubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeTDSubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);