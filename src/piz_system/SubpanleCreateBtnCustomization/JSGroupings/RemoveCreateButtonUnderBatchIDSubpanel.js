(function appFunction(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseM03_Work_ProductPanelTopView &&
            !app.view.views.BaseM03_Work_ProductCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "M03_Work_Product", undefined, false, "base");
        }

        var WPPanelTop = "BaseM03_Work_ProductPanelTopView";

        if (app.view.views.BaseM03_Work_ProductCustomPanelTopView) {
            WPPanelTop = "BaseM03_Work_ProductCustomPanelTopView";
        }

        if (App.view.views[WPPanelTop].WPRmoveCreateButton === true) {
            return;
        }

        App.view.views[WPPanelTop] = App.view.views[WPPanelTop].extend({
            WPRmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "BID_Batch_ID") {
                    this._removeTDSubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeTDSubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });

    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseTaskD_Task_DesignPanelTopView &&
            !app.view.views.BaseTaskD_Task_DesignCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "TaskD_Task_Design", undefined, false, "base");
        }

        var TDPanelTop = "BaseTaskD_Task_DesignPanelTopView";

        if (app.view.views.BaseTaskD_Task_DesignCustomPanelTopView) {
            TDPanelTop = "BaseTaskD_Task_DesignCustomPanelTopView";
        }

        if (App.view.views[TDPanelTop].TDRmoveCreateButton === true) {
            return;
        }

        App.view.views[TDPanelTop] = App.view.views[TDPanelTop].extend({
            TDRmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "BID_Batch_ID") {
                    this._removeTDSubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeTDSubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });

    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseWPE_Work_Product_EnrollmentPanelTopView &&
            !app.view.views.BaseWPE_Work_Product_EnrollmentCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "WPE_Work_Product_Enrollment", undefined, false, "base");
        }

        var WPAPanelTop = "BaseWPE_Work_Product_EnrollmentPanelTopView";

        if (app.view.views.BaseWPE_Work_Product_EnrollmentCustomPanelTopView) {
            WPAPanelTop = "BaseWPE_Work_Product_EnrollmentCustomPanelTopView";
        }

        if (App.view.views[WPAPanelTop].WPARmoveCreateButton === true) {
            return;
        }

        App.view.views[WPAPanelTop] = App.view.views[WPAPanelTop].extend({
            WPARmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "BID_Batch_ID") {
                    this._removeTDSubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeTDSubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });

    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseM06_ErrorPanelTopView &&
            !app.view.views.BaseM06_ErrorCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "M06_Error", undefined, false, "base");
        }

        var ErrorPanelTop = "BaseM06_ErrorPanelTopView";

        if (app.view.views.BaseM06_ErrorCustomPanelTopView) {
            ErrorPanelTop = "BaseM06_ErrorCustomPanelTopView";
        }

        if (App.view.views[ErrorPanelTop].ErrorRmoveCreateButton === true) {
            return;
        }

        App.view.views[ErrorPanelTop] = App.view.views[ErrorPanelTop].extend({
            ErrorRmoveCreateButton: true,

            render: function () {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "BID_Batch_ID") {
                    this._removeTDSubpanelCreateButton();
                }

                return initializeOptions;
            },

            _removeTDSubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);