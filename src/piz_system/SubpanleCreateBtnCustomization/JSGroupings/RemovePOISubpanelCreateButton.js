(function appFunction(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BasePOI_Purchase_Order_ItemPanelTopView &&
            !app.view.views.BasePOI_Purchase_Order_ItemCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "POI_Purchase_Order_Item", undefined, false, "base");
        }

        var POIPanelTop = "BasePOI_Purchase_Order_ItemPanelTopView";

        if (app.view.views.BasePOI_Purchase_Order_ItemCustomPanelTopView) {
            POIPanelTop = "BasePOI_Purchase_Order_ItemCustomPanelTopView";
        }

        if (App.view.views[POIPanelTop].POIRmoveCreateButton === true) {
            return;
        }
        var status = "";
        App.view.views[POIPanelTop] = App.view.views[POIPanelTop].extend({
            POIRmoveCreateButton: true,
            render: function () {
                var initializeOptions = this._super("render", arguments);
                if (this.parentModule === "Prod_Product") {
                    var modelId = this.context.parent.attributes.modelId;
                    App.api.call("get", "rest/v10/Prod_Product/" + modelId, null, {
                        success: function (Data) {
                            status = Data.status_c;
                        }
                    });
                    setTimeout(() => {
                        this._removePOISubpanelCreateButton();
                    }, 1500);
                }
                return initializeOptions;
            },
            _removePOISubpanelCreateButton: function () {
                if ((status != undefined || status != "") && status == "Out of Production") {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);