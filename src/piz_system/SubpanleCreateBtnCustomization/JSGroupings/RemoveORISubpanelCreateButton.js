(function appFunction(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseORI_Order_Request_ItemPanelTopView &&
            !app.view.views.BaseORI_Order_Request_ItemCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "ORI_Order_Request_Item", undefined, false, "base");
        }

        var ORIPanelTop = "BaseORI_Order_Request_ItemPanelTopView";

        if (app.view.views.BaseORI_Order_Request_ItemCustomPanelTopView) {
            ORIPanelTop = "BaseORI_Order_Request_ItemCustomPanelTopView";
        }

        if (App.view.views[ORIPanelTop].ORIRmoveCreateButton === true) {
            return;
        }
        var status = "";
        App.view.views[ORIPanelTop] = App.view.views[ORIPanelTop].extend({
            ORIRmoveCreateButton: true,
            render: function () {
                var initializeOptions = this._super("render", arguments);
                if (this.parentModule === "Prod_Product") {
                    var modelId = this.context.parent.attributes.modelId;
                    App.api.call("get", "rest/v10/Prod_Product/" + modelId, null, {
                        success: function (Data) {
                            status = Data.status_c;
                        }
                    });
                    setTimeout(() => {
                        this._removeORISubpanelCreateButton();
                    }, 1030);
                }
                return initializeOptions;
            },
            _removeORISubpanelCreateButton: function () {
                if (this.$el.find("[name=create_button]").length > 0) {
                    if (status == "Out of Production") {
                        this.$el.find("[name=create_button]").remove();
                    }
                }
            }
        });
    });
})(SUGAR.App);