/**
 * Extend the create view in order to pupolate the name on create
 */

;
(function (app) {
    app.events.on("app:sync:complete", function () {
        if (!app.view.views.BaseW_WeightCreateView && !app.view.views.BaseW_WeightCustomCreateView) {
            app.view.declareComponent("view", "create", "W_Weight", undefined, false, "base");
        }

        var createView = "BaseW_WeightCreateView";

        if (app.view.views.BaseW_WeightCustomCreateView) {
            createView = "BaseW_WeightCustomCreateView";
        }

        if (App.view.views[createView].createExtended === true) {
            return;
        }

        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function () {
                this._super("initialize", arguments);
                this.model.on("change:anml_animals_w_weight_1_name", this.get_study_id, this);
            },


            /*1387 Get WP id from linked latest WPA*/

            render: function () {
                this._super("render", arguments);

                if (this.module == 'W_Weight' && this.context.parent && this.context.parent.get('module') == 'ANML_Animals') {
                    var anml_animals_w_weight_1anml_animals_ida = this.model.get('anml_animals_w_weight_1anml_animals_ida');
                    var module_Id = this.model.get('id');

                    if (anml_animals_w_weight_1anml_animals_ida != '') {
                        //console.log('1');
                        var url = app.api.buildURL("getStudyId");
                        var method = "create";
                        var data = {
                            module: 'W_Weight',
                            moduleId: module_Id,
                            animalId: anml_animals_w_weight_1anml_animals_ida,
                        };

                        var callback = {
                            success: _.bind(function successCB(res) {
                                //console.log('data', res);
                                this.model.set('study_id', res);
                            }, this)
                        };
                        app.api.call(method, url, data, callback);

                    } else {
                        this.model.set('study_id', '');
                    }
                }
            },

        })
    });
})(SUGAR.App);