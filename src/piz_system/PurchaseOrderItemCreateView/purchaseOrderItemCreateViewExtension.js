/**
 * Extend the create view in order to pupolate the name on create
 */

;
(function (app) {
    app.events.on("app:sync:complete", function () {
        if (!app.view.views.BasePOI_Purchase_Order_ItemCreateView && !app.view.views.BasePOI_Purchase_Order_ItemCustomCreateView) {
            app.view.declareComponent("view", "create", "POI_Purchase_Order_Item", undefined, false, "base");
        }

        var createView = "BasePOI_Purchase_Order_ItemCreateView";

        if (app.view.views.BasePOI_Purchase_Order_ItemCustomCreateView) {
            createView = "BasePOI_Purchase_Order_ItemCustomCreateView";
        }

        if (App.view.views[createView].createExtended === true) {
            return;
        }

        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function () {
                this._super("initialize", arguments);
            },
            render: function () {
                this._super("render", arguments);

                if (this.module == 'POI_Purchase_Order_Item' && this.context.parent && this.context.parent.get('module') == 'PO_Purchase_Order') {
                    var PO_ID = this.model.get('po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida');
                    console.log(PO_ID);
                    if (PO_ID != "" && PO_ID != null) {
                        var self = this;
                        App.api.call("get", "rest/v10/PO_Purchase_Order/" + PO_ID + "?fields=estimated_arrival_date,order_date,status_c", null, {
                            success: function (Data) {
                                //self.model.set('estimated_arrival_date', Data.estimated_arrival_date);
                                //self.model.set('order_date', Data.order_date);
                                //$('div[data-name="order_date"]').css('pointer-events', 'none');

                                setTimeout(function () {
                                    self.model.set('estimated_arrival_date', Data.estimated_arrival_date);
                                    self.model.set('order_date', Data.order_date);
                                    self.model.set('status', Data.status_c);
                                    $('div[data-name="order_date"]').css('pointer-events', 'none');
                                    self.$el.find('.record-edit-link-wrapper[data-name=order_date]').hide();
                                }, 300);
                            }
                        });
                    }
                }
            },
        })
    });
})(SUGAR.App);