(function extendInitFunc(app) {
    app.events.on(
        "app:sync:complete",
        function extendOrderRequestRecordFunctionality() {
            if (
                !app.view.views.BaseOR_Order_RequestRecordView &&
                !app.view.views.BaseOR_Order_RequestCustomRecordView
            ) {
                app.view.declareComponent(
                    "view",
                    "record",
                    "OR_Order_Request",
                    undefined,
                    false,
                    "base"
                );
            }

            var baseORRecordView = "BaseOR_Order_RequestRecordView";

            if (app.view.views.BaseOR_Order_RequestCustomRecordView) {
                baseORRecordView = "BaseOR_Order_RequestCustomRecordView";
            }

            if (
                App.view.views[baseORRecordView]
                    .uBaseOrderRequestRecordViewOverride === true
            ) {
                return;
            }

            App.view.views[baseORRecordView] = App.view.views[
                baseORRecordView
            ].extend({
                uBaseOrderRequestRecordViewOverride: true,

                initialize: function (options) {
                    var initResult = this._super("initialize", arguments);

                    this.addCopyButton();

                    return initResult;
                },

                addCopyButton: function () {
                    var keepMeetingsHeaderPaneMeta = this.options.meta.buttons;
                    var copyORButton = {
                        name:   "copy_or_button",
                        type:   "button",
                        label:  "Copy Order",
                        events: {
                            click: "button:copyORButton:click"
                        },
                        primary: true,
                        showOn:  "view"
                    };

                    _.each(
                        keepMeetingsHeaderPaneMeta,
                        function addButton(action, index) {
                            if (index === 0) {
                                if (
                                    keepMeetingsHeaderPaneMeta[index].name !== "copy_or_button"
                                ){
                                    keepMeetingsHeaderPaneMeta.splice(
                                        index,
                                        0,
                                        copyORButton
                                    );
                                }
                            }
                        },
                        this
                    );

                    this.context.on("button:copyORButton:click", this.startCopyProcess, this);
                },               

                 
                copyOR_record: function (model) { console.log("In Copy Function ");
                    if (_.isEmpty(model) === false) {
                        var orderID = [];
                        //var orderID = this.model.get("id");
                        orderID.push(this.model.get('id'));
                         

                        app.api.call("create", "rest/v10/copy_order_request", {
                            recordId:  orderID, }, {
                            success: function (Data) {
                              console.log(Data);
                              if(Data==1){
                                app.alert.dismiss('inProgress-id');
                                  app.alert.show('or_copied', {
                                    level: 'success',
                                    messages: 'Order Request has been copied successfully!',
                                    autoClose : true
        
                                });
                              }
                            },
                            error: function (error) {
                                console.log(error);
                                app.alert.dismiss('inProgress-id');
                                app.alert.show('or_copied', {
                                    level: 'error',
                                    messages: error
                                });
                            }
                          });
				    }
                },                

                startCopyProcess: function () { 
                    var self = this;

                    app.alert.show('message-id', {
                        level: 'confirmation',
                        messages: 'Are you sure you want to copy this OR and the linked ORI record(s)?',
                        autoClose: false,
                        onConfirm: function () {
                            console.log("Start Process");

                            app.alert.show('inProgress-id', {
                                level: 'process',
                                title: 'In Process...' //change title to modify display from 'Loading...'
                            });
                            self.copyOR_record(self);
                        }, 
                        onCancel: function () { console.log("OK Thank you ");},   
                    });
                },
            });
        }
    );
})(SUGAR.App);

 