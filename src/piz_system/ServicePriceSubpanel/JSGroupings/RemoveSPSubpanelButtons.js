(function appFUnc(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseSP_Service_PricingPanelTopView &&
            !app.view.views.BaseSP_Service_PricingCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "SP_Service_Pricing", undefined, false, "base");
        }

        var iiPanelTop = "BaseSP_Service_PricingPanelTopView";

        if (app.view.views.BaseSP_Service_PricingCustomPanelTopView) {
            iiPanelTop = "BaseSP_Service_PricingCustomPanelTopView";
        }

        if (App.view.views[iiPanelTop].iiRmoveCreateButton === true) {
            return;
        }

        App.view.views[iiPanelTop] = App.view.views[iiPanelTop].extend({
            iiRmoveCreateButton: true,
            initialize: function () {
                this._super("initialize", arguments);
            },
            render: function() {
                console.log(this.model.get('id'));
                console.log(this.model.get('format'));


                var initializeOptions = this._super("render", arguments);
               

                //console.log(this.model.link.bean);
                console.log(this.model.link.bean.attributes);

                if (this.parentModule === "SP_Service_Pricing") {
					var self = this;
					setTimeout(function(){                        
                        if(self.model.link.bean.attributes.format=="Hourly" || self.model.link.bean.attributes.format=="Task" || self.model.link.bean.attributes.format=="Each")
                        {
                            self._removeSPSubpanelActionButtons();
                        }else if(self.model.link.bean.attributes.format=="Combination"){
							 self._removeSPSubpanelCreateActionButtons();
						}
                    }, 1000);
                }       

                return initializeOptions;
            },

            _removeSPSubpanelActionButtons: function () { 
                if (this.$el.find("[name=create_button]").length > 0 ){
                    this.$el.find("[name=create_button]").remove();
                }
                if (this.$el.find("[track='click:actiondropdown']").length > 0 ){
                    this.$el.find("[track='click:actiondropdown']").remove();
                }
                
            },
			
			_removeSPSubpanelCreateActionButtons: function () { 
                if (this.$el.find("[name=create_button]").length > 0 ){
                    this.$el.find("[name=create_button]").remove();
                } 
                
            }
        });
    });
})(SUGAR.App);