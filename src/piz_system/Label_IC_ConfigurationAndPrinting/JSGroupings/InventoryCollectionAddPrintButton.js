(function addPrintButtonToListView(app) {
    app.events.on("app:start", function appStartAddButton() {

        if (!app.view.views.BaseRecordlistView && !app.view.views.BaseCustomRecordlistView) {
            app.view.declareComponent("view", "recordlist", undefined, undefined, false, "base");
        }

        var recordlistView = "BaseRecordlistView";

        if (app.view.views.BaseCustomRecordlistView) {
            recordlistView = "BaseCustomRecordlistView";
        }

        if (App.view.views[recordlistView].newEventHandlersAdded === true) {
            return;
        }

        App.view.views[recordlistView] = App.view.views[recordlistView].extend({
            newEventHandlersAdded: true,
            initialize: function (options) {
                this._super("initialize", arguments);
                /*var initRes = this._super("initialize", arguments);

                if ((this.module === "IC_Inventory_Collection") && this.name === "recordlist") {
                    var printLabelButton = {
                        "name": "print_label_button",
                        "type": "button",
                        "label": "Print Labels",
                        "acl_action": "print_label_button",
                        "primary": "true",
                        "events": {
                            "click": "list:printLabelButton:fire",
                        },
                        // eslint-disable-next-line no-dupe-keys
                        "acl_action": "edit",
                    };

                    var printButton = this.meta.selection.actions;

                    printButton.push(printLabelButton);

                    this.meta.selection.actions = printButton;


                    this.context.on("list:printLabelButton:fire", this.startPrintProcess, this);
                }
                return initRes;*/
            },
            /*openTemplatesDrawer: function (validTemplates) {
                var listCollection = app.data.createBeanCollection("PdfManager");
                var oldFetch = listCollection.fetch;

                listCollection.fetch = function fetch(options) {
                    options.params = {};
                    options.params.labelsTemplatesNames = validTemplates;
                    options.params.labelsTemplateManager = true;

                    oldFetch.apply(listCollection, [options]);
                },
                        app.drawer.open({
                            layout: "selection-list",
                            context: {
                                module: "PdfManager",
                                printLabel: true,
                                collection: listCollection,
                                model: app.data.createBean("PdfManager")
                            }
                        }, this.generateAndPrintIC.bind(this));
            },
            generateAndPrintIC: function (model) {
                if (_.isEmpty(model) === false) {
                    var templateId = model.id;
                    templateId = '26522b06-41b3-11ec-9d5c-06ef2d010d1d';

                    var massCollection = app.controller.context.get("mass_collection");

                    var apiUrl = app.api.buildURL("generateLabelsIC", "create", requestParams, {});
                    var inventorycIds = [];

                    _.each(massCollection.models, function getCollectionData(model) {
                        inventorycIds.push({
                            inventoryCId: model.get("id"),
                        });
                    });

                    var requestParams = {
                        inventorycIds: inventorycIds,
                        templateId: '26522b06-41b3-11ec-9d5c-06ef2d010d1d', //templateId,
                    };

                    app.api.call("create", apiUrl, requestParams, null, {success: this.printLabelsIC.bind(this)});
                }
            },
            printLabelsIC: function (data) {
                if (_.isEmpty(data) === false) {
                    var htmlText = data["bodyHtml"];
                    var template = Handlebars.compile(htmlText);
                    var htmlBody = template(data.records);
                    var popupWindow = window.open();
                    var timeoutWindowValue = 40;

                    popupWindow.document.body.innerHTML += htmlBody;

                    setTimeout(function setTimeoutWindow() {
                        popupWindow.print();
                    }, timeoutWindowValue);

                    popupWindow.onafterprint = function printCompleted() {
                        popupWindow.close();
                    };
                }
            },
            startPrintProcess: function () {
                //this.getValidTemplates(this.openTemplatesDrawer.bind(this));
                this.generateAndPrintIC(this);
            },*/
        });
    });
})(SUGAR.App);
