(function extendInitFunc(app) {
    app.events.on(
        "app:sync:complete",
        function extendInventoryRecordFunctionality() {
            if (
                !app.view.views.BaseIC_Inventory_CollectionRecordView &&
                !app.view.views.BaseIC_Inventory_CollectionCustomRecordView
            ) {
                app.view.declareComponent(
                    "view",
                    "record",
                    "IC_Inventory_Collection",
                    undefined,
                    false,
                    "base"
                );
            }

            var baseInventoryRecordView = "BaseIC_Inventory_CollectionRecordView";

            if (app.view.views.BaseIC_Inventory_CollectionCustomRecordView) {
                baseInventoryRecordView = "BaseIC_Inventory_CollectionCustomRecordView";
            }

            if (
                App.view.views[baseInventoryRecordView]
                    .uBaseInventoryRecordViewOverride === true
            ) {
                return;
            }

            App.view.views[baseInventoryRecordView] = App.view.views[
                baseInventoryRecordView
            ].extend({
                uBaseInventoryRecordViewOverride: true,

                initialize: function (options) {
                    var initResult = this._super("initialize", arguments);

                    this.addPrintLabelButton();

                    return initResult;
                },

                addPrintLabelButton: function () {
                    var keepMeetingsHeaderPaneMeta = this.options.meta.buttons;
                    var printLabelsButton = {
                        name: "print_label_button",
                        type: "button",
                        label: "Print Labels",
                        events: {
                            click: "button:printLabelButton:click"
                        },
                        primary: true,
                        showOn: "view"
                    };

                    _.each(
                        keepMeetingsHeaderPaneMeta,
                        function addButton(action, index) {
                            if (index === 0) {
                                if (
                                    keepMeetingsHeaderPaneMeta[index].name !== "print_label_button"
                                ) {
                                    keepMeetingsHeaderPaneMeta.splice(
                                        index,
                                        0,
                                        printLabelsButton
                                    );
                                }
                            }
                        },
                        this
                    );

                    this.context.on("button:printLabelButton:click", this.startPrintProcess, this);
                },

                openTemplatesDrawer: function (validTemplates) {
                    var listCollection = app.data.createBeanCollection("PdfManager");
                    var oldFetch = listCollection.fetch;

                    listCollection.fetch = function fetch(options) {
                        options.params = {};
                        options.params.labelsTemplatesNames = validTemplates;
                        options.params.labelsTemplateManager = true;

                        oldFetch.apply(listCollection, [options]);
                    },

                        app.drawer.open({
                            layout: "selection-list",
                            context: {
                                module: "PdfManager",
                                printLabel: true,
                                collection: listCollection,
                                model: app.data.createBean("PdfManager")
                            }
                        }, this.generateAndPrintIC.bind(this));
                },
                generateAndPrintIC: function (model) {
                    console.log("In Generate Print label");
                    if (_.isEmpty(model) === false) {
                        var templateId = model.id;
                        templateId = '26522b06-41b3-11ec-9d5c-06ef2d010d1d';

                        var apiUrl = app.api.buildURL("generateLabelsIC", "create", requestParams, {});
                        var inventorycIds = [];

                        inventorycIds.push({
                            inventoryCId: this.model.get("id"),
                        });

                        var requestParams = {
                            inventorycIds: inventorycIds,
                            templateId: '26522b06-41b3-11ec-9d5c-06ef2d010d1d',//templateId,
                        };
                        app.api.call("create", apiUrl, requestParams, null, { success: this.printLabelsIC.bind(this) });
                    }
                },
                printLabelsIC: function (data) {
                    if (_.isEmpty(data) === false) {
                        var htmlText = data["bodyHtml"];
                        var template = Handlebars.compile(htmlText);
                        var htmlBody = template(data.records);
                        var popupWindow = window.open();
                        var timeoutWindowValue = 40;

                        popupWindow.document.body.innerHTML += htmlBody;

                        setTimeout(function setTimeoutWindow() {
                            popupWindow.print();
                        }, timeoutWindowValue);

                        popupWindow.onafterprint = function printCompleted() {
                            popupWindow.close();
                        };
                    }
                },

                startPrintProcess: function () {
                    console.log("Start Process");
                    //this.getValidTemplates(this.openTemplatesDrawer.bind(this));
                    this.generateAndPrintIC(this);
                },
            });
        }
    );
})(SUGAR.App);
