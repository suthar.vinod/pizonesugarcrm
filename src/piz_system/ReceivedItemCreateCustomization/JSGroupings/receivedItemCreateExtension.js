/**
 * Extend the create view in order to pupolate the name on create
 */
 flagRI = 0;
 (function (app) {
     flagRI = 0;
     $(document).on('click', '.closeCustomIconRI', function () {
         console.log('RI line 26');
         $("#alertsCustom").remove();
         flagRI = 1;
         console.log('closeCustomIconRI create IF ', flagRI);
         // selfRI.saveRIRecord();
         self._super('saveAndClose');
     });
     //console.log('laxRI123');
     app.events.on('app:sync:complete', function () {
         if (!app.view.views.BaseRI_Received_ItemsCreateView && !app.view.views.BaseRI_Received_ItemsCustomCreateView) {
             app.view.declareComponent('view', 'create', 'RI_Received_Items', undefined, false, 'base');
         }
 
         var createView = 'BaseRI_Received_ItemsCreateView';
 
         if (app.view.views.BaseRI_Received_ItemsCustomCreateView) {
             createView = 'BaseRI_Received_ItemsCustomCreateView';
         }
 
         if (App.view.views[createView].createExtended === true) {
             return;
         }
         App.view.views[createView] = App.view.views[createView].extend({
             createExtended: true,
             initialize: function () {
                 this._super('initialize', arguments);
                 this.on('render', _.bind(this.onload_function, this));
             },
             onload_function: function () {
                 var self = this;
                 var POIVal = this.model.get('poi_purcha665cer_item_ida');
                 var ORIVal = this.model.get('ori_order_6b32st_item_ida');
                 if (POIVal != null && POIVal != '') {
                    flagRI = 0;
                    console.log('onload_function create IF ', flagRI);
                 } else {
                    flagRI = 0;
                    console.log('onload_function create Else ', flagRI);
                 }
             },
             render: function () {
                 this._super('render', arguments);
                 var self = this;
                 setTimeout(function () {
                    //console.log('laxRI');
                    if (self.module == 'RI_Received_Items' && self.context.parent && self.context.parent.get('module') == 'POI_Purchase_Order_Item') {
                        self.model.set('type_2', 'POI');
                    } else if (self.module == 'RI_Received_Items' && self.context.parent && self.context.parent.get('module') == 'ORI_Order_Request_Item') {
                        self.model.set('type_2', 'ORI');
                    }
                 }, 300);
             },
 
             // saveRIRecord: function () {
             //     selfRI._super('saveAndClose');
             // },
             saveAndClose: function () {
                // console.log("Save and Close active");
                self = this;
                var POIVal = this.model.get('poi_purcha665cer_item_ida');
                var ORIVal = this.model.get('ori_order_6b32st_item_ida');

                if ((POIVal != undefined || ORIVal != undefined) && flagRI == 0 && POIVal != null && POIVal != '') {
                    App.api.call('get','rest/v10/POI_Purchase_Order_Item/' + POIVal +
                        '?fields=po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
                        null,
                        {
                            success: function (Data) {
                                var POI_PO = Data.po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida;
                                if (POI_PO != '') {
                                    App.api.call('get','rest/v10/PO_Purchase_Order/' + POI_PO + '?fields=equipment', null,
                                        {
                                            success: function (Data1) {
                                                var equipment = Data1.equipment;
                                                if (equipment == 'Required Not Received' || equipment == 'Required and Received') {
                                                    $('#sidecar').prepend(`
                                                        <div id="alertsCustom" class="alert-top">
                                                            <div class="alert-wrapper">
                                                                <div class="alert alert-warning alert-block rounded-md shadow-lg bg-alert-background block m-auto closeable">
                                                                    <div class="flex">
                                                                        <div class="indicator py-5 px-4">
                                                                            <i class="sicon sicon-32 sicon-warning-lg"></i>
                                                                        </div>
                                                                        <button class="close btn btn-link btn-invisible closeCustomIconRI" data-action="close"><i class="sicon sicon-close"></i></button>
                                                                        <span class="text py-5 pr-4">
                                                                            <strong>Warning: </strong>
                                                                            Review and Verify Equipment Form.
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    `);
                                                    flagRI = 1;
                                                } else {
                                                flagRI = 1;
                                                    self._super('saveAndClose');
                                                }
                                            }
                                        }
                                    );
                                } else {
                                    self._super('saveAndClose');
                                }
                            }
                        }
                    );
                } 
                else if((POIVal != undefined || ORIVal != undefined) && flagRI == 0 && ORIVal != null && ORIVal != '') {
                    App.api.call('get','rest/v10/ORI_Order_Request_Item/' + ORIVal +
                        '?fields=po_purchase_order_ori_order_request_item_1po_purchase_order_ida', null,
                        {
                            success: function (Data) {
                                var ORI_PO = Data.po_purchase_order_ori_order_request_item_1po_purchase_order_ida;
                                if (ORI_PO != '') {
                                    App.api.call('get','rest/v10/PO_Purchase_Order/' + ORI_PO + '?fields=equipment',null,
                                        {
                                            success: function (Data1) {
                                                var equipment = Data1.equipment;
                                                if (equipment == 'Required Not Received' || equipment == 'Required and Received') {
                                                    $('#sidecar').prepend(`
                                                        <div id="alertsCustom" class="alert-top">
                                                            <div class="alert-wrapper">
                                                                <div class="alert alert-warning alert-block rounded-md shadow-lg bg-alert-background block m-auto closeable">
                                                                    <div class="flex">
                                                                        <div class="indicator py-5 px-4">
                                                                            <i class="sicon sicon-32 sicon-warning-lg"></i>
                                                                        </div>
                                                                        <button class="close btn btn-link btn-invisible closeCustomIconRI" data-action="close"><i class="sicon sicon-close"></i></button>
                                                                        <span class="text py-5 pr-4">
                                                                            <strong>Warning: </strong>
                                                                            Review and Verify Equipment Form.
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    `);
                                                flagRI = 1;
                                                } else {
                                                flagRI = 1;
                                                self._super('saveAndClose');
                                                }
                                            }
                                        }
                                    );
                                } else {
                                    self._super('saveAndClose');
                                }
                            }
                        }
                    );
                }
                else {
                flagRI = 1;
                self._super('saveAndClose');
                }
             }
         });
     });
 })(SUGAR.App);
 