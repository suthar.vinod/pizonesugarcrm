(function appFUnc(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseWPE_Work_Product_EnrollmentPanelTopView &&
            !app.view.views.BaseWPE_Work_Product_EnrollmentCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "WPE_Work_Product_Enrollment", undefined, false, "base");
        }

        var iiPanelTop = "BaseWPE_Work_Product_EnrollmentPanelTopView";

        if (app.view.views.BaseWPE_Work_Product_EnrollmentCustomPanelTopView) {
            iiPanelTop = "BaseWPE_Work_Product_EnrollmentCustomPanelTopView";
        }

        if (App.view.views[iiPanelTop].iiRmoveCreateButton === true) {
            return;
        }

        App.view.views[iiPanelTop] = App.view.views[iiPanelTop].extend({
            iiRmoveCreateButton: true,
            initialize: function () {
                this._super("initialize", arguments);
            },
            render: function() {
                var initializeOptions = this._super("render", arguments);
                //console.log(this.model.link.bean.attributes);

                if (this.parentModule === "M03_Work_Product") { 
                    var self = this;
					setTimeout(function(){
                        var iacucClosureDate = self.model.link.bean.attributes.iacuc_closure_date_c;
                        var wpPhase          = self.model.link.bean.attributes.work_product_status_c;
                        var primaryAnimal    = self.model.link.bean.attributes.primary_animals_countdown_c;
                        if(iacucClosureDate!="" || wpPhase=="Archived" || primaryAnimal<="0")
                        {
                            self._removeWPASubpanelActionButtons();
                        }else{
                            self.$el.find("[track='click:actiondropdown']").removeClass("disabled");
                        }  
                    }, 1000);
                }  
                
                if (this.parentModule === "ANML_Animals" && 1==0) {  
                    var self = this;
                    //alert(this.parentModule);
                    setTimeout(function(){
                        self.WPA_terminationDate();
                        //console.log('self model',self.model);
                        var wpid = self.model.link.bean.attributes.assigned_to_wp_c;
                        //alert(termination_date_c);
                        APIURL = "rest/v10/M03_Work_Product?fields=iacuc_closure_date_c,work_product_status_c,primary_animals_countdown_c&max_num=1&filter[0][$and][0][$and][1][$or][0][name][$starts]=" + wpid;
                         
                        App.api.call("read", APIURL, null, {
                            success: function (Data) {
                                console.log(Data);
                                $.each(Data.records, function (key, item) { 
                                    var iacucClosureDate = item.iacuc_closure_date_c;
                                    var wpPhase          = item.work_product_status_c;
                                    var primaryAnimal    = item.primary_animals_countdown_c;

                                    if(iacucClosureDate!="" || wpPhase=="Archived" || primaryAnimal<="0")
                                    {
                                       
                                        self._removeWPASubpanelActionButtons();
                                    }else{
                                        self.$el.find("[track='click:actiondropdown']").removeClass("disabled");
                                    }  
                                }); 
                            }
                        });
                        



                    },1000);    
                }
                return initializeOptions;
            },

            _removeWPASubpanelActionButtons: function () {  
                if (this.$el.find("[name=create_button]").length > 0 ){  
                    this.$el.find("[name=create_button]").remove();
                }
                if (this.$el.find("[track='click:actiondropdown']").length > 0 ){  
                    this.$el.find("[track='click:actiondropdown']").remove();
                }
                
            }, 
            WPA_terminationDate: function(){
                //alert("test temination");
                /*25 Nov 2020: #577 : Hide WPA subpanel + edit button on termination date not null */
                //var testsys_id = self.model.link.bean.attributes.id;
                var termination_date_c = this.model.link.bean.attributes.termination_date_c;
                //console.log('termination_date_c',termination_date_c);
                if(termination_date_c!="")
                {
                   this._removeWPASubpanelActionButtons();
                }else{
                    this.$el.find("[track='click:actiondropdown']").removeClass("disabled");
                }  
               

            },
        });
    });
})(SUGAR.App);