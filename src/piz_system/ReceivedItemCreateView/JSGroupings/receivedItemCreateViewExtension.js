/**
 * Extend the create view in order to pupolate the name on create
 */
;
(function (app) {
     app.events.on("app:sync:complete", function () {
        if (!app.view.views.BaseRI_Received_ItemsCreateView && !app.view.views.BaseRI_Received_ItemsCustomRimasscreateView) {
            app.view.declareComponent("view", "create", "RI_Received_Items", undefined, false, "base");
        }

        var createView = "BaseRI_Received_ItemsCreateView";

        if (app.view.views.BaseRI_Received_ItemsCustomRimasscreateView) {
            createView = "BaseRI_Received_ItemsCustomRimasscreateView";
        }

        if (App.view.views[createView].createExtended === true) {
            return;
        }
 
        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function () {
                this._super("initialize", arguments);
            },
        
            render: function () {
                this._super("render", arguments);
                var self = this;
                setTimeout(function(){
                    $('[data-name="type_2"] input[name="type_2"]').prop('disabled',true);
                },300);
                
                if (this.module == 'RI_Received_Items' && this.context.parent && this.context.parent.get('module') == 'POI_Purchase_Order_Item') {
                    this.model.set('type_2','POI');
                }else if(this.module == 'RI_Received_Items' && this.context.parent && this.context.parent.get('module') == 'ORI_Order_Request_Item'){
                    this.model.set('type_2','ORI');
                }
            },
        })
    });
})(SUGAR.App);