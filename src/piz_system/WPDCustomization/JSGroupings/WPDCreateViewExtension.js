dateTimeChangedCV = false;
popupCount = 0;

(function(app) {
    popupAlert = 0;
    $(document).on('click', '.closeCustomWPDPopup', function() {
        self.saveWPDRecord();
    });
    app.events.on("app:sync:complete", function() {
        if (!app.view.views.BaseM03_Work_Product_DeliverableCreateView && !app.view.views.BaseM03_Work_Product_DeliverableCustomCreateView) {
            app.view.declareComponent("view", "create", "M03_Work_Product_Deliverable", undefined, false, "base");
        }

        var createView = "BaseM03_Work_Product_DeliverableCreateView";

        if (app.view.views.BaseM03_Work_Product_DeliverableCustomCreateView) {
            createView = "BaseM03_Work_Product_DeliverableCustomCreateView";
        }

        if (App.view.views[createView].createExtended === true) {
            return;
        }
        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function() {
                this._super("initialize", arguments);
                this.model.on('change:datetime_desired_c', this.functionDatetimeDesired, this);
                this.on('render', _.bind(this.onload_function, this));
            },
            onload_function: function() {
                var self = this;
                if (this.model.get("faxitron_required_c") != "Yes") {
                    popupAlert = 0;
                    console.log('onload_function Record ', popupAlert);
                }
                this._render();
            },
            render: function() {
                this._super("render", arguments);
            },
            functionDatetimeDesired: function() {
                dateTimeChangedCV = true;
                popupCount = 0;
            },

            saveAndClose: function() {
                self = this;
                if (self.model.get("faxitron_required_c") == "Yes" && popupAlert == 0) {
                    $("#sidecar").prepend(`    
                        <div id="alertsCustom" class="alert-top">
                        <div class="alert-wrapper">
                            <div class="alert alert-warning alert-block closeable">
                                <div class="indicator">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </div>
                                <button class="close btn btn-link btn-invisible closeCustomWPDPopup" data-action="close"><i class="fa fa-times"></i></button>
                                <span class="text">
                            <strong>Warning: </strong>
                            Deliverable must be assigned to Pathology Services for imaging prior to shipment.
                        </span>
                            </div>
                        </div>
                        </div>
                    `);
                } else {
                    $("#alertsCustom").remove();
                    self.saveWPDRecord();
                }
            },

            saveWPDRecord: function() {
                $("#alertsCustom").remove();
                popupCount++;
                popupAlert++;
                console.log(this.model.get('datetime_desired_c') != "" && this.model.get('datetime_desired_c') != null);
                /*#642 : 31 May 2021*/
                var formattedDesiredDate = this._fixDateFormatToLocale(this.model.get('datetime_desired_c'));
                var formattedCurrentDate = this._fixDateFormatToLocale(new Date());
                var nextDate = this._fixDateFormatToLocale(new Date(formattedCurrentDate).getTime() + (60 * 60 * 24 * 1000));
                var showPopUp = false;
                console.log(new Date(formattedDesiredDate).getTime());
                console.log(new Date(nextDate).getTime());
                if (new Date(formattedDesiredDate).getTime() < new Date(nextDate).getTime()) {
                    showPopUp = true;
                }

                var stability_considerations_c = this.model.get('stability_considerations_c');
                var analyze_by_date_c = this.model.get('analyze_by_date_c');
                var final_due_date_c = this.model.get('final_due_date_c');
                //alert(stability_considerations_c);
                //alert(analyze_by_date_c);
                if (stability_considerations_c == 'Yes' && analyze_by_date_c != '' && final_due_date_c != '' && analyze_by_date_c < final_due_date_c)
                //if(analyze_by_date_c < final_due_date_c )
                {
                    app.alert.show("NotAlloted_SaveWPD", {
                        level: "error",
                        messages: "The Analyze-by Date must not be before the External Final Due Date. Please review."
                    });

                } else if (stability_considerations_c != 'Yes') {
                    this.model.set('analyze_by_date_c', '');
                    // this._super('saveAndClose');
                }
                /*#642 : 31 May 2021*/
                var self = this;
                /*#642 : 31 May 2021*/
                if (this.model.get('deliverable_c') == 'SpecimenSample Disposition' && this.model.get('type_3_c') == 'Transfer' && showPopUp && (typeof dateTimeChangedCV !== 'undefined') && dateTimeChangedCV && this.model.get('datetime_desired_c') != "" && this.model.get('datetime_desired_c') != null && popupCount == 1) {

                    var self = this;
                    app.alert.show('message-id', {
                        level: 'confirmation',
                        messages: '24 hours notice is required for internal specimen transfers.  Continue with less than 24 hour notice?',
                        autoClose: false,
                        confirm: {
                            label: "Yes"
                        },
                        cancel: {
                            label: "No"
                        },
                        onCancel: function() {
                            return;
                        },
                        onConfirm: function() {
                            self._super('saveAndClose');
                            // self.initiateSave(_.bind(function initSave() {
                            //     app.navigate(self.context, self.model);
                            // }, self));
                        },

                    });
                } else if (this.model.get('deliverable_c') == 'SpecimenSample Disposition' && this.model.get("faxitron_required_c") == "Yes" && popupAlert == 1) {
                    //this._super('saveAndClose');
                    // console.log("faxitron_required_c: ");
                    // self.initiateSave(_.bind(function() {
                    //     app.navigate(self.context, self.model);
                    // }, self));
                } else {
                    this._super('saveAndClose');
                    console.log("Else condition : ");
                }
            },
            /*#642 : 31 May 2021*/
            _fixDateFormatToLocale: function(date) {
                var local = app.date.utc(date).toDate();
                var dateObj = app.date(local);
                // get date and time based on user preferences
                var fixedDate = dateObj.format(app.date.getUserDateFormat());
                var fixedTime = dateObj.format(app.date.getUserTimeFormat());

                return fixedDate + ' ' + fixedTime;
            },
        });
    });
})(SUGAR.App);