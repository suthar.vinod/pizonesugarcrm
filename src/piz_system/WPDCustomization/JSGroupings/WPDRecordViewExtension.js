dateTimeChangedRV = false;
apsdev = 1;
(function (app) {
    popupAlert = 0;
    $(document).on('click', '.closeCustomWPDPopup', function () {
        self.saveWPDRecord();
        //popupAlert = 1;
    });
    app.events.on("app:sync:complete", function () {
        if (!app.view.views.BaseM03_Work_Product_DeliverableRecordView && !app.view.views.BaseM03_Work_Product_DeliverableCustomRecordView) {
            app.view.declareComponent("view", "record", "M03_Work_Product_Deliverable", undefined, false, "base");
        }

        var recordView = "BaseM03_Work_Product_DeliverableRecordView";

        if (app.view.views.BaseM03_Work_Product_DeliverableCustomRecordView) {
            recordView = "BaseM03_Work_Product_DeliverableCustomRecordView";
        }

        if (App.view.views[recordView].recordExtended === true) {
            return;
        }
        App.view.views[recordView] = App.view.views[recordView].extend({
            recordExtended: true,
            initialize: function () {
                this._super("initialize", arguments);
                this._bindEvents();
            },
            _bindEvents: function () {
                this.context.on('button:edit_button:click', this.editClicked, this);
                this.model.once("sync", function () { this.model.on("change:datetime_desired_c", this.functionDatetimeDesired, this); }, this);
                //this.on('render', _.bind(this.onload_function, this));
                this.model.on('data:sync:complete', this.onload_function, this);
            },
            editClicked: function () {
                i = 0;
                dateTimeChangedRV = false;
                if (this.model.get("faxitron_required_c") == "Yes") {
                    popupAlert = 1;
                    //console.log('onload_function Record ', popupAlert);
                } else {
                    popupAlert = 0;
                }
                this._super('editClicked');
            },
            functionDatetimeDesired: function () {
                console.log(i);
                if (i > 0)
                    dateTimeChangedRV = true;
                i++;

            },
            onload_function: function () {
                var self = this;
                if (this.model.get("faxitron_required_c") == "Yes") {
                    popupAlert = 1;
                    //console.log('onload_function Records ', popupAlert);
                } else {
                    popupAlert = 0;
                    //console.log('Else onload_function Records ', popupAlert);
                }
                //this._render();
                this._super("render");

                var relative_2_c        = this.model.get('relative_2_c');
                var relative_template_c = this.model.get('relative_template_c');
                if (relative_2_c == 'To Other Work Product Deliverable') {
                    $('[data-name="related_deliverable_2_c"]').css("visibility", "visible"); 
                    $('[data-name="related_deliverable_2_c"]').css("height", "auto"); 

                } else {
                    $('[data-name="related_deliverable_2_c"]').css("visibility", "hidden"); 
                    $('[data-name="related_deliverable_2_c"]').css("height", "0"); 
                }
                if (relative_2_c == 'No') {
                    $('[data-name="interval_days_2_c"]').css("visibility", "hidden"); 
                    $('[data-name="interval_days_2_c"]').css("height", "0");
                } else {
                    $('[data-name="interval_days_2_c"]').css("visibility", "visible"); 
                    $('[data-name="interval_days_2_c"]').css("height", "auto");
                }

                var deliverable_c = this.model.get('deliverable_c');
                if (deliverable_c == 'SpecimenSample Disposition') {
                    $('[data-name="type_3_c"]').css("visibility", "visible"); 
                    $('[data-name="filename"]').css("visibility", "visible");
                    $('[data-name="type_3_c"]').css("height", "auto");
                    $('[data-name="filename"]').css("height", "auto");                                   
                } else {
                    $('[data-name="type_3_c"]').css("visibility", "hidden");
                    $('[data-name="filename"]').css("visibility", "hidden"); 
                    $('[data-name="type_3_c"]').css("height", "0");
                    $('[data-name="filename"]').css("height", "0"); 
                }
                if (deliverable_c == 'Slide Scanning') {
                    $('[data-name="slide_type_c"]').css("visibility", "visible"); 
                    $('[data-name="slide_type_c"]').css("height", "auto");                                                        
                } else {
                    $('[data-name="slide_type_c"]').css("visibility", "hidden");
                    $('[data-name="slide_type_c"]').css("height", "0"); 
                }
                if (deliverable_c == 'Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Gross Pathology Report' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
                    $('[data-name="pathologist_on_call_c"]').css("visibility", "visible");   
                    $('[data-name="pathologist_on_call_c"]').css("height", "auto");                                    
                } else {
                    $('[data-name="pathologist_on_call_c"]').css("visibility", "hidden"); 
                    $('[data-name="pathologist_on_call_c"]').css("height", "0");  
                }                
                if (deliverable_c == 'Gross Pathology Report' || deliverable_c == 'Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Gross Pathology Report' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
                    $('[data-name="planned_bd_to_draft_c"]').css("visibility", "visible");  
                    $('[data-name="actual_bc_to_complete_draft_c"]').css("visibility", "visible");  
                    $('[data-name="actual_bc_to_complete_draft_c"]').css("height", "auto");                                    
                } else {
                    $('[data-name="planned_bd_to_draft_c"]').css("visibility", "hidden"); 
                    $('[data-name="actual_bc_to_complete_draft_c"]').css("visibility", "hidden");
                    $('[data-name="actual_bc_to_complete_draft_c"]').css("height", "0"); 
                }
                if (deliverable_c == 'Final Report Amendment') {
                    $('[data-name="amendment_responsibility_c"]').css("visibility", "visible");   
                    $('[data-name="reason_for_amendment_c"]').css("visibility", "visible");
                    $('[data-name="amendment_responsibility_c"]').css("height", "auto"); 
                    $('[data-name="reason_for_amendment_c"]').css("height", "auto");                                    
                } else {
                    $('[data-name="amendment_responsibility_c"]').css("visibility", "hidden"); 
                    $('[data-name="reason_for_amendment_c"]').css("visibility", "hidden");
                    $('[data-name="amendment_responsibility_c"]').css("height", "0"); 
                    $('[data-name="reason_for_amendment_c"]').css("height", "0");  
                }
                if (deliverable_c != 'Animal Selection' && deliverable_c != 'Animal Selection Populate TSD' && deliverable_c != 'Animal Selection Procedure Use' && deliverable_c != 'Sample Prep Design') {
                    $('[data-name="due_date_c"]').css("visibility", "visible"); 
                    $('[data-name="due_date_c"]').css("height", "auto");   
                    $('[data-name="internal_final_due_date_c"]').css("visibility", "visible");  
                    $('[data-name="internal_final_due_date_c"]').css("height", "auto");                                     
                } else {
                    $('[data-name="due_date_c"]').css("visibility", "hidden"); 
                    $('[data-name="due_date_c"]').css("height", "0"); 
                    $('[data-name="internal_final_due_date_c"]').css("visibility", "hidden"); 
                    $('[data-name="internal_final_due_date_c"]').css("height", "0"); 
                }
                
                if (deliverable_c == 'Paraffin Slide Completion' || deliverable_c == 'EXAKT Slide Completion' || deliverable_c == 'Plastic Microtome Slide Completion' || deliverable_c == 'Frozen Slide Completion' || deliverable_c == 'Slide Scanning' || deliverable_c == 'Histopathology Report2' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
                    $('[data-name="standard_he_slides_c"]').css("visibility", "visible"); 
                    $('[data-name="standard_special_stain_c"]').css("visibility", "visible"); 
                    $('[data-name="large_he_slides_c"]').css("visibility", "visible");      
                    $('[data-name="large_special_stain_c"]').css("visibility", "visible");
                    
                    $('[data-name="standard_he_slides_c"]').css("height", "auto");
                    $('[data-name="standard_special_stain_c"]').css("height", "auto");
                    $('[data-name="large_he_slides_c"]').css("height", "auto");
                    $('[data-name="large_special_stain_c"]').css("height", "auto");

                } else {
                    $('[data-name="standard_he_slides_c"]').css("visibility", "hidden"); 
                    $('[data-name="standard_special_stain_c"]').css("visibility", "hidden"); 
                     $('[data-name="large_he_slides_c"]').css("visibility", "hidden"); 
                     $('[data-name="large_special_stain_c"]').css("visibility", "hidden"); 

                     $('[data-name="standard_he_slides_c"]').css("height", "0");
                     $('[data-name="standard_special_stain_c"]').css("height", "0");
                     $('[data-name="large_he_slides_c"]').css("height", "0");
                     $('[data-name="large_special_stain_c"]').css("height", "0");
                }
                if (deliverable_c == 'Paraffin Slide Completion' || deliverable_c == 'Plastic Microtome Slide Completion' || deliverable_c == 'Frozen Slide Completion') {
                    $('[data-name="standard_unstained_slides_c"]').css("visibility", "visible"); 
                    $('[data-name="standard_unstained_slides_c"]').css("height", "auto");
                  
                } else {
                    $('[data-name="standard_unstained_slides_c"]').css("visibility", "hidden"); 
                    $('[data-name="standard_unstained_slides_c"]').css("height", "0");
                }
                if (deliverable_c == 'Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
                    $('[data-name="minutes_per_slide_c"]').css("visibility", "visible"); 
                    $('[data-name="total_number_of_slides_c"]').css("visibility", "visible"); 
                    $('[data-name="report_writing_days_c"]').css("visibility", "visible"); 
                    $('[data-name="minutes_per_slide_c"]').css("height", "auto");
                    $('[data-name="total_number_of_slides_c"]').css("height", "auto");
                    $('[data-name="report_writing_days_c"]').css("height", "auto");
                } else {
                    $('[data-name="minutes_per_slide_c"]').css("visibility", "hidden"); 
                    $('[data-name="total_number_of_slides_c"]').css("visibility", "hidden"); 
                    $('[data-name="report_writing_days_c"]').css("visibility", "hidden"); 

                    $('[data-name="minutes_per_slide_c"]').css("height", "0");
                    $('[data-name="total_number_of_slides_c"]').css("height", "0");
                    $('[data-name="report_writing_days_c"]').css("height", "0");
                }
                if (deliverable_c == 'Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
                    $('[data-name="pathologist_participation_c"]').css("visibility", "visible");
                    $('[data-name="pathologist_participation_c"]').css("height", "auto");
                } else {
                    $('[data-name="pathologist_participation_c"]').css("visibility", "hidden");
                    $('[data-name="pathologist_participation_c"]').css("height", "0");        
                }
                if(deliverable_c == ''){
                    $('[data-name="deliverable_status_c"]').css("visibility", "hidden");
                    $('[data-name="deliverable_status_c"]').css("height", "0");
                } else if (deliverable_c == 'Sample Prep Design') {
                    $('[data-name="same_day_prep_c"]').css("visibility", "visible");   
                    $('[data-name="neat_preparation_c"]').css("visibility", "visible");
                    $('[data-name="hdpe_eto_c"]').css("visibility", "visible");   
                    $('[data-name="number_test_implants_c"]').css("visibility", "visible");
                    $('[data-name="number_control_implants_c"]').css("visibility", "visible");   
                    $('[data-name="sample_prep_time_estimate_c"]').css("visibility", "visible");
                    $('[data-name="additional_supplies_required_c"]').css("visibility", "visible");   
                    $('[data-name="sample_prep_design_notes_c"]').css("visibility", "visible"); 
                    
                    //$('[data-name="assigned_user_name"]').css("visibility", "visible"); 
                    //$('[data-name="m03_work_product_m03_work_product_deliverable_1_name"]').css("visibility", "visible");
                    $('[data-name="same_day_prep_c"]').css("height", "auto"); 
                    $('[data-name="neat_preparation_c"]').css("height", "auto"); 
                    $('[data-name="hdpe_eto_c"]').css("height", "auto"); 
                    $('[data-name="number_test_implants_c"]').css("height", "auto"); 
                    $('[data-name="number_control_implants_c"]').css("height", "auto"); 
                    $('[data-name="sample_prep_time_estimate_c"]').css("height", "auto"); 
                    $('[data-name="additional_supplies_required_c"]').css("height", "auto"); 
                    $('[data-name="sample_prep_design_notes_c"]').css("height", "auto"); 
                    
                    //$('[data-name="assigned_user_name"]').css("height", "auto");
                    //$('[data-name="m03_work_product_m03_work_product_deliverable_1_name"]').css("height", "auto");   

                    $('[data-name="timeline_type_c"]').css("visibility", "hidden"); 
                    $('[data-name="work_type_c"]').css("visibility", "hidden");
                    $('[data-name="bc_send_draft_report_c"]').css("visibility", "hidden");
                    $('[data-name="redlines_c"]').css("visibility", "hidden");
                    $('[data-name="draft_deliverable_sent_date_c"]').css("visibility", "hidden");
                    $('[data-name="deliverable_status_c"]').css("visibility", "hidden");
                    $('[data-name="overdue_risk_level_c"]').css("visibility", "hidden");
                     
                    $('[data-name="work_type_c"]').css("height", "0"); 
                    $('[data-name="bc_send_draft_report_c"]').css("height", "0"); 
                    $('[data-name="redlines_c"]').css("height", "0"); 
                    $('[data-name="draft_deliverable_sent_date_c"]').css("height", "0"); 
                    $('[data-name="deliverable_status_c"]').css("height", "0");
                    $('[data-name="overdue_risk_level_c"]').css("height", "0");  

                } else {
                    $('[data-name="same_day_prep_c"]').css("visibility", "hidden"); 
                    $('[data-name="neat_preparation_c"]').css("visibility", "hidden");
                    $('[data-name="hdpe_eto_c"]').css("visibility", "hidden");   
                    $('[data-name="number_test_implants_c"]').css("visibility", "hidden");
                    $('[data-name="number_control_implants_c"]').css("visibility", "hidden");   
                    $('[data-name="sample_prep_time_estimate_c"]').css("visibility", "hidden");
                    $('[data-name="additional_supplies_required_c"]').css("visibility", "hidden");   
                    $('[data-name="sample_prep_design_notes_c"]').css("visibility", "hidden");
                    
                    //$('[data-name="assigned_user_name"]').css("visibility", "hidden");
                    //$('[data-name="m03_work_product_m03_work_product_deliverable_1_name"]').css("visibility", "hidden");
                    $('[data-name="same_day_prep_c"]').css("height", "0"); 
                    $('[data-name="neat_preparation_c"]').css("height", "0"); 
                    $('[data-name="hdpe_eto_c"]').css("height", "0"); 
                    $('[data-name="number_test_implants_c"]').css("height", "0"); 
                    $('[data-name="number_control_implants_c"]').css("height", "0"); 
                    $('[data-name="sample_prep_time_estimate_c"]').css("height", "0"); 
                    $('[data-name="additional_supplies_required_c"]').css("height", "0"); 
                    $('[data-name="sample_prep_design_notes_c"]').css("height", "0");
                      
                    //$('[data-name="assigned_user_name"]').css("height", "0");  
                    //$('[data-name="m03_work_product_m03_work_product_deliverable_1_name"]').css("height", "0");
                    $('[data-name="deliverable_status_c"]').css("visibility", "visible");
                    $('[data-name="overdue_risk_level_c"]').css("visibility", "visible");
                    $('[data-name="timeline_type_c"]').css("visibility", "visible");
                    $('[data-name="work_type_c"]').css("visibility", "visible"); 
                    $('[data-name="bc_send_draft_report_c"]').css("visibility", "visible"); 
                    $('[data-name="redlines_c"]').css("visibility", "visible"); 
                    $('[data-name="draft_deliverable_sent_date_c"]').css("visibility", "visible");  
                    
                    $('[data-name="deliverable_status_c"]').css("height", "auto"); 
                    $('[data-name="work_type_c"]').css("height", "auto");
                    $('[data-name="bc_send_draft_report_c"]').css("height", "auto");
                    $('[data-name="redlines_c"]').css("height", "auto");
                    $('[data-name="draft_deliverable_sent_date_c"]').css("height", "auto");
                    $('[data-name="overdue_risk_level_c"]').css("height", "auto");
                }

                var type_3_c = this.model.get('type_3_c');
                if (type_3_c == 'Transfer') {
                    $('[data-name="datetime_desired_c"]').css("visibility", "visible"); 
                    $('[data-name="current_location_c"]').css("visibility", "visible");  
                    $('[data-name="transfer_to_location_c"]').css("visibility", "visible");   
                    
                    $('[data-name="datetime_desired_c"]').css("height", "auto");
                    $('[data-name="current_location_c"]').css("height", "auto");
                    $('[data-name="transfer_to_location_c"]').css("height", "auto");

                } else {
                    $('[data-name="datetime_desired_c"]').css("visibility", "hidden"); 
                    $('[data-name="current_location_c"]').css("visibility", "hidden"); 
                    $('[data-name="transfer_to_location_c"]').css("visibility", "hidden");

                    $('[data-name="datetime_desired_c"]').css("height", "0");
                    $('[data-name="current_location_c"]').css("height", "0");
                    $('[data-name="transfer_to_location_c"]').css("height", "0");
                }
                if (type_3_c == 'Shipping' || type_3_c == 'Archive' || deliverable_c =='Shipping Request' ) {
                    $('[data-name="shipping_conditions_c"]').css("visibility", "visible");  
                    $('[data-name="type_2_c"]').css("visibility", "visible"); 
                    $('[data-name="shipping_company_c"]').css("visibility", "visible");  
                    $('[data-name="hazardous_contents_c"]').css("visibility", "visible");
                    
                    $('[data-name="shipping_conditions_c"]').css("height", "auto");
                    $('[data-name="type_2_c"]').css("height", "auto");
                    $('[data-name="shipping_company_c"]').css("height", "auto");
                    $('[data-name="hazardous_contents_c"]').css("height", "auto");

                } else {
                    $('[data-name="shipping_conditions_c"]').css("visibility", "hidden");
                    $('[data-name="type_2_c"]').css("visibility", "hidden"); 
                    $('[data-name="shipping_company_c"]').css("visibility", "hidden"); 
                    $('[data-name="hazardous_contents_c"]').css("visibility", "hidden");
                    
                    $('[data-name="shipping_conditions_c"]').css("height", "0");
                    $('[data-name="type_2_c"]').css("height", "0");
                    $('[data-name="shipping_company_c"]').css("height", "0");
                    $('[data-name="hazardous_contents_c"]').css("height", "0");
                }
                if (type_3_c == 'Shipping' || type_3_c == 'Archive' || type_3_c == 'Transfer') {
                    $('[data-name="stability_considerations_c"]').css("visibility", "visible");
                    $('[data-name="stability_considerations_c"]').css("height", "auto");                               
                } else {
                    $('[data-name="stability_considerations_c"]').css("visibility", "hidden"); 
                    $('[data-name="stability_considerations_c"]').css("height", "0");
                }
                if (type_3_c == 'Shipping') {
                    $('[data-name="receiving_party_expecting_c"]').css("visibility", "visible");  
                    $('[data-name="attn_to_name_c"]').css("visibility", "visible");  
                    $('[data-name="address_street_c"]').css("visibility", "visible");  
                    $('[data-name="address_city_c"]').css("visibility", "visible");  
                    $('[data-name="address_state_c"]').css("visibility", "visible");  
                    $('[data-name="address_postalcode_c"]').css("visibility", "visible");  
                    $('[data-name="phone_number_c"]').css("visibility", "visible"); 
                    $('[data-name="address_confirmed_c"]').css("visibility", "visible");  
                    $('[data-name="faxitron_required_c"]').css("visibility", "visible");  
                    $('[data-name="address_country_c"]').css("visibility", "visible");
                    
                    $('[data-name="receiving_party_expecting_c"]').css("height", "auto");
                    $('[data-name="attn_to_name_c"]').css("height", "auto");
                    $('[data-name="address_street_c"]').css("height", "auto");
                    $('[data-name="address_city_c"]').css("height", "auto");
                    $('[data-name="address_state_c"]').css("height", "auto");
                    $('[data-name="address_postalcode_c"]').css("height", "auto");
                    $('[data-name="phone_number_c"]').css("height", "auto");
                    $('[data-name="address_confirmed_c"]').css("height", "auto");
                    $('[data-name="faxitron_required_c"]').css("height", "auto");
                    $('[data-name="address_country_c"]').css("height", "auto");
                   
                } else {
                    $('[data-name="receiving_party_expecting_c"]').css("visibility", "hidden"); 
                    $('[data-name="attn_to_name_c"]').css("visibility", "hidden");
                    $('[data-name="address_street_c"]').css("visibility", "hidden");
                    $('[data-name="address_city_c"]').css("visibility", "hidden");
                    $('[data-name="address_state_c"]').css("visibility", "hidden");
                    $('[data-name="address_postalcode_c"]').css("visibility", "hidden");
                    $('[data-name="phone_number_c"]').css("visibility", "hidden");
                    $('[data-name="address_confirmed_c"]').css("visibility", "hidden");
                    $('[data-name="faxitron_required_c"]').css("visibility", "hidden");
                    $('[data-name="address_country_c"]').css("visibility", "hidden");

                    $('[data-name="receiving_party_expecting_c"]').css("height", "0");
                    $('[data-name="attn_to_name_c"]').css("height", "0");
                    $('[data-name="address_street_c"]').css("height", "0");
                    $('[data-name="address_city_c"]').css("height", "0");
                    $('[data-name="address_state_c"]').css("height", "0");
                    $('[data-name="address_postalcode_c"]').css("height", "0");
                    $('[data-name="phone_number_c"]').css("height", "0");
                    $('[data-name="address_confirmed_c"]').css("height", "0");
                    $('[data-name="faxitron_required_c"]').css("height", "0");
                    $('[data-name="address_country_c"]').css("height", "0");
                }

                var deliverable_status_c = this.model.get('deliverable_status_c');
                if (deliverable_status_c == 'Completed') {
                    $('[data-name="deliverable_completed_date_c"]').css("visibility", "visible");   
                    $('[data-name="deliverable_completed_date_c"]').css("height", "auto");                                  
                } else {
                    $('[data-name="deliverable_completed_date_c"]').css("visibility", "hidden"); 
                    $('[data-name="deliverable_completed_date_c"]').css("height", "0");
                }

                var stability_considerations_c = this.model.get('stability_considerations_c');
                if (stability_considerations_c == 'Yes') {
                    $('[data-name="analyze_by_date_c"]').css("visibility", "visible"); 
                    $('[data-name="analyze_by_date_c"]').css("height", "auto");
                } else {
                    $('[data-name="analyze_by_date_c"]').css("visibility", "hidden"); 
                    $('[data-name="analyze_by_date_c"]').css("height", "0");
                }

                var shipping_company_c = this.model.get('shipping_company_c');
                if(shipping_company_c =='Fed Ex' || shipping_company_c=='On Time')
                {   
                    $('[data-name="shipping_delivery_c"]').css("visibility", "visible"); 
                    $('[data-name="shipping_delivery_c"]').css("height", "auto");
                } else {
                    $('[data-name="shipping_delivery_c"]').css("visibility", "hidden");  
                    $('[data-name="shipping_delivery_c"]').css("height", "0");
                }
                
                console.log(this.model.get('relative_2_c'));
                console.log(this.model.get('deliverable_c'));
                console.log(this.model.get('type_3_c'));
                console.log(this.model.get('deliverable_status_c'));

                
            },
            render: function () {
                this._super("render", arguments);
            },

            handleSave: function () {
                /** #1576 : 01 Oct 2021 */
                self = this;
                if (self.model.get("faxitron_required_c") == "Yes" && popupAlert == 0) {
                    popupAlert = 1;
                    $("#sidecar").prepend(`    
                       <div id="alertsCustom" class="alert-top">
                       <div class="alert-wrapper">
                           <div class="alert alert-warning alert-block closeable">
                               <div class="indicator">
                                   <i class="fa fa-exclamation-triangle"></i>
                               </div>
                               <button class="close btn btn-link btn-invisible closeCustomWPDPopup" data-action="close"><i class="fa fa-times"></i></button>
                               <span class="text">
                           <strong>Warning: </strong>
                           Deliverable must be assigned to Pathology Services for imaging prior to shipment.
                       </span>
                           </div>
                       </div>
                       </div>
                   `);
                } else {
                    $("#alertsCustom").remove();
                    self.saveWPDRecord();
                }
            },


            saveWPDRecord: function () {
                $("#alertsCustom").remove();
                //console.log(this.model.get('datetime_desired_c'));
                /** #642 : 31 May 2021 */
                var formattedDesiredDate = this._fixDateFormatToLocale(this.model.get('datetime_desired_c'));
                var formattedCurrentDate = this._fixDateFormatToLocale(new Date());
                var nextDate = this._fixDateFormatToLocale(new Date(formattedCurrentDate).getTime() + (60 * 60 * 24 * 1000));
                var showPopUpRecord = false;
                //console.log(new Date(formattedDesiredDate).getTime());
                //console.log(new Date(nextDate).getTime());
                if (new Date(formattedDesiredDate).getTime() < new Date(nextDate).getTime()) {
                    showPopUpRecord = true;
                }
                //console.log(showPopUpRecord);
                var stability_considerations_c = this.model.get('stability_considerations_c');
                var analyze_by_date_c = this.model.get('analyze_by_date_c');
                var final_due_date_c = this.model.get('final_due_date_c');
                if (stability_considerations_c == 'Yes' && analyze_by_date_c != '' && final_due_date_c != '' && analyze_by_date_c < final_due_date_c)
                //if(analyze_by_date_c < final_due_date_c )
                {
                    // console.log('if stability_considerations_c equal yes');
                    app.alert.show("NotAlloted_SaveWPD", {
                        level: "error",
                        messages: "The Analyze-by Date must not be before the External Final Due Date. Please review."
                    });

                } else if (stability_considerations_c != 'Yes') {
                    //console.log('if stability_considerations_c not equal yes');
                    this.model.set('analyze_by_date_c', '');
                }

                /*#642 : 31 May 2021*/
                if (this.model.get('deliverable_c') == 'SpecimenSample Disposition' && this.model.get('type_3_c') == 'Transfer' && showPopUpRecord && (typeof dateTimeChangedRV !== 'undefined') && dateTimeChangedRV && this.model.get('datetime_desired_c') != "" && this.model.get('datetime_desired_c') != null) {
                    //console.log('if deliverable_c equal SpecimenSample');
                    var self = this;
                    app.alert.show('message-id', {
                        level: 'confirmation',
                        messages: '24 hours notice is required for internal specimen transfers.  Continue with less than 24 hour notice?',
                        autoClose: false,
                        confirm: {
                            label: "Yes"
                        },
                        cancel: {
                            label: "No"
                        },
                        onCancel: function () {
                            return;
                        },
                        onConfirm: function () {
                            self._super('handleSave');
                        },

                    });
                } else {
                    this._super('handleSave');
                    //console.log('handleSave');
                }

            },

            /** #642 : 31 May 2021 */
            _fixDateFormatToLocale: function (date) {
                console.log('in _fixDateFormatToLocale');
                var local = app.date.utc(date).toDate();
                var dateObj = app.date(local);
                // get date and time based on user preferences
                var fixedDate = dateObj.format(app.date.getUserDateFormat());
                var fixedTime = dateObj.format(app.date.getUserTimeFormat());

                return fixedDate + ' ' + fixedTime;
            },
        });
    });
})(SUGAR.App);