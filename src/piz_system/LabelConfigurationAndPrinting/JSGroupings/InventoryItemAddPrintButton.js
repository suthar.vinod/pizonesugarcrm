(function addPrintButtonToListView(app) {
    app.events.on("app:start", function appStartAddButton() {

        if (!app.view.views.BaseRecordlistView && !app.view.views.BaseCustomRecordlistView) {
            app.view.declareComponent("view", "recordlist", undefined, undefined, false, "base");
        }

        var recordlistView = "BaseRecordlistView";

        if (app.view.views.BaseCustomRecordlistView) {
            recordlistView = "BaseCustomRecordlistView";
        }

        if (App.view.views[recordlistView].newEventHandlersAdded === true) {
            return;
        }

        App.view.views[recordlistView] = App.view.views[recordlistView].extend({
            newEventHandlersAdded: true,

            initialize: function (options) {
                var initRes = this._super("initialize", arguments);

                if ((this.module === "II_Inventory_Item") && this.name === "recordlist") {
                    var printLabelButton = {
                        "name":       "print_label_button",
                        "type":       "button",
                        "label":      "Print Labels",
                        "acl_action": "print_label_button",
                        "primary":    "true",
                        "events":     {
                            "click": "list:printLabelButton:fire",
                        },
                        // eslint-disable-next-line no-dupe-keys
                        "acl_action": "edit",
                    };

                    var printButton = this.meta.selection.actions;

                    printButton.push(printLabelButton);

                    this.meta.selection.actions = printButton;


                    this.context.on("list:printLabelButton:fire", this.startPrintProcess, this);
                }
                return initRes;
            },

            getValidTemplates: function (callback) {
                var apiUrl = app.api.buildURL("getValidTemplates", "create", {}, {});

                var successCallback = _.bind(function response(validTemplates) {
                    callback(validTemplates);
                }, this);

                var inventoryItems = [];
                var massCollection = app.controller.context.get("mass_collection");

                _.each(massCollection.models, function getCollectionData(model) {
                    inventoryItems.push({
                        category: model.get("category"),
                        type:     model.get("type_2"),
                    });
                });

                var requestParams = {
                    inventoryItemsCategory: inventoryItems,
                };

                app.api.call("create", apiUrl, requestParams, null, { success: successCallback });
            },

            openTemplatesDrawer: function (validTemplates) {
                var listCollection = app.data.createBeanCollection("PdfManager");
                var oldFetch = listCollection.fetch;

                listCollection.fetch = function fetch(options) {
                    options.params = {};
                    options.params.labelsTemplatesNames = validTemplates;
                    options.params.labelsTemplateManager = true;

                    oldFetch.apply(listCollection, [options]);
                },
                app.drawer.open({
                    layout:  "selection-list",
                    context: {
                        module:     "PdfManager",
                        printLabel: true,
                        collection: listCollection,
                        model:      app.data.createBean("PdfManager")
                    }
                }, this.generateAndPrint.bind(this));
            },

            generateAndPrint: function (model) {
                if (_.isEmpty(model) === false) {
                    var templateId = model.id;
					templateId = 'a0db8512-41b2-11ec-8224-029395602a62';
					
                    var massCollection = app.controller.context.get("mass_collection");

                    var apiUrl = app.api.buildURL("generateLabels", "create", requestParams, {});
                    var inventoryIds = [];

                    var err = 0;

                    _.each(massCollection.models, function getCollectionData(model) {
                        category	 = model.get("category");
						type_2		 = model.get("type_2");
						dateTimeType = model.get("collection_date_time_type");
						IC 			=  model.get("ic_inventory_collection_ii_inventory_item_1_name");
                         
						if(category=='Specimen'){
							if(type_2=='Extract'){
								err = 1;
							}
							
							if(type_2=='Block' || type_2=='Fecal'){
								if(dateTimeType=="Date at Specimen Collection" || dateTimeType=="Date Time at Specimen Collection")
								{err = 1;}
								
							}
                            if(type_2=='Tissue'){
								if((dateTimeType=="Date at Specimen Collection" || dateTimeType=="Date Time at Specimen Collection"))
								{err = 1;}
								
							}
							
						} 

                        inventoryIds.push({
                            inventoryId: model.get("id"),
                        });
                    });

                    if(err){
                        app.alert.show("no_Print_Label", {
                            level: "error",
                            messages: "No existing label template for chosen record!",
                            
                        });
                    }else{
                        var requestParams = {
                            inventoryIds: inventoryIds,
                            templateId:   'a0db8512-41b2-11ec-8224-029395602a62',//templateId,
                        };

                        app.api.call("create", apiUrl, requestParams, null, { success: this.printLabels.bind(this) });
                    }
                }
            },

            printLabels: function (data) {
                if (_.isEmpty(data) === false) {
                    var htmlText = data["bodyHtml"];
                    var template = Handlebars.compile(htmlText);
                    var htmlBody = template(data.records);
                    var popupWindow = window.open();
                    var timeoutWindowValue = 40;

                    popupWindow.document.body.innerHTML += htmlBody;
                    
                    setTimeout(function setTimeoutWindow(){
                        popupWindow.print();
                    }, timeoutWindowValue);

                    popupWindow.onafterprint = function printCompleted() {
                        popupWindow.close();
                    };
                }
            },

            startPrintProcess: function () {
                //this.getValidTemplates(this.openTemplatesDrawer.bind(this));
				this.generateAndPrint(this);
            },
        });
    });
})(SUGAR.App);
