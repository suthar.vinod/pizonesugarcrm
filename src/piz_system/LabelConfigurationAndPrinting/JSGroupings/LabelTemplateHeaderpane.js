(function extendHeaderpane(app) {
    app.events.on(
        "app:sync:complete",
        function extendHeaderpaneView() {
            if (
                !app.view.views.BaseSelectionHeaderpaneView &&
                !app.view.views.BaseCustomSelectionHeaderpaneView
            ) {
                app.view.declareComponent(
                    "view",
                    "selection-headerpane",
                    undefined,
                    undefined,
                    false,
                    "base"
                );
            }

            var selectionView = "BaseSelectionHeaderpaneView";

            if (app.view.views.BaseLeadsCustomRecordView) {
                selectionView = "BaseCustomSelectionHeaderpaneView";
            }

            if (
                App.view.views[selectionView]
                    .uSelectionViewOverride === true
            ) {
                return;
            }

            App.view.views[selectionView] = App.view.views[
                selectionView
            ].extend({
                uSelectionViewOverride: true,

                _formatTitle: function (title) {
                    var isFromPrint = this.options.context.get("printLabel");

                    if (isFromPrint === true) {
                        return "Select Template(s)";
                    } else {
                        return this._super("_formatTitle", arguments);
                    }
                },
            });
        }
    );
})(SUGAR.App);
