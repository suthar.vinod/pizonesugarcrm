(function extendInitFunc(app) {
    app.events.on(
        "app:sync:complete",
        function extendInventoryRecordFunctionality() {
            if (
                !app.view.views.BaseII_Inventory_ItemRecordView &&
                !app.view.views.BaseII_Inventory_ItemCustomRecordView
            ) {
                app.view.declareComponent(
                    "view",
                    "record",
                    "II_Inventory_Item",
                    undefined,
                    false,
                    "base"
                );
            }

            var baseInventoryRecordView = "BaseII_Inventory_ItemRecordView";

            if (app.view.views.BaseII_Inventory_ItemCustomRecordView) {
                baseInventoryRecordView = "BaseII_Inventory_ItemCustomRecordView";
            }

            if (
                App.view.views[baseInventoryRecordView]
                    .uBaseInventoryRecordViewOverride === true
            ) {
                return;
            }

            App.view.views[baseInventoryRecordView] = App.view.views[
                baseInventoryRecordView
            ].extend({
                uBaseInventoryRecordViewOverride: true,

                initialize: function (options) {
                    var initResult = this._super("initialize", arguments);

                    this.addPrintLabelButton();

                    return initResult;
                },

                addPrintLabelButton: function () {
                    var keepMeetingsHeaderPaneMeta = this.options.meta.buttons;
                    var printLabelsButton = {
                        name: "print_label_button",
                        type: "button",
                        label: "Print Labels",
                        events: {
                            click: "button:printLabelButton:click"
                        },
                        primary: true,
                        showOn: "view"
                    };

                    _.each(
                        keepMeetingsHeaderPaneMeta,
                        function addButton(action, index) {
                            if (index === 0) {
                                if (
                                    keepMeetingsHeaderPaneMeta[index].name !== "print_label_button"
                                ) {
                                    keepMeetingsHeaderPaneMeta.splice(
                                        index,
                                        0,
                                        printLabelsButton
                                    );
                                }
                            }
                        },
                        this
                    );

                    this.context.on("button:printLabelButton:click", this.startPrintProcess, this);
                },

                getValidTemplates: function (callback) {
                    var apiUrl = app.api.buildURL("getValidTemplates", "create", {}, {});

                    var successCallback = _.bind(function response(validTemplates) {
                        callback(validTemplates);
                    }, this);

                    var inventoryItems = [];

                    inventoryItems.push({
                        category: this.model.get("category"),
                        type: this.model.get("type_2"),
                    });

                    var requestParams = {
                        inventoryItemsCategory: inventoryItems,
                    };
                    app.api.call("create", apiUrl, requestParams, null, { success: successCallback });
                },

                openTemplatesDrawer: function (validTemplates) {
                    var listCollection = app.data.createBeanCollection("PdfManager");
                    var oldFetch = listCollection.fetch;

                    listCollection.fetch = function fetch(options) {
                        options.params = {};
                        options.params.labelsTemplatesNames = validTemplates;
                        options.params.labelsTemplateManager = true;

                        oldFetch.apply(listCollection, [options]);
                    },

                        app.drawer.open({
                            layout: "selection-list",
                            context: {
                                module: "PdfManager",
                                printLabel: true,
                                collection: listCollection,
                                model: app.data.createBean("PdfManager")
                            }
                        }, this.generateAndPrint.bind(this));
                },
                generateAndPrint: function (model) { //console.log("In Generate Print label");
                    if (_.isEmpty(model) === false) {
                        var templateId = model.id;
                        templateId = 'a0db8512-41b2-11ec-8224-029395602a62';

                        var apiUrl = app.api.buildURL("generateLabels", "create", requestParams, {});
                        var inventoryIds = [];

                        inventoryIds.push({
                            inventoryId: this.model.get("id"),
                        });

                        var err = 0;

                        category = this.model.get("category");
                        type_2 = this.model.get("type_2");
                        dateTimeType = this.model.get("collection_date_time_type");
                        IC = this.model.get("ic_inventory_collection_ii_inventory_item_1_name");

                        if (category == 'Specimen') {
                            if (type_2 == 'Extract') {
                                err = 1;
                            }

                            if (type_2 == 'Block' || type_2 == 'Fecal') {
                                if (dateTimeType == "Date at Specimen Collection" || dateTimeType == "Date Time at Specimen Collection") { err = 1; }

                            }
                            if (type_2 == 'Tissue') {
                                if ((dateTimeType == "Date at Specimen Collection" || dateTimeType == "Date Time at Specimen Collection")) { err = 1; }

                            }

                        }

                        if (err) {
                            app.alert.show("no_Print_Label", {
                                level: "error",
                                messages: "No existing label template for chosen record!",

                            });
                        } else {

                            var requestParams = {
                                inventoryIds: inventoryIds,
                                templateId: 'a0db8512-41b2-11ec-8224-029395602a62',//templateId,
                            };

                            app.api.call("create", apiUrl, requestParams, null, { success: this.printLabels.bind(this) });
                        }
                    }
                },
                printLabels: function (data) {

                    if (_.isEmpty(data) === false) {
                        var htmlText = data["bodyHtml"];
                        var template = Handlebars.compile(htmlText);
                        var htmlBody = template(data.records);
                        var popupWindow = window.open();
                        var timeoutWindowValue = 40;

                        popupWindow.document.body.innerHTML += htmlBody;

                        setTimeout(function setTimeoutWindow() {
                            popupWindow.print();
                        }, timeoutWindowValue);

                        popupWindow.onafterprint = function printCompleted() {
                            popupWindow.close();
                        };
                    }
                },

                startPrintProcess: function () {

                    this.generateAndPrint(this);
                },
            });
        }
    );
})(SUGAR.App);
