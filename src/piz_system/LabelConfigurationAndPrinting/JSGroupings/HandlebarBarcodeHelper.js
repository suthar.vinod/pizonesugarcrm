/**
 * Handlebars helpers.
 *
 * These functions are to be used in handlebars templates.
 * @class Handlebars.helpers
 * @singletonsp
 */
(function handlebarHelper(app) {
    app.events.on("app:init", function handlebarHelpers() {


        var setAttributes = function setAttributes(el, attrs) {
            for (var key in attrs) {
                el.setAttribute(key, attrs[key]);
            }
        };

        Handlebars.registerHelper("getBarcode", function barcode(barcodeValue, hbsElementAttributes) {
            var barcode = document.createElement("canvas");
            var width = hbsElementAttributes.hash.width;
            var height = hbsElementAttributes.hash.height;
            var fontSize = hbsElementAttributes.hash.fontSize;
            var displayValue = hbsElementAttributes.hash.displayValue;


            setAttributes(barcode, {
                "jsbarcode-value": barcodeValue,
                "jsbarcode-fontSize": fontSize,
                "jsbarcode-width": width,
                "jsbarcode-height": height,
                "jsbarcode-margin": "0",
                "jsbarcode-fontoptions": "normal",
                "jsbarcode-displayValue": displayValue,
            });

            // eslint-disable-next-line no-undef
            JsBarcode(barcode).init();
            var barcodeUrl = barcode.toDataURL();

            var img = document.createElement("img");

            img.setAttribute("src", barcodeUrl);

            return img.outerHTML;
        });

        Handlebars.registerHelper("isEqualWith", function isEqualWith(typeValue, list, options) {
            if (list.indexOf(typeValue) > -1) {
                return options.fn(this);
            }
            return options.inverse(this);
        });
    });
})(SUGAR.App);