<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("select id_c, account_id_c , contact_id1_c from m03_work_product_cstm inner join m03_work_product on m03_work_product_cstm.id_c = m03_work_product.id and m03_work_product.deleted = 0");
while ($row = $db->fetchByAssoc($result)) {
    $idOfWorkProduct = $row['id_c'];
    $date_modified = date("Y-m-d H:i:s");
    $deleted = 0;

    $contact_m03_wp_ida = $row['contact_id1_c'];
    $contactId = create_guid();
    if ($contact_m03_wp_ida) {
        $queryForContact = "insert into contacts_m03_work_product_1_c (id , date_modified , deleted , contacts_m03_work_product_1contacts_ida, contacts_m03_work_product_1m03_work_product_idb) "
                . "values ('" . $contactId . "', '" . $date_modified . "', '" . $deleted . "', '" . $contact_m03_wp_ida . "',  '" . $idOfWorkProduct . "')";
        if (!$db->query($queryForContact)) {
            $message1 = "Something wrong";
            echo "<script type='text/javascript'>alert('$message1');</script>";
            exit(1);
        }
    }

    $account_m03_wp_ida = $row['account_id_c'];
    $accountId = create_guid();
    if ($account_m03_wp_ida) {
        $queryForAccount = "insert into accounts_m03_work_product_1_c (id , date_modified , deleted , accounts_m03_work_product_1accounts_ida, accounts_m03_work_product_1m03_work_product_idb) "
                . "values ('" . $contactId . "', '" . $date_modified . "', '" . $deleted . "', '" . $account_m03_wp_ida . "',  '" . $idOfWorkProduct . "')";
        if (!$db->query($queryForAccount)) {
            $message2 = "Something wrong";
            echo "<script type='text/javascript'>alert('$message2');</script>";
            exit(1);
        }
    }
}
$message3 = "Script executed successfully but do not execute this script again otherwise unusual behaviour may occure";
echo "<script type='text/javascript'>alert('$message3');</script>";
