<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

global $db;
$names = array();
//Getting names of the records reached to 1000
$get_names_query = 'SELECT name FROM wpe_work_product_enrollment WHERE name LIKE "%1 WPE1000" GROUP BY name';
$resultedNames = $db->query($get_names_query);

while ($row = $db->fetchByAssoc($resultedNames)) {
    $names[] = $row['name'];
}

//foreach name getting all the records which have name WPE1000

foreach ($names as $key => $name) {
    $data = array();
    $get_records_query = 'SELECT CONVERT(SUBSTRING_INDEX(name,"WPE",-1), UNSIGNED INTEGER) as prefix, id, name FROM wpe_work_product_enrollment WHERE name = ' . "'$name'" . ' ORDER BY date_entered ASC';
    $result = $db->query($get_records_query);
    while ($row = $db->fetchByAssoc($result)) {
        $data[] = $row;
    }

    $i = 0;

    //setting new name for each record

    foreach ($data as $key => $index) {

        $id = $index['id'];
        $prefix = $index['prefix'] + $i++;
        $newName = strtok($index['name'], ' ') . ' WPE' . $prefix;
        $update_name_query = 'UPDATE wpe_work_product_enrollment SET name = ' . "'$newName'" . ' WHERE id = ' . "'$id'";
        $update_query_result = $db->query($update_name_query);

        if ($update_query_result) {
            echo "Record Updated Successfully!";
        }
        else {
            echo "Query Failed!";
        }
        echo '<br>' . $update_name_query . '<br>';
    }
    echo '<br>' . 'OK!' . '<br>';
}

