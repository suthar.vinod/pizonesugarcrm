<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once 'modules/EmailTemplates/EmailTemplate.php';
require_once('include/SugarPHPMailer.php');

class sendEmailTemplates
{
    //static $already_ran = false;
    function sendemail($data, $user_email, $wp_email, $oneupmanager_email)
    {
        // if (self::$already_ran == true)
        //     return;   //So that hook will only trigger once
        // self::$already_ran = true;
        $template = new EmailTemplate();
        $template = $this->getTemplate($data, $template);
        $this->sendMail($template, $user_email, $wp_email, $data["email"], $data['submitter_domain_check'], $oneupmanager_email);
    }

    function sendemailWithoutWP($data, $user_email, $oneupmanager_email)
    {
        // if (self::$already_ran == true)
        //     return;   //So that hook will only trigger once
        // self::$already_ran = true;
        $template = new EmailTemplate();
        $template = $this->getTemplate($data, $template);
        $this->sendMailForVCAndMR($template, $user_email, $data["email"], $data['submitter_domain_check'], $oneupmanager_email);
    }



    function getTemplate($data, $template)
    {
        $work_prod = '';
        $work_prod_compliance = "";
        $template->retrieve_by_string_fields(array('name' => 'APS Deviation', 'type' => 'email'));
        if ($data['error_type'] === 'Choose One' || $data['error_type'] == '') {
            $template->body_html = str_replace('[Error Type]', "NA", $template->body_html);
        } else {
            $template->body_html = str_replace('[Error Type]', $data['error_type'], $template->body_html);
        }

        $template->body_html = str_replace('[insert expected event]', $this->repResult($data['expected_event']), $template->body_html);
        $template->body_html = str_replace('[insert actual event]', $this->repResult($data['actual_event']), $template->body_html);
        $template->body_html = str_replace('[Error Category]', $this->repResult($data['error_category']), $template->body_html);
        $template->body_html = str_replace('[Date Occurred]', $this->repResult($data['date_error_occurred'], 'date'), $template->body_html);
        $template->body_html = str_replace('[Date Discovered]', $this->repResult($data['date_error_discovered'], 'date'), $template->body_html);
        $template->body_html = str_replace('[Date Time Discovered]', $this->repResult($data['date_time_error_discovered'], 'datetime'), $template->body_html);

        $template->body_html = str_replace('[insert Why Happened]', $this->repResult($data['why_it_happened_c']), $template->body_html);
        $template->body_html = str_replace('[insert Why Happened Detail]', $this->repResult($data['details_why_happened_c']), $template->body_html);
        if (empty($data['manager_departments'])) {
            $template->body_html = str_replace('[Manager Department]', 'NA', $template->body_html);
        } else {
            $mData = array();
            foreach ($data['manager_departments'] as $id => $name) {
                $mData[] = '<a target="_blank"href="' . $data['site_url'] . '/#Contacts/' . $id . '">' . $name . '</a>';
            }
            $template->body_html = str_replace('[Manager Department]', implode(", ", $mData), $template->body_html);
        }

        if (empty($data['oneupmanager_departments'])) {
            $template->body_html = str_replace('[OneUpManager Department]', 'NA', $template->body_html);
        } else {
            foreach ($data['oneupmanager_departments'] as $id => $name) {
                $oneupmanager[] = '<a target="_blank"href="' . $data['site_url'] . '/#Contacts/' . $id . '">' . $name . '</a>';
            }

            $template->body_html = str_replace('[OneUpManager Department]', $this->repResult(implode(",", $oneupmanager)), $template->body_html);
        }


        $template->body_html = str_replace('[Recommendation]', $this->repResult($data['recommendation']), $template->body_html);
        //Populate related data
        foreach ($data['related_data'] as $module => $related_records) {
            if (empty($related_records)) {
                $template->body_html = str_replace('[' . $module . ']', "NA", $template->body_html);
            } else {
                $rData = array();
                foreach ($related_records as $index => $record) {
                    if ($record['usda_id_c']) {
                        $rData[] = '<a target="_blank"href="' . $data['site_url'] . '/#' . $module . '/' . $record['id'] . '">' . $record['name'] . "-" . $record['usda_id_c'] . '</a>';
                    } else {
                        $rData[] = '<a target="_blank"href="' . $data['site_url'] . '/#' . $module . '/' . $record['id'] . '">' . $record['name'] . '</a>';
                    }
                }
                $template->body_html = str_replace('[' . $module . ']', implode(", ", $rData), $template->body_html);
            }
        }


        //Work Products
        $sData = array();
        foreach ($data['Title'] as $id => $name) {
            if ($name['name'] != "") {
                if ($name['wp_compliance'] == "GLP") {
                    $work_prod .= '<a style="color:red" target="_blank"href="' . $data['site_url'] . '/#M03_Work_Product/' . $id . '">' . $name['name'] . ' </a> ';
                } else {
                    $work_prod .= '<a target="_blank"href="' . $data['site_url'] . '/#M03_Work_Product/' . $id . '">' . $name['name'] . ' </a> ';
                }

                if (!empty($name['account_id']) && !empty($name['account_name'])) {
                    $work_prod .= '<a target="_blank"href="' . $data['site_url'] . '/#Accounts/' . $name['account_id'] . '"> (' . $name['account_name'] . ')</a>, ';
                }

                $work_prod_compliance = $name['wp_compliance'];
            }
            $bean = BeanFactory::retrieveBean("M03_Work_Product", $id);
            //Study Director
            if ($bean->contact_id_c) {
                $SDId = $bean->contact_id_c;
                $SDName = $bean->study_director_script_c;
                $sData = array();
                $sData[] = '<a target="_blank"href="' . $data['site_url'] . '/#Contacts/' . $SDId . '">' . $SDName . '</a>';
                $template->body_html = str_replace('[Study Director]', implode(", ", $sData), $template->body_html);
            } else {
                $template->body_html = str_replace('[Study Director]', "NA", $template->body_html);
            }
        }
        $pos = strrpos($work_prod, ', ');

        if ($pos !== false) {
            $work_prod = substr_replace($work_prod, '', $pos, strlen(', '));
        }

        if (is_array($sData) && count($sData) == 0) {
            $template->body_html = str_replace('[Study Director]', "NA", $template->body_html);
        }


        $template->body_html = str_replace('[insert Work Product]', $this->repResult($work_prod), $template->body_html);
        $template->body_html = str_replace('[WP Compliance]', $this->repResult($work_prod_compliance), $template->body_html);
        $template->body_html = str_replace('[insert Error Name]', '<a target="_blank"href="' . $data['site_url'] . '/#M06_Error/' . $data['id'] . '">' . $data['error_name'] . '</a>', $template->body_html);
        $template->body_html = str_replace('[insert email adress of the user that submitted the Error]', '<a href="mailto:' . $data['submittedBy'] . '">' . $data['submittedBy'] . '</a>', $template->body_html);
        $template->body_html = str_replace('[insert responsible personnel entry]', $this->repResult(implode(", ", $data['entryEmails'])), $template->body_html);
        $template->body_html = str_replace('[insert responsible personnel review]', $this->repResult(implode(", ", $data['reviewEmails'])), $template->body_html);

        $clinicalIssueComIDStr = "";
        $deceasedAnimalComIDStr = "";
        if ($data['clinicalIssueComID'] != "") {
            $clinicalIssueComIDStr = '<a target="_blank"href="' . $data['site_url'] . '/#M06_Error/' . $data['clinicalIssueComID'] . '">' . $data['clinicalIssueComName'] . '</a>';
        }

        if ($data['deceasedAnimalComID'] != "") {
            $deceasedAnimalComIDStr = '<a target="_blank"href="' . $data['site_url'] . '/#M06_Error/' . $data['deceasedAnimalComID'] . '">' . $data['deceasedAnimalComName'] . '</a>';
        }

        $template->body_html = str_replace('[Resolution Date]', $this->repResult($data['Resolution_Date'], 'date'), $template->body_html);

        $template->body_html = str_replace('[Clinical Issue COM ID]', $this->repResult($clinicalIssueComIDStr), $template->body_html);

        $template->body_html = str_replace('[Deceased Animal COM ID]', $this->repResult($deceasedAnimalComIDStr), $template->body_html);

        $clinicalIssueComIDStr = "";
        $deceasedAnimalComIDStr = "";
        if ($data['clinicalIssueComID'] != "") {
            $clinicalIssueComIDStr = '<a target="_blank"href="' . $data['site_url'] . '/#M06_Error/' . $data['clinicalIssueComID'] . '">' . $data['clinicalIssueComName'] . '</a>';
        }

        if ($data['deceasedAnimalComID'] != "") {
            $deceasedAnimalComIDStr = '<a target="_blank"href="' . $data['site_url'] . '/#M06_Error/' . $data['deceasedAnimalComID'] . '">' . $data['deceasedAnimalComName'] . '</a>';
        }

        $template->body_html = str_replace('[Resolution Date]', $this->repResult($data['Resolution_Date'], 'date'), $template->body_html);

        $template->body_html = str_replace('[Clinical Issue COM ID]', $this->repResult($clinicalIssueComIDStr), $template->body_html);

        $template->body_html = str_replace('[Deceased Animal COM ID]', $this->repResult($deceasedAnimalComIDStr), $template->body_html);
        $template->body_html = str_replace('[insert activities]', $this->repResult($data['activities']), $template->body_html);




        $customEmailSubject = $this->getEmailSubject($data);
        //$template->subject = $template->subject .' - '. $customEmailSubject;
        $template->subject = $customEmailSubject;
        return $template;
    }

    /**
     * create custom Email subject from template data (Work Products, test systems, Category)
     * @param array $data
     * @return string
     */
    function getEmailSubject($data)
    {
        $error_category = $data['error_category'];
        if (!empty($error_category)) {
            if ($error_category == "Work Product Outcome") {
                $error_category  = "WP Outcome ";
            }
            $subject .= $error_category;
        }

        if (!empty($data['error_type'])) {

            $subject .= ' : ' . $data['error_type'];
        }

        $work_products = array();
        foreach ($data['Title'] as $key => $value) {
            if ($value['name'] != "")
                $work_products[] = $value['name'];
        }
        if (!empty($work_products)) {
            //$subject .= ' - Work Product(s)['.implode(",",$work_products).']';
            $subject .= ' : ' . implode(",", $work_products) . '';
        }

        $anml_animals = array();
        foreach ($data['related_data']['ANML_Animals'] as $key => $value) {
            $testName = "";
            if ($value['name'] != "" && $value['usda_id_c'] != "")
                $testName = $value['name'] . '-' . $value['usda_id_c'];
            else if ($value['name'] != "")
                $testName = $value['name'];
            if ($testName != "")
                $anml_animals[] = $testName;
        }
        if (!empty($anml_animals)) {
            //$subject .= ' - Test System(s)['.implode(",",$anml_animals).']';
            $subject .= " : " . implode(",", $anml_animals);
        }
        return "COM : " . $subject;
    }

    /**
     * Replace empty variable values with NA in template instead of showing nothing
     * @param string $value
     * @return string
     */
    function repResult($value, $type = '')
    {
        if (trim($value) == '' || $value == NULL || $value == 'null') {
            return 'NA';
        }

        if (is_array($value) && count($value) == 0) {
            return 'NA';
        }
        if ($type == 'date') {
            $value = date("m-d-Y", strtotime($value));
        } else if ($type == 'datetime') {
            $arr = explode(" ", $value);
            if (count($arr) > 1) {
                $value = date("m-d-Y H:i", strtotime($value));
            }
        }
        return $value;
    }

    function sendMailForVCAndMR($template, $user_email, $submitter, $valid_domain, $oneupmanager_email)
    {

        $emailObj = new Email();
        $defaults = $emailObj->getSystemDefaultEmail();
        $mail = new SugarPHPMailer();
        $mail->setMailerForSystem();
        $mail->IsHTML(true);
        $mail->From = $defaults['email'];
        $mail->FromName = $defaults['name'];

        if (!empty($template->subject))
            $mail->Subject = $template->subject;
        else
            $mail->Subject = "";

        if (!empty($template->body_html)) {
            $mail->Body = $template->body_html;
        } else {
            $mail->Body = "";
        }
        foreach ($user_email as $umail) {
            $mail->AddAddress($umail);
        }

        if ($valid_domain == true) {
            $mail->AddCC($submitter);
        }

        foreach ($oneupmanager_email as $oneUpmail) {
            $mail->AddCC($oneUpmail);
        }

        $mail->AddBCC('cjagadeeswaraiah@apsemail.com');
        $mail->AddBCC('vsuthar@apsemail.com'); //to be deleted
        if (!$mail->Send()) {
            $GLOBALS['log']->debug("210:Fail to Send Email, Please check settings");
        } else {
            $GLOBALS['log']->debug('211: email sent');
        }
    }

    function sendMail($template, $user_email, $email, $submitter, $valid_domain, $oneupmanager_email)
    {

        $emailObj = new Email();
        $defaults = $emailObj->getSystemDefaultEmail();
        $mail = new SugarPHPMailer();
        $mail->setMailerForSystem();
        $mail->IsHTML(true);
        $mail->From = $defaults['email'];
        $mail->FromName = $defaults['name'];

        if (!empty($template->subject))
            $mail->Subject = $template->subject;
        else
            $mail->Subject = "";

        if (!empty($template->body_html)) {
            $mail->Body = $template->body_html;
        } else {
            $mail->Body = "";
        }
        foreach ($user_email as $umail) {
            $mail->AddAddress($umail);
        }
        foreach ($email as $wpmail) {
            $mail->AddAddress($wpmail);
        }

        if ($valid_domain == true) {
            $mail->AddCC($submitter);
        }
        foreach ($oneupmanager_email as $oneUpmail) {
            $mail->AddCC($oneUpmail);
        }
        
        $mail->AddBCC('cjagadeeswaraiah@apsemail.com');
        $mail->AddBCC('vsuthar@apsemail.com'); //to be deleted
        if (!$mail->Send()) {
            $GLOBALS['log']->debug("250: Fail to Send Email, Please check settings");
        } else {
            $GLOBALS['log']->debug('253: email sent');
        }
    }

    function sendMailForTSUseNotification($template, $email)
    {

        $emailObj = new Email();
        $defaults = $emailObj->getSystemDefaultEmail();
        $mail = new SugarPHPMailer();
        $mail->setMailerForSystem();
        $mail->IsHTML(true);
        $mail->From = $defaults['email'];
        $mail->FromName = $defaults['name'];

        if (!empty($template->subject))
            $mail->Subject = $template->subject;
        else
            $mail->Subject = "";

        if (!empty($template->body_html)) {
            $mail->Body = $template->body_html;
        } else {
            $mail->Body = "";
        }

        $mail->AddAddress('ehollen@apsemail.com');
        $mail->AddAddress('edrake@apsemail.com');
        $mail->AddAddress('calcox@apsemail.com');
        //$mail->AddAddress('dmcclenahan@apsemail.com');
        $mail->AddAddress('lzugschwert@apsemail.com');
        $mail->AddAddress('lwalz@apsemail.com');
        $mail->AddAddress('iacucsubmissions@apsemail.com');
		$mail->AddBCC('vsuthar@apsemail.com'); //to check the git commit
		
		
        if (!$mail->Send()) {
            $GLOBALS['log']->debug("Fail to Send Email TS Use Notification");
        } else {
            $GLOBALS['log']->debug('Sent Email TS Use Notification');
        }
    }
}
