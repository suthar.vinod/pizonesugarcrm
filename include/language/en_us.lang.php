<?php
$app_list_strings['moduleList']['Accounts'] = 'Companies';
$app_list_strings['moduleListSingular']['Accounts'] = 'Company';
$app_list_strings['record_type_display']['Accounts'] = 'Company';
$app_list_strings['parent_type_display']['Accounts'] = 'Company';
$app_list_strings['record_type_display_notes']['Accounts'] = 'Company';
$app_strings['NTC_OVERWRITE_ADDRESS_PHONE_CONFIRM'] = 'This record currently contains values in the Office Phone and Address fields. To overwrite these values with the following Office Phone and Address of the Company that you selected, click "OK". To keep the current values, click "Cancel".';
$app_strings['LBL_ACCOUNT'] = 'Company';
$app_strings['LBL_OLD_ACCOUNT_LINK'] = 'Old Company';
$app_strings['LBL_ACCOUNTS'] = 'Companies';
$app_strings['LBL_BILL_TO_ACCOUNT'] = 'Bill to Company';
$app_strings['LBL_LIST_ACCOUNT_NAME'] = 'Company Name';
$app_strings['LBL_ACCESSKEY_SELECT_ACCOUNTS_TITLE'] = 'Select Company';
$app_strings['LBL_ACCESSKEY_SELECT_ACCOUNTS_LABEL'] = 'Select Company';
$app_strings['LBL_ACCESSKEY_CLEAR_ACCOUNTS_TITLE'] = 'Clear Company';
$app_strings['LBL_ACCESSKEY_CLEAR_ACCOUNTS_LABEL'] = 'Clear Company';
$app_strings['LBL_SHIP_TO_ACCOUNT'] = 'Ship to Company';
$app_strings['LBL_DASHLET_NEWS_DESCRIPTION'] = 'Google News feed for Related Company';
$app_list_strings['moduleList']['Meetings'] = 'Communications';
$app_list_strings['moduleListSingular']['Meetings'] = 'Communication';
$app_list_strings['record_type_display_notes']['Meetings'] = 'Communication';
$app_list_strings['moduleList']['Opportunities'] = 'Opportunities';
$app_list_strings['moduleListSingular']['Opportunities'] = 'Opportunity';
$app_strings['LBL_TOUR_SCREEN_2_DESCRIPTION_670'] = '<div style="float: left;"><div style="float: left; width: 300px;"><ul><li class="fa fa-check">Choose fiscal year start date</li><li class="fa fa-check">Setup Time Periods to be either yearly or quarterly</li><li class="fa fa-check">Decide how many past and future Time Periods need to be visible in the worksheet</li><li class="fa fa-check">Select Forecast ranges to tag Opportunities as Include, Exclude or Upside</li><li class="fa fa-check">Select Forecast scenarios to identify Likely, Best and Worst forecast</li></ul></div><div class="well" style="float: left; width: 220px; margin-left: 20px;"><img src="themes/default/images/pt-screen2-thumb.png" width="220" id="thumbnail_2" class="thumb"></div></div><div class="clear"></div>';
$app_strings['LBL_TOUR_SCREEN_6_DESCRIPTION_670'] = '<div style="float: left;"><div style="float: left; width: 300px;"><ul><li class="fa fa-check">View Pareto chart for pipeline analysis over time for your Opportunities</li><li class="fa fa-check">Choose to visualize pipeline distribution by Sales Stage or Win Probability</li><li class="fa fa-check">View projected sales and how close you are to make your assigned Quota</li></ul></div><div class="well" style="float: left; width: 220px; margin-left: 20px;"><img src="themes/default/images/pt-screen6-thumb.png" width="220" id="thumbnail_6" class="thumb"></div></div><div class="clear"></div>';
$app_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$app_strings['LBL_OPPORTUNITY_NAME'] = 'Opportunity Name';
$app_strings['LBL_OPPORTUNITY'] = 'Opportunity';
$app_strings['LBL_EMAIL_QC_OPPORTUNITIES'] = 'Opportunity';
$app_strings['LBL_OPPORTUNITY_METRICS'] = 'Opportunity Metrics';
$app_strings['LBL_CREATE_OPPORTUNITY'] = 'Create Opportunity';
$app_strings['LBL_DASHLET_OPPORTUNITY_NAME'] = 'Opportunity Metrics';
$app_strings['LBL_DASHLET_OPPORTUNITY_DESCRIPTION'] = 'Opportunity Metrics for Related Company.  Requires D&B subscription.';
$app_strings['ERR_QUOTE_CONVERTED'] = 'This Quote has already been converted to an Opportunity.';
$app_strings['LBL_QUOTE_TO_OPPORTUNITY_LABEL'] = 'Create Opportunity from Quote';
$app_strings['LBL_QUOTE_TO_OPPORTUNITY_TITLE'] = 'Create Opportunity from Quote';
$app_strings['ERR_OPPORTUNITY_NAME_DUPE'] = 'An Opportunity with the name %s already exists.  Please enter another name below.';
$app_strings['ERR_OPPORTUNITY_NAME_MISSING'] = 'An Opportunity name was not entered.  Please enter an Opportunity name below.';
$app_strings['LBL_JOB_NOTIFICATION_OPPS_WITH_RLIS_BODY'] = 'Revenue Line Items are now enabled in your Sugar instance, and all existing Opportunity data has been processed. Your instance is now ready to be used with Revenue Line Items. For more information about the changes which have been made, please refer to the {{doc_url}}.

Sincerely, SugarCRM';
$app_strings['LBL_JOB_NOTIFICATION_OPP_FORECAST_SYNC_SUBJECT'] = 'Forecasts Module is Now Ready for Use With Opportunities in Your Sugar Instance';
$app_strings['LBL_JOB_NOTIFICATION_RLI_NOTE_BODY'] = 'Revenue line Items are now disabled in your Sugar instance, and all existing Revenue Line Item data has been processed. Your instance is now ready to be used with Opportunities. For more information about the changes which have been made, please refer to the {{doc_url}}.

Sincerely, SugarCRM';
$app_strings['LBL_JOB_NOTIFICATION_DOC_LINK_TEXT'] = 'Opportunities Configuration documentation';
$app_list_strings['moduleList']['Project'] = 'Projects';
$app_list_strings['moduleListSingular']['Project'] = 'Project';
$app_list_strings['record_type_display']['Project'] = 'Project';
$app_list_strings['parent_type_display']['Project'] = 'Project';
$app_list_strings['record_type_display_notes']['Project'] = 'Project';
$app_list_strings['moduleListSingular']['M02_SA_Division_Department_'] = 'SA Division Department';
$app_list_strings['moduleList']['M03_Work_Product_IDs'] = 'Additional Work Product IDs';
$app_list_strings['moduleListSingular']['M03_Work_Product_IDs'] = 'Work Product ID';
$app_list_strings['moduleList']['M03_Work_Product_Personnel'] = 'Additional Work Product Personnel';
$app_list_strings['moduleListSingular']['M03_Work_Product_Personnel'] = 'Additional Work Product Person';
$app_list_strings['moduleListSingular']['m06_APS_Communication'] = 'APS Communication';
$app_list_strings['moduleListSingular']['M01_SA_Division_Department'] = 'SA_Division_Department';
$app_list_strings['moduleListSingular']['m06_Note_To_File'] = 'Note_To_File';
$app_list_strings['moduleList']['M01_Sales_Activity_Quote'] = 'Activity Quotes';
$app_list_strings['moduleList']['m06_APS_Communication'] = 'APS Communications';
$app_list_strings['moduleList']['M01_SA_Division_Department'] = 'SA_Division_Departments';
$app_list_strings['moduleList']['m06_Note_To_File'] = 'Notes_To_File';
$app_list_strings['moduleList']['M01_Sales'] = 'Sales Activities';
$app_list_strings['moduleList']['M01_Sales_Activity'] = 'Sales';
$app_list_strings['moduleListSingular']['M01_Sales_Activity_Quote'] = 'Sales Activity Quote';
$app_list_strings['moduleList']['WPP_Work_Product_Personnel'] = 'Work Product Personnel';
$app_list_strings['moduleList']['M02_SA_Division_Department_'] = 'SA Divisions Departments';
$app_list_strings['moduleListSingular']['M01_Sales'] = 'Sale';
$app_list_strings['moduleListSingular']['M01_Sales_Activity'] = 'Sales Activity';
$app_list_strings['moduleList']['M01_Quote_Document'] = 'SA Quote Follow Up &amp; Documents';
$app_list_strings['moduleListSingular']['M01_Quote_Document'] = 'SA Quote Follow Up &amp; Document';
$app_list_strings['moduleList']['ABC12_Work_Product_Activities'] = 'Time Keeping';
$app_list_strings['moduleListSingular']['ABC12_Work_Product_Activities'] = 'Time Keeping';
$app_list_strings['is_manager_list'] = array(
    '' => '',
    'yes' => 'Yes',
    'no' => 'No',

);

$app_list_strings['ORI_or_status_list'] = array(
    'Ordered' => 'Ordered',
    'Backordered' => 'Backordered',
);

$app_list_strings['POI_or_status_list'] = array(
    'Ordered' => 'Ordered',
    'Backordered' => 'Backordered',
    '' => '',
);

$app_list_strings['orstatuslistnew'] = array(
    'Open' => 'Open',
    'Complete' => 'Complete',
    '' => '',
);

$app_list_strings['departments_list'] = array(
    '' => '',
    'SA AC' => 'SA AC',
    'LA AC' => 'LA AC',
    'SP' => 'SP',
    'IVP' => 'IVP',
    'SA OP' => 'SA OP',
    'DVM' => 'DVM',
    'LA OP' => 'LA OP',
    'ISR' => 'ISR',
    'PS' => 'PS',
    'SCI' => 'SCI',
    'AS' => 'AS',
    'Facility' => 'Facility',
);
