<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;
$resultData = array();
 
if(isset($_REQUEST['product_name'])){
    if ($_REQUEST['product_name'] != "" && $_REQUEST['post_action'] == 'product_data') {
        $sql = "SELECT * FROM `prod_product` WHERE `name`='" . $_REQUEST['product_name'] . "' AND `deleted`= 0 ";

        $result = $db->query($sql);
        if ($result->num_rows > 0) {
            $row = $db->fetchByAssoc($result);

            $external_barcode = $row['external_barcode'];

            $resultData = array(
                'external_barcode' => $external_barcode,
                'data' => $row,
                'status' => 'success',
            );
            echo json_encode($resultData);
        }
    }
}
if(isset($_REQUEST['expiration_date'])){
    if ($_REQUEST['expiration_date'] != "" && $_REQUEST['post_action'] == 'expiration_date_data') {

        $expDatetime	= date('Y-m-d 0:0:0',strtotime($_REQUEST['expiration_date'])); /*Get Last Time on Test Entered*/
        $currentTime	= date('Y-m-d 0:0:0', time()); //date('Y-m-d H:i:0', time()); /*Get Current Record*/
        $date1			= strtotime($expDatetime);  
        $date2			= strtotime($currentTime); 
        // Formulate the Difference between two dates 
        $diff = abs($date1 - $date2); /*Get Time Difference*/
        $days = floor(($diff)/ (60*60*24));	
        
        if($expDatetime > $currentTime){
            $no_of_days = $days;
        }else{
            //$no_of_days = '-'.$days;
            $no_of_days = '0';
        }
                
        $resultData = array(
            'days_to_expiration' => $no_of_days,
            'status' => 'success',
        );
        echo json_encode($resultData);

    }
}
if(isset($_REQUEST['expiration_date_time'])){
    if ($_REQUEST['expiration_date_time'] != "" && $_REQUEST['post_action'] == 'expiration_date_time_data') {

        $expDatetime	= date('Y-m-d 0:0:0',strtotime($_REQUEST['expiration_date_time'])); /*Get Last Time on Test Entered*/
        $currentTime	= date('Y-m-d 0:0:0', time()); //date('Y-m-d H:i:0', time()); /*Get Current Record*/
        $date1			= strtotime($expDatetime);  
        $date2			= strtotime($currentTime); 
        // Formulate the Difference between two dates 
        $diff = abs($date1 - $date2); /*Get Time Difference*/
        $days = floor(($diff)/ (60*60*24));	
        
        if($expDatetime > $currentTime){
            $no_of_days = $days;
        }else{
            //$no_of_days = '-'.$days;
            $no_of_days = '0';
        }
                
        $resultData = array(
            'days_to_expiration' => $no_of_days,
            'status' => 'success',
        );
        echo json_encode($resultData);

    }
}
?>