<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
require_once('custom/modules/Reports/CustomReportUtils.php');
// global $db;
global $timedate, $current_user;
$work_product_id = $_REQUEST['workproduct_id'];
$resultData = array();
$reportData = array();
if(isset($_REQUEST['report']) && $_REQUEST['report']=='AllNotificationforStudy'){
	$reportData = AllNotificationforStudy($work_product_id);
}else if(isset($_REQUEST['report']) && $_REQUEST['report']=='AllDeviationsforStudy'){
	$reportData = AllDeviationsforStudy($work_product_id);
}else if(isset($_REQUEST['report']) && $_REQUEST['report']=='AllAdverseEventsforStudy'){
	$reportData = AllAdverseEventsforStudy($work_product_id);
}

/* get date as per required formate and time zone */
$timedate = new TimeDate($current_user);
// returns current date/time in the user's timezone
$now_userTZ = $timedate->getNow(true);
$report_print_time = $now_userTZ->format("D, d M Y H:i:s O");

$resultData['status'] = 'Success';
$resultData['work_product'] = $reportData['work_product'];
$resultData['report_print_time'] = $current_user->id;
$resultData['report_length'] = sizeof($reportData['ReportData']);
$resultData['data'] = $reportData['ReportData'];
echo json_encode($resultData); 
 
?>