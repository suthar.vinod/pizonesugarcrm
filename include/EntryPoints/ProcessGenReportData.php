<?php


use Sugarcrm\Sugarcrm\modules\Reports\Exporters\ReportExporter;
require_once('include/export_utils.php');
global $db, $timedate;
// $report_id = $_REQUEST['report_id'];
$id = 'fbb122b2-8e99-11ea-a9f5-0242ac130006';


$user_timezone = $timedate->getInstance()->userTimezone(); // Time Zone
$timezone = new DateTimeZone($user_timezone);
$user_dateTime_obj = new DateTime(date(), $timezone);
$args = array();

$saved_report_seed = BeanFactory::newBean('Reports');
$saved_report_seed->retrieve($id, false);

$report_name = $saved_report_seed->name;
$report_type = $saved_report_seed->report_type;
$report_module = $saved_report_seed->module;

$reporter = new Report($saved_report_seed->content);

// ini_set('zlib.output_compression', 'Off');

$reporter->plain_text_output = true;
//disable paging so we get all results in one pass
$reporter->enable_paging = false;

$exporter = new ReportExporter($reporter);
$content = $exporter->export();


global $locale, $sugar_config;

$transContent = $GLOBALS['locale']->translateCharset(
	$content, 'UTF-8', $GLOBALS['locale']->getExportCharset(), false, true
);
$pattern = '/[\n]/';
$data = preg_split($pattern, $content);

$report_data = array();
foreach($data as $row){
	$simple_str =  substr($row,1,-2);
	$report_data[] = explode('","',$simple_str);
}
//remove extra empty row
array_pop($report_data);
$return_data = array();

// echo '<pre>';
// print_r($return_data);

//report_name,report_type,report_module
$return_data['status'] = 'Success';
$return_data['report_name'] = $report_name;
$return_data['report_id'] = $report_name;
$return_data['report_type'] = $report_type;
$return_data['report_module'] = $report_module;
$return_data['report_length'] = sizeof($report_data);
$return_data['report_data'] = $report_data;

echo json_encode($return_data);

?>