<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

    global $db;
    $SugarQuery = new SugarQuery();
    $SugarQuery->from(BeanFactory::newBean('M06_Error'));
    $SugarQuery->select(array('id', 'date_time_discovered_c'));
    $SugarQuery->where()->isNotEmpty('date_time_discovered_c')->notNull('date_time_discovered_c');
    $SugarQuery->where()->equals('wordpress_flag', '1');
    $query = $SugarQuery->compile();
    $GLOBALS['log']->debug("SugarQuery: ".$query);
    $results = $SugarQuery->execute();
    foreach($results as $index => $result) {
      $query = "UPDATE m06_error_cstm"
              ." SET date_time_discovered_text_c = '" . $result['date_time_discovered_c']."'"
              ." WHERE id_c = '" . $result['id']."'";
      $db->query($query);
    }

if ($index > 0) {
  echo "Success: Date Time Discovered field value successfully copied over new text field for " . $index ." records.";
} else {
  echo "Success: No records found with matching criteria.";
}
