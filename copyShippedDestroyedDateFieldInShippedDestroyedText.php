<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("SELECT m03_work_product_cstm.id_c, m03_work_product_cstm.shipped_destroyed_date_c FROM m03_work_product_cstm INNER JOIN m03_work_product ON m03_work_product.id = m03_work_product_cstm.id_c WHERE m03_work_product.deleted = 0 AND m03_work_product_cstm.shipped_destroyed_date_c IS NOT NULL");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $id = $row['id_c'];
        $shipped_destroyed_date_c = $row['shipped_destroyed_date_c'];
	$year = date('y', strtotime($shipped_destroyed_date_c));
        $month = date('m', strtotime($shipped_destroyed_date_c));
        $day = date('d', strtotime($shipped_destroyed_date_c));
        $new_date = $month. "/". $day . "/" . $year;

        $query = "update m03_work_product_cstm set shipped_destroyed_date_text_c = '" . $new_date . "' where id_c = '" . $id . "'";
        if (!$db->query($query)) {
            echo "Updation Failed";
            exit(1);
        }
    }
    echo "Script executed successfully!";
}
   

