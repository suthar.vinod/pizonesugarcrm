<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("SELECT id, name FROM de_deviation_employees WHERE date_entered >= '2019-01-01'");

while ($row = $db->fetchByAssoc($result)) {
    $id = $row['id'];
    $obser_emp_name = $row['name'];
    $updated_name = preg_replace('/OE/', 'D', $obser_emp_name, 1);
    $query_update = "UPDATE de_deviation_employees SET name = '".$updated_name."' WHERE id = '".$id."'";
    if(!$db->query($query_update)) {
        echo "Updation failed at some point";
        exit(1);
    }
    
}
echo "Observation Employees module names updated successfully!";
