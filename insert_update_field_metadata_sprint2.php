<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$queryInsert = "INSERT INTO `fields_meta_data` (`id`, `name`, `vname`, `comments`, `help`, `custom_module`, `type`, `len`, `required`, `default_value`, `date_modified`, `deleted`, `audited`, `massupdate`, `duplicate_merge`, `reportable`, `importable`, `ext1`, `ext2`, `ext3`, `ext4`) VALUES
('M06_Erroroccured_on_weekend_c', 'occured_on_weekend_c', 'LBL_OCCURED_ON_WEEKEND', NULL, NULL, 'M06_Error', 'bool', NULL, 0, '0', '2019-04-08 14:33:03', 0, 0, 0, 1, 1, 'true', NULL, NULL, NULL, NULL)";

if (!$db->query($queryInsert)) {
    echo "Insert failed at some point";
    exit(1);
}

$query_array = array();

$queryUpdate1 = "UPDATE fields_meta_data SET ext1 = 'error_category_list' "
        . "WHERE id = 'M06_Errorerror_type_c' ";
$query_array[1] = $queryUpdate1;

$queryUpdate2 = "UPDATE fields_meta_data SET ext1 = 'category_list' "
        . "WHERE id = 'M06_Errorerror_category_c' ";
$query_array[2] = $queryUpdate2;

$queryUpdate3 = "UPDATE fields_meta_data SET audited =0 "
        . "WHERE id = 'M06_Errorerror_category_c' ";
$query_array[3] = $queryUpdate3;


foreach ($query_array as $value) {
   
    if (!$db->query($value)) {
        echo "Updation failed for query" . $value;
        exit(1);
    }
}
echo "Script executed successfully";

