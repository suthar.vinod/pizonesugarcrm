<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$queryUpdate = "UPDATE accounts_cstm SET category_c = 'Sponsor' WHERE category_c IS NULL OR category_c = '' ";

if (!$db->query($queryUpdate)) {
    echo "Script failed";
    exit(1);
}
echo "Script executed successfully";

