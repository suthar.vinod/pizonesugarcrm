<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
global $app_list_strings;
$room_list = $app_list_strings['room_list'];
$room_assoc = array();
$record_count = 0;
unset($room_list['Choose One']);
$sugarQuery = new SugarQuery();
$sugarQuery->from(BeanFactory::newBean('ANML_Animals'));
$sugarQuery->select(array('id', 'room_c'));
$sugarQuery->where()->isNotEmpty('room_c');
$query = $sugarQuery->compile();
$results = $sugarQuery->execute();
echo "Animal Records Found: " . count($results) . "</br></br>";
foreach ($results as $index => $result) {
    $key = $result['room_c'];
    if (array_key_exists($key, $room_list)) {
        $roomNumber = $room_list[$key];
    } else {
        if (strlen($key) == 6 || strlen($key) == 5) {
            if (strpos($key, "-") === false) {
                $roomNumber = substr_replace($key, "-", 4, 0);
            }
        } else if (strlen($key) == 4) {
            if (strpos($key, "-") === false) {
                $roomNumber = substr_replace($key, "-", 3, 0);
            }
        } else {
            echo "Unable to find room in room list: " . $key . "</br>";
            continue;
        }
    }
    $GLOBALS['log']->debug("roomNumber: " . $roomNumber);
    echo "Room number : " . $roomNumber . "</br>";
    if (strpos($roomNumber, "-") !== false) {
        $roomArr = explode("-", $roomNumber);
        $prefixStr = $roomArr[0];
        $postfixStr = $roomArr[1];
        $prefixStr = $prefixStr . " AH";
        if (strlen($postfixStr) == 1) {
            $postfixStr = "0" . $postfixStr;
        }
        $roomToSearch = $prefixStr . "-" . $postfixStr;

        $GLOBALS['log']->debug("roomToSearch: " . $roomToSearch);
        echo "Room to search : " . $roomToSearch . "</br>";
        getRelatedRoom($roomToSearch, $result['id']);
    } else {
        echo "Invalid format for above room number.</br>";
    }
    $GLOBALS['log']->debug("----------------------------");
    echo "----------------------------</br>";
}

function getRelatedRoom($roomName, $animalId)
{
    global $room_assoc;
    if (array_key_exists($roomName, $room_assoc)) {
        attachAnimalToRoom($room_assoc[$roomName], $animalId);
    } else {
        $bean = BeanFactory::newBean('RMS_Room');
        $sugarQuery = new SugarQuery();
        $sugarQuery->from($bean);
        $sugarQuery->select(array('id'));
        $sugarQuery->where()->equals('name', $roomName);
        $sugarQuery->limit(1);
        $query = $sugarQuery->compile();
        $result = $sugarQuery->execute();
        if (!empty($result)) {
            $room_assoc[$roomName] = $result[0]['id'];
            attachAnimalToRoom($result[0]['id'], $animalId);
        } else {
            echo "No record found against room: " . $roomName . "</br>";
        }
    }
    $GLOBALS['log']->debug("Result: " . json_encode($result));
}

function attachAnimalToRoom($roomId, $animalId)
{
    global $db, $record_count;
    echo "Room found with ID: " . $roomId . "</br>";
    $query = "UPDATE anml_animals_cstm SET rms_room_id_c ='" . $roomId . "' WHERE id_c = '" . $animalId . "'";
    $GLOBALS['log']->debug("Query: " . $query);
    $db->query($query);
    $record_count++;
}

global $record_count;
echo "******* Process completed *******</br>";
echo "<b>" . $record_count . "</b> records are updated.</br>";