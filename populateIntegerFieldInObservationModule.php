<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("SELECT m06_error_cstm.id_c FROM m06_error_cstm WHERE m06_error_cstm.subtype_c LIKE '%^Documentation Error^%'");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $id = $row['id_c'];
        
        $query = "UPDATE m06_error_cstm SET m06_error_cstm.missing_date_initials_quan_c  = '1' WHERE m06_error_cstm.id_c = '" .$id. "'";
        if(!$db->query($query)) {
            echo "Falied to update Missing Date/Initials Quantity field with 1 for id = ". $id;
        }
    }
    echo "Missing Date/Initials Quantity field updated with 1 successfully !";
} else {
    echo "Table not found";
}
