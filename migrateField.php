<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
global $app_list_strings;
$category_dom = $app_list_strings['category_list'];
unset($category_dom['Choose One']);
$category_dom_keys = array_keys($category_dom);

$subtype_dom = $app_list_strings['error_type_list'];
unset($subtype_dom['Choose One']);
$subtype_dom_keys = array_keys($subtype_dom);

$type_dom = $app_list_strings['error_category_list'];
unset($type_dom['Choose One']);
$type_dom_keys = array_keys($type_dom);

$SugarQuery = new SugarQuery();
$SugarQuery->from(BeanFactory::newBean('M06_Error'));
$SugarQuery->select(array('id', 'error_category_c', 'error_type_c', 'subtype_c'));
$query = $SugarQuery->compile();
$GLOBALS['log']->debug("SugarQuery: ".$query);
$results = $SugarQuery->execute();
$type_count = 0;
$subtype_count = 0;
$query_count = 0;
$total = count($results);
echo "Processing records, Please wait....</br>";
echo "Total records found: <b>".$total."</b></br>";
foreach($results as $index => $result) {
    $subQuery = array();
    //Migrating Deviation Category into type
    if(in_array($result["error_category_c"],$type_dom_keys)
        && !empty($result["error_category_c"])
        && !in_array($result["error_type_c"],$type_dom_keys)
    ) {
        $subQuery[] = "error_type_c = '" . $result["error_category_c"] ."'";
        $subQuery[] = "error_category_c = NULL";
        $type_count++;
    }

    if(empty($subQuery) && !in_array($result["error_category_c"],$category_dom_keys)) {
        $subQuery[] = "error_category_c = NULL";
    }

    //Migrating Deviation type into subtype
    if(in_array($result["error_type_c"],$subtype_dom_keys) && !empty($result["error_type_c"])) {
        $migrate = true;
        //Checking if sub type already have correct values according to its dom.
        if(!empty($result["subtype_c"])) {
            $string = str_replace("^", "", $result["subtype_c"]);
            $string_array = explode(",", $string);
            if(in_array($string_array[0], $subtype_dom_keys)) {
                $migrate = false;
            }
        }
        if($migrate == true){
            $subQuery[] = "subtype_c = '^" . $result["error_type_c"] ."^'";
            $subQuery[] = "error_type_c = NULL";
            $subtype_count++;
        }
    }
    if(!empty($subQuery)) {
        $query_count++;
        $query = "UPDATE m06_error_cstm SET " . implode(", ", $subQuery)." WHERE id_c='" . $result['id']."'";
        $GLOBALS['log']->debug($query_count .": ".json_encode($query));
        $GLOBALS['log']->debug("--------------- * ---------------");
        $GLOBALS['db']->query($query);
    }
}
echo "******* Migration completed *******</br>";
echo "<b>1- Deviation Category:</b></br>";
echo "<b>". $type_count . "</b> record values are migrated. </br>";
echo "Almost ". ($total-$type_count). " records are skipped because their migrated values does not appear to be present in Type dom [error_category_list] OR they are already migrated.</br>";
echo "<b>2- Deviation Type:</b></br>";
echo "<b>". $subtype_count . "</b> record values are migrated. </br>";
echo "Almost ". ($total-$subtype_count). " are skipped because their migrated values does not appear to be present in Sub Type dom [error_type_list] OR they are already migrated.</br>";

