<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("select id_c, anml_animals_cstm.vendor_c from anml_animals_cstm inner join anml_animals on anml_animals.id = anml_animals_cstm.id_c where anml_animals.deleted = 0");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $id = $row['id_c'];
        $vendor_list_key = $row['vendor_c'];
        $vendor_list_value = $GLOBALS['app_list_strings']['vendor_list'][$vendor_list_key];
   
        if ($vendor_list_key) {
            if($vendor_list_value == "APS"){
                $query = "select id from accounts where name = 'American Preclinical Services' and deleted = 0";
            } else if($vendor_list_value == "Bakkom Rabbitry"){
                $query = "select id from accounts where name = 'Bakkom Rabbitry LLC' and deleted = 0";
            } else if($vendor_list_value == "Central Minnesota Livestock Sourcing"){
                $query = "select id from accounts where name = 'Central Minnesota Livestock Sourcing Inc.' and deleted = 0";
            } else if($vendor_list_value == "Elm Hill"){
                $query = "select id from accounts where name = 'Elm Hill Labs' and deleted = 0";
            } else if($vendor_list_value == "Envigo"){
                $query = "select id from accounts where name = 'Envigo (fka Harlan) Laboratories' and deleted = 0";
            } else if($vendor_list_value == "Exemplar"){
                $query = "select id from accounts where name = 'Exemplar Genetics' and deleted = 0";
            } else if($vendor_list_value == "Oak Hill Genetics"){
                $query = "select id from accounts where name = 'Oak Hill Genetics, LLC' and deleted = 0";
            } else if($vendor_list_value == "S and S Farms"){
                $query = "select id from accounts where name = 'S & S Farms' and deleted = 0";
            } else if($vendor_list_value == "Twin Valley Kennels"){
                $query = "select id from accounts where name = 'Twin Valley Kennel' and deleted = 0";
            } else if($vendor_list_value == "Rigland Farms"){
                $query = "select id from accounts where name = 'Ridglan Farms' and deleted = 0";
            } else if($vendor_list_value == "Neaton Polupays"){
                $query = "select id from accounts where name = 'Neaton Polypays' and deleted = 0";
            } else if($vendor_list_value == "Covance"){
                $query = "select id from accounts where name = 'Covance Research Products' and deleted = 0";
            } 
            else {
               $query = "select id from accounts where name = '".$vendor_list_value."' and deleted = 0";
            }
            $result2 = $db->query($query);
            if ($row2 = $db->fetchByAssoc($result2)) {
                $query2 = "update anml_animals_cstm set account_id_c = '".$row2['id']."' where id_c = '".$id."'";
                if(!$db->query($query2)) {
                    echo "Fail to populate ". $vendor_list_value. " for account id ". $row2['id']. "<br>";
                }
            } else {
                echo $vendor_list_value. " company not found <br>";
            }
        }
    }
    echo "Script executed successfully";
} else {
    echo "Table not found";
}

